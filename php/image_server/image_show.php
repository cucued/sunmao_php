<?php
include('init.php');

class image_show{
	private $aVersions;
	private $img_id;
	private $version;
	private $oUploader;
	public function run(){
		$this->img_id = $_GET['id'];
		$this->version = $_GET['version'];

		$this->oUploader = new imgsrv_model_upload();
		$img_path = $this->oUploader->get_image_path($this->img_id,$this->version);

		if(!is_file($img_path)){

			$this->resize();
		}

		
        header("Content-Type: ".$this->oUploader->get_image_mime($this->img_id));
        header("Content-Length: " . $this->oUploader->get_image_size($this->img_id,$this->version));
        
        $fp = fopen($img_path , 'rb');            
        fpassthru($fp);
	}
	private function resize(){
		$this->aVersions = ml_factory::load_common_standard_conf('imageVersion');
		
		$img_ori_path = $this->oUploader->get_image_path($this->img_id,'ori');
		if(!is_file($img_ori_path)){
			
			$this->_not_found();
		}
		
		if(!$this->aVersions[$this->version]){

			$this->_not_found();
		}

		$conf = $this->aVersions[$this->version];
	    $oResizer = new Lib_image_resizer();
	    $oResizer->set_source_image($img_ori_path);

	    $dest_path = $this->oUploader->get_image_path($this->img_id,$this->version);
	    Tool_os::checkDir(dirname($dest_path));

	    $oResizer->set_dest_image($dest_path);



	    if($conf['type'] == ML_IMG_TYPE_REGULARWIDTH)
	    {
	        $oResizer->set_mode(Lib_image_resizer::MODE_REGWIDTH);
	        $oResizer->set_single_size($conf['width']);
	        $oResizer->resize();
	    }
	    else if($conf['type'] == ML_IMG_TYPE_CROP)
	    {
	        $oResizer->set_mode(Lib_image_resizer::MODE_CROP);
	        $oResizer->set_size($conf['width'] , $conf['height']);
	        $oResizer->resize();
	    }

	    $pic_info = Lib_image_uploader::decode_image_name($this->img_id);

	    if($pic_info['isWaterMark']){
		    $oMark = new Lib_image_watermark();
		    $oMark->mark($dest_path,IMAGE_ROOT_PATH.'/aaa.gif',$dest_path,10,10);
		}
	
	}
	private function _not_found(){

		$conf = $this->aVersions[$this->version];
		
		$width = $conf['width'];
		$height = $conf['height'] ? $conf['height'] : $conf['def_height'];



		die;
	}
}
$o = new image_show();
$o->run();