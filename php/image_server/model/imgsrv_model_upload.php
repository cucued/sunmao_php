<?php
class imgsrv_model_upload
{
	const SIZE_THUMBNAIL = 'thumb';
	const SIZE_SMALL = 'small';

	const IMG_MAX_SIZE_M = 5;
	private $result;
	public function upload($key , $isWaterMark = true){
		$oUpload = new Lib_image_uploader();
		$oUpload->start($key);

		if(!$oUpload->check_file_size()){
			$this->result = 'size';
			return false;
		}else if(!$oUpload->check_content_type()){
			$this->result = 'type';
			return false;
		}

		$img_id = $oUpload->encode_image_name($isWaterMark);
		$save_dir = $this->calc_image_dirpath($img_id , 'ori');
		
		$oUpload->set_save_dir($save_dir);

		$oUpload->set_file_name($img_id);
		$oUpload->save();
		$this->result = $img_id;
		return true;
	}
	public function get_result(){
		return $this->result;
	}

	static public function get_display_url($img_id,$size = 'big'){
		return BIZSITE_DOMAIN_IMAGE.'/show/'.$img_id.'/'.$size;
	}
	public function get_image_mime($img_id){
		$image_info = Lib_image_uploader::decode_image_name($img_id);
		return $image_info['mime'];
	}
	static public function get_image_upload_time($img_id){
		$image_info = Lib_image_uploader::decode_image_name($img_id);
		return $image_info['time'];	
	}
	public function get_image_size($img_id,$version = 'big'){
		return filesize($this->calc_image_dirpath($img_id,$version).'/'.$img_id);
	}

	public function get_image_path($img_id , $version='big'){
		return $this->calc_image_dirpath($img_id,$version).'/'.$img_id;
	}

	private function calc_image_dirpath($img_id , $version='big'){
		$subdir1 = substr($img_id,0,2);
		return BIZ_IMAGESERVER_DATAPATH.($version?$version.'/':'').$subdir1;
	}

}