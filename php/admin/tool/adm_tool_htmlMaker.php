<?php
class adm_tool_htmlMaker{
	static function select($aData , $value , $id , $nullOption = '请选择',$attr = ''){
		$class = $class ? 'class="'.$class.'"' : '';
		$html = '<select id="'.$id.'"'.$class.' '.$attr.'>';

		if($nullOption){
			$html .= '<option>'.$nullOption.'</option>';
		}
		foreach ($aData as $key => $v) {
			$html .= '<option value="'.$key.'"'.($key==$value ? ' selected' : '').'>'.$v.'</option>';
		}
		$html .= '</select>';
		return $html;
	}
	static function html_header($title = ''){
		$controller = admin_controller::get_instance();
		$theme = $controller->theme;
		?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>管理后台</title>
  <link rel="stylesheet" type="text/css" href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/css/themes/<?php echo $theme; ?>/easyui.css">
  <link rel="stylesheet" type="text/css" href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/css/icon.css">
  <link rel="stylesheet" type="text/css" href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/css/font-awesome.css">
  <link rel="stylesheet" type="text/css" href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/css/bearshop.css">
  
  <script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/jquery.easyui.min.js"></script>
  <?php if($controller->get_lang() == 'cn'){ ?>
  <script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/easyui-lang-zh_CN.js"></script>
  <?php } ?>
  <script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/bearshop.js"></script>
  
  
</head>
		<?php
	}

	static function js_lang($lang){
		?>
<script type="text/javascript">
	var L=<?php echo json_encode($lang) ?>;
	</script>
		<?php
	}
}