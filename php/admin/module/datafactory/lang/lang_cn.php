<?php
$LANG['datafactory'] = array(
	'data_type' => '数据种类',
	'set_data' => '设置数据',
	'backup_memo' => '说明',
	'isNeedBackup' => '是否需要备份',
	'basic_info_for_data' => '基本信息',
	'data_description' => '数据描述',
);