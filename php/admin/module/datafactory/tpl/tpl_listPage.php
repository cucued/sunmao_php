<?php
	adm_tool_htmlMaker::html_header(); 


?>

<body style="margin: 10px;" url_base="<?php echo $url_base; ?>">


<table class="easyui-datagrid" id="tt" title="数据类型" data-options="singleSelect:false,selectOnCheck:true,collapsible:true,url:'/datafactory/index/listJson',method:'get',pagination:true,pageSize:10,toolbar:'#dgMenu'">
	<thead>
		<tr>
			<th field="ck" checkbox="true"></th>
		<?php foreach ($field2name as $key => $value) {
				$sortSign = in_array($key , $aSortable) ? ' sortable="true"' : '';
			?>
			<th data-options="field:'<?php echo $key; ?>'"<?php echo $sortSign; ?>><?php echo $value; ?></th>
		<?php } ?>
			<th data-options="field:'op'"><?php L('edit'); ?></th>
		</tr>
	</thead>
</table>

<div id="dgMenu" style="padding:5px;height:auto">
	<div class="bs-dtgrid-menu-span icon-filter bs-icon-left">
			<span class="bs-dtgrid-menu-span right-border">
				<?php foreach (array_keys($filterField2Options) as $field) { ?>
				<?php echo $field2name[$field]; ?>:
				<?php echo adm_tool_htmlMaker::select($filterField2Options[$field] , '','sel_'.$field , L('all',1) , 'field="'.$field.'"') ?>
				<?php } ?>
			</span>
			<span class="bs-dtgrid-menu-span right-border">
				<?php L('search');?>:<input id="iptSearch">
				<?php echo adm_tool_htmlMaker::select($searchField2name , 0, 'selSearchField','') ?>
			</span>
			<input id="btnSelect" type="button" value="<?php L('select'); ?>">
	</div>
</div>

<script type="text/javascript">
//filter
	$('#btnSelect').click(function(){
		var aCondition = Array();
		$(this).parent().parent().find('select').each(function(){
			if($(this).find('option:selected').attr('value') > 0){
				aCondition.push($(this).attr('field')+'='+$(this).val());
			}
		});

		if($('#iptSearch').val()){
			aCondition.push('search_field='+$('#selSearchField').val());	
			aCondition.push('search_key='+$('#iptSearch').val());	
		}

		var sCondition = aCondition.join('&');
		
		$('#tt').datagrid({  
    		url:$('body').attr('url_base')+'/listJson?'+sCondition,  
		});
		
	})
//image preview
	$('#tt').datagrid({  
	    onLoadSuccess:function(data){  

	        $('.imgPreview').tooltip({
			    position: 'left',
			    content: function(){return '<img id="imgPreview" src="'+$(this).attr('src')+'" width="300"/>'}
		    });
		}  
	}) 
//
	function changeById(dom){
		var select = $(dom);
		$.get($('body').attr('url_base')+'/doChange?id='+select.attr('data_id')+'&field='+select.attr('field')+'&value='+select.val(),'',function(data){
			var rs = bs_str2obj(data);
			if(rs.code != APICODE_SUCC){
				alert('error');
			}

		})
	}
</script>
</body>
</html>