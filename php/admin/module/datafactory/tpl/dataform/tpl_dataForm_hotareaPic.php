<div id="wCrop" class="easyui-window" title="Basic Window" data-options="iconCls:'icon-save'" collapsible="false" minimizable="false" maximizable="false" closed="true" style="width:500px;height:200px;padding:10px;">
		<img src="" id="imgHotareaPreview" width="300px">
	</div>
<script type="text/javascript">
	var jcrop_api;
	var x_ipt;
	var y_ipt;
	var w_ipt;
	var h_ipt;
    $('#imgHotareaPreview').Jcrop({
    	onChange:drawHotarea,
    },function(){
      jcrop_api = this;
    });
    function drawHotarea(c){

    	x_ipt.val(c.x);
    	y_ipt.val(c.y);
    	w_ipt.val(c.w);
    	h_ipt.val(c.h);
    }
    
	$('#imgcover_img_id').load(function(){
		// $('#imgHotareaPreview').attr('src',$(this).attr('src'));
		
		jcrop_api.setImage($(this).attr('src'));
	})
	

	$('input').click(function(){
		var name = $(this).attr('name')
		if(name.substring(0,9) == 'hotarea_x'){
			var mix_tb = $(this).parentsUntil('table');
			x_ipt = mix_tb.find("[name^='hotarea_x']");
			y_ipt = mix_tb.find("[name^='hotarea_y']");
			w_ipt = mix_tb.find("[name^='hotarea_w']");
			h_ipt = mix_tb.find("[name^='hotarea_h']");
			var coord_choose = [
				x_ipt.val(),
				y_ipt.val(),
				parseInt(x_ipt.val())+parseInt(w_ipt.val()),
				parseInt(y_ipt.val())+parseInt(h_ipt.val())
			];
			console.log(x_ipt.val()+' '+y_ipt.val()+' '+w_ipt.val()+' '+h_ipt.val()+' ');
			jcrop_api.setSelect(coord_choose);
			$('#wCrop').window('open');
			
		}
	});
</script>
