<?php
	adm_tool_htmlMaker::html_header(); 

	$rs = $oEditForm->prepare();
	$fields = $rs['fields'];
	$mixfields = $rs['mixfields'];

	$submit = $rs['submit'];
	echo $oEditForm->getInclude(); 
?>
<script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/jcrop/jquery.Jcrop.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/css/jcrop/jquery.Jcrop.min.css">
<style type="text/css">

	
</style>
<body style="margin: 10px;">
<form id="dataFactoryForm" method="post">
<div class="easyui-panel" title="<?php L('data_description'); ?>" style="width:100%"<?php if($action=='modify'){ ?> collapsed="true"<?php } ?> data-options="collapsible:true">
    <table class="bs_adm_formtable">
		<tr>
			<td class="nameTd"><?php echo $fields['title']['cn_name']; ?>:</td>
			<td><?php echo $fields['title']['html']; ?></td>
		</tr>
		<tr>
			<td class="nameTd"><?php echo $fields['description']['cn_name']; ?>:</td>
			<td><?php echo $fields['description']['html']; ?></td>
		</tr>
		<tr>
			<td class="nameTd"><?php echo $fields['status']['cn_name']; ?>:</td>
			<td><?php echo $fields['status']['html']; ?></td>
		</tr>
	</table>
</div>
<?php
	unset($fields['title']);
	unset($fields['description']);
	unset($fields['status']);
?>

<div class="easyui-panel" title="<?php L('basic_info_for_data'); ?>" style="width:100%" data-options="collapsible:true">
    <table class="bs_adm_formtable">
    	<?php foreach ($fields as $row) { ?>
		<tr>
			<td class="nameTd"><?php echo $row['cn_name']; ?>:</td>
			<td><?php echo $row['html']; ?></td>
		</tr>
		<?php } ?>
	</table>
</div>
<?php if($mixfields){ ?>
<div class="easyui-tabs">
	<?php foreach ($mixfields as $key => $mixfield) { ?>
	<div title="<?php echo $mixfield['cn_name'] ?>" style="padding:10px">
		<div class="bs_adm_btnpanel">
			<a href="javascript:;" class="aAddMix easyui-linkbutton" data-options="iconCls:'icon-add'" type="<?php echo $key; ?>"><?php L('add'); ?></a>
			
			<a href="javascript:;" class="aSortMix easyui-linkbutton" data-options="iconCls:'icon-filter'" toggle="true" sort="0" lang="<?php L('done')?>"><?php L('sort'); ?></a>
		</div>
		<div class="divMixhtmlTpl" style="display: none;">
			<div style="margin-left: 20px">
				<a href="javascript:;" class="aDelMix">x</a>
				<?php echo $mixfield['html']; ?>
			</div>
		</div>
		<div>
			<ul class="ulMix bs_ulMix" type="<?php echo $key; ?>"></ul>
		</div>
	</div>
	<?php } ?>
</div>
<?php } ?>
			
<hr>
<?php echo $submit; ?>
</form>


<?php if($dataForm){
	foreach ($dataForm as $row) {
		include($module_path.'/tpl/dataform/tpl_dataForm_'.$row.'.php');
	}
	
} ?>
<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
<?php echo $oEditForm->getJs();?>
</body>
<script type="text/javascript">

	$('.aAddMix').click(function(){
		var this_div = $(this).parent().parent();
		var tpl = this_div.children('.divMixhtmlTpl').children().clone(true);
		var sons_count = this_div.find('ul').children().size();
		tpl.find('input').each(function(){
			if($(this).attr('name')){
				$(this).attr('name',$(this).attr('name')+'_'+(sons_count+1));
			}
		});
		tpl.find('textarea').each(function(){
			if($(this).attr('name')){
				$(this).attr('name',$(this).attr('name')+'_'+(sons_count+1));
			}
		});
		this_div.find('ul').append('<li class="drag-item" sort_number="'+(sons_count+1)+'"></li>');
		tpl.appendTo(this_div.find('ul').children(':last'));
		start_drag(this_div.find('ul').children(':last'));
	})
	$('.aDelMix').click(function(){
		$(this).parent().parent().remove();
	});

	var indicator = $('<div class="indicator">>></div>').appendTo('body');

	

	var mix = <?php echo $mix ? $mix : '[]'; ?>;
	for(var mixtype in mix){
		var n = mix[mixtype].length;
		for (var i = 0; i < n; i++) {
			$('.aAddMix[type="'+mixtype+'"]').trigger('click');
			var mix_tb = $('.ulMix[type="'+mixtype+'"]').children(':last');
			var mix_fields = mix[mixtype][i];
			// console.log(mix_fields['hotarea_title']);
			mix_tb.find('input').each(function(){
				
				var n = $(this).attr('name').split('_',2).join('_');
				$(this).val(mix_fields[n]);
			})
			mix_tb.find('textarea').each(function(){
				
				var n = $(this).attr('name').split('_',2).join('_');
				$(this).val(mix_fields[n]);
			})

		};

	}

	function start_drag(dom){
		dom.draggable({
			containment:'ul',
			revert:true,
			deltaX:0,
			deltaY:0
		}).droppable({
			onDragOver:function(e,source){
				indicator.css({
					display:'block',
					left:$(this).offset().left-10,
					top:$(this).offset().top+$(this).outerHeight()-5
				});
			},
			onDragLeave:function(e,source){
				indicator.hide();
			},
			onDrop:function(e,source){
				$(source).insertAfter(this);
				indicator.hide();
			}
		});
		dom.draggable('disable');
	}
	start_drag($('.drag-item'));
	$('.drag-item').draggable('disable');
	$('.aSortMix').click(function(){

		var lang = $(this).text();
		if($(this).attr('sort') == 1){
			$(this).attr('sort',0);
			$('.drag-item').draggable('disable');

		}else{
			$(this).attr('sort',1);
			$('.drag-item').draggable('enable');
		}
		
	})
</script>

</html>