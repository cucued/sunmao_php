<?php
	adm_tool_htmlMaker::html_header(); 

	$rs = $oEditForm->prepare();
	$fields = $rs['fields'];
	$fields = $rs['fields'];
	$submit = $rs['submit'];
	echo $oEditForm->getInclude(); 

	
?>

<body style="margin: 10px;">
<form id="dataFactoryForm" action="/datafactory/data/doModify?type=<?php echo $type_id; ?>&id=<?php echo $id ?>" method="post">
<div class="easyui-panel" title="<?php L('basic_info_for_data'); ?>" data-options="collapsible:true">
    <table>
		<tr>
			<td><?php echo $fields['title']['cn_name']; ?>:</td>
			<td colspan="3"><?php echo $fields['title']['html']; ?></td>
		</tr>
		<tr>
			<td><?php echo $fields['description']['cn_name']; ?>:</td>
			<td colspan="3"><?php echo $fields['description']['html']; ?></td>
		</tr>
	</table>
</div>
<?php
	unset($fields['title']);
	unset($fields['description']);
?>
<table cellspacing="0" style="width: 100%;">
	<tr>
		<td style="vertical-align: top;">
			<div class="easyui-panel" title="<?php L('basic_info_for_data'); ?>" data-options="collapsible:true">
			    <table>
			    	<?php foreach ($fields as $row) { ?>
					<tr>
						<td><?php echo $row['cn_name']; ?>:</td>
						<td colspan="3"><?php echo $row['html']; ?></td>
					</tr>
					<?php } ?>
				</table>
			</div>
			
		</td>
		
	</tr>

</table>
<hr>
<?php L('isNeedBackup'); ?>:<input type="checkbox" name="backup" value="1" /><br/>
<?php L('backup_memo'); ?>:<input name="backup_memo" id="ipt_backup_memo" type="text" class="easyui-validatebox" validType="length[2,30]"><br/>
<?php echo $submit; ?>
</form>

<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
<?php echo $oEditForm->getJs("/datafactory/data/doModify?type=".$type_id.'&id='.$id); ?>
</body>
</html>