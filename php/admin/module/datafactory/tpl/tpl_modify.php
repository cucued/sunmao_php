<?php
	adm_tool_htmlMaker::html_header(); 

	$html = $oEditForm->getFormHtml($aEditField , '/company/staff/doCreate' , file_get_contents(dirname(__file__).'/tpl_articleForm.html'));
	echo $oEditForm->getInclude(); 
?>

<body style="margin: 10px;">

<div class="easyui-panel" title="<?php L('modify'); ?>">
    <?php echo $html; ?>
</div>

<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
<?php echo $oEditForm->getJs(); ?>
</body>
</html>