<?php

class adm_datafactory_index extends datafactory_controller{

	function _construct(){
		$this->setDataDefine('datafactory_type');
		$this->listField = array('title','data_key','num_rows' , 'create_user' , 'status');
		$this->filterField = array('status');
		$this->searchField = array('title','data_key' , 'description');
		$this->sortableField = array();
		$this->modifyInListField = array();

		$this->listTitle = L('data_type' , true);
	}

	protected function _fetchForModify($id){
		$oType = new adm_datafactory_model_type();
		$oType->getById($id) or $this->redirect('busy');
		return $oType->get_data();
	}
	protected function _doCreate($aData){
		$oType = new adm_datafactory_model_type();
		$oType->insertType($aData , $this->adm_id) or $this->output_json(APICODE_BUSY);
		return $oType->insert_id();
	}
	protected function _doModify($id , $aData){
		
		$oType = new adm_datafactory_model_type();
		return $oType->modifyById($id , $aData , $this->adm_id);
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$oType = new adm_datafactory_model_type();
		$oType->listByCondition($this->listField , $ths->$aConditionValue , $page , $pagesize , $sort , $order) or $this->redirect('busy');
		
		return $oType->get_data();
	}
	protected function _fetchListCount($aConditionValue){
		$oType = new adm_datafactory_model_type();

		$oType->countByCondition($aConditionValue) or $this->redirect('busy');
		return $oType->get_data();
	}
	protected function _opHtml($row){
		$html = '<a href="/datafactory/data/run?type='.$row['id'].'">'.L('set_data',true).'</a>';
		return $html;
	}
	protected function _doChange($id , $field , $value){
		$oType = new adm_datafactory_model_type();
		$aData = array(
			$field => $value
		);
		$oType->modifyById($id , $aData , $this->adm_id) or $this->output_json(APICODE_BUSY);
		return;
	}
}