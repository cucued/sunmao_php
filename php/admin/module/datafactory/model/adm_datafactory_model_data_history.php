<?php
class adm_datafactory_model_data_history extends Lib_datamodel_db{
    const STATUS_NORMAL = 1;
    const STATUS_PREPARE = 8;
    const STATUS_DEL = 9;
	const TABLE = 'adm_datafactory_data_history';
	/**
	 * 创建构造函数
	 *
	 */
    function __construct()
    {
        $this->_is_ctime = true;
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }

    private function dataEncode($data){
        return json_encode($data);
    }
    private function dataDecode($data){
        return json_decode($data , 1);
    }


    public function insertData($data_key , $title , $description , $aExtraData , $status , $adm_id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData = array(
            'data_key' => $data_key,
            'title' => $title,
            'description' => $description,
            'create_user' => $adm_id,
            'status' => $status,
            'data' => $aExtraData,
        );


        $this->table = self::TABLE;
        return $this->insert($aData);
    }
    public function modifyDataById($id , $title , $description , $aExtraData ,$status, $adm_id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData = array(
            'title' => $title,
            'description' => $description,
            'update_user' => $adm_id,
            'status' => $status,
            'data' => $aExtraData,
        );


        $this->table = self::TABLE;
        $where = 'id='.$id;
        return $this->update($aData , $where);
    }

    public function getById($id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
  	    return false;
  		$sql = 'select * from '.self::TABLE.' where id = '.((int)$id);
  		return $this->fetch_row($sql);
    }

    private function formatCondition($aCondition){
        foreach ($aCondition as $field => $value) {
            if($field == 'search'){
                $aConditionWhere[] = $this->escape($value['field']).' like "%'.$this->escape($value['key']).'%"';
            }else if(!empty($value)){
                $aConditionWhere[] = $field.'='.$value;
            }
        }
        return implode(' and ',$aConditionWhere);
    }
    public function listByCondition($aFields , $aCondition , $page , $pagesize = 10,$sort='id',$order='desc'){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition , $search_field , $search_key);
        

        $start = ($page-1)*$pagesize;
        $where = $where ? ' where'.$where : '';
        $sql = 'select id,'.implode(',',$aFields).' from '.self::TABLE
            .$where
            .' order by '.$sort.' '.$order
            .' limit '.$start.','.$pagesize;
        return $this->fetch($sql);
    }
    public function countByCondition($aCondition){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition);
        $this->table = self::TABLE;
        return $this->fetch_count($where);
    }


    protected function hook_after_fetch(){

        if($this->_data['data']){
            $this->_data['data'] = $this->dataDecode($this->_data['data']);
        }else if($this->_data[0]['data']){
            foreach ($this->_data as &$row) {
                $row['data'] = $this->dataDecode($row['data']);
            }
        }

    }
    
    protected function hook_before_write($array)
    {
        $array['data'] = $this->dataEncode($array['data']);
        return $array;
    }
}