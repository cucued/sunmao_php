<?php
class adm_datafactory_model_type extends Lib_datamodel_db{
    const STATUS_NORMAL = 1;
    const STATUS_PREPARE = 8;
    const STATUS_DEL = 9;
	const TABLE = 'adm_datafactory_type';
	/**
	 * 创建构造函数
	 *
	 */
    function __construct()
    {
        $this->_is_ctime = true;
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }

    public function insertType($aData , $adm_id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData['create_user'] = $adm_id;

        $this->table = self::TABLE;
        return $this->insert($aData);
    }
    public function modifyById($id , $aData , $adm_id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData['update_user'] = $adm_id;
        $where = 'id = '.$id;
        $this->table = self::TABLE;
        return $this->update($aData , $where , 1);
    }
    public function getById($id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
  	    return false;
  		$sql = 'select * from '.self::TABLE.' where id = '.((int)$id);
  		return $this->fetch_row($sql);
    }

    private function formatCondition($aCondition){
        if($aCondition){
            foreach ($aCondition as $field => $value) {
                if($field == 'search'){
                    $aConditionWhere[] = $this->escape($value['field']).' like "%'.$this->escape($value['key']).'%"';
                }else if(!empty($value)){
                    $aConditionWhere[] = $field.'='.$value;
                }
            }
            return implode(' and ',$aConditionWhere);
        }else{
            return '';
        }
    }
    public function listByCondition($aFields , $aCondition , $page , $pagesize = 10,$sort='id',$order='desc'){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition , $search_field , $search_key);
        

        $start = ($page-1)*$pagesize;
        $where = $where ? ' where'.$where : '';
        $sql = 'select * from '.self::TABLE
            .$where
            .' order by '.$sort.' '.$order
            .' limit '.$start.','.$pagesize;
        return $this->fetch($sql);
    }
    public function countByCondition($aCondition){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition);
        $this->table = self::TABLE;
        return $this->fetch_count($where);
    }

}