<?php
class adm_datafactory_model_data extends ml_model_datafactory{


    


    public function insertData($data_key , $title , $description , $aExtraData , $status , $adm_id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData = array(
            'data_key' => $data_key,
            'title' => $title,
            'description' => $description,
            'create_user' => $adm_id,
            'status' => $status,
            'data' => $aExtraData,
        );


        return $this->insert($aData);
    }
    public function modifyDataById($id , $aData , $adm_id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData['update_user'] = $adm_id;


        
        $where = 'id='.$id;
        return $this->update($aData , $where);
    }


    private function formatCondition($aCondition){
        foreach ($aCondition as $field => $value) {
            if($field == 'search'){
                $aConditionWhere[] = $this->escape($value['field']).' like "%'.$this->escape($value['key']).'%"';
            }else if(!empty($value)){
                
                $aConditionWhere[] = $field.'="'.$value.'"';
            }
        }
        return implode(' and ',$aConditionWhere);
    }
    public function listByCondition($aFields , $aCondition , $page , $pagesize = 10,$sort='id',$order='desc'){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition , $search_field , $search_key);
        

        $start = ($page-1)*$pagesize;
        $where = $where ? ' where '.$where : '';
        $sql = 'select * from '.$this->table
            .$where
            .' order by '.$sort.' '.$order
            .' limit '.$start.','.$pagesize;
        return $this->fetch($sql);
    }
    public function countByCondition($aCondition){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition);
        $this->table = $this->table;
        return $this->fetch_count($where);
    }



    public function backup($id ,$memo, $adm_id){
        $this->getById($id);
        $aData = $this->get_data();

        $aData['backup_user'] = $adm_id;
        $aData['backup_memo'] = $memo;
        $aData['backup_time'] = date('Y-m-d H:i:s');

        $this->table = $this->table.'_history';
        $this->insert($aData);
    }
}