<?php
class adm_datafactory_dataCompontent_freepage extends adm_datafactory_dataCompontent_basic{
	public function afterInsert($aParam){
		$oModel = new adm_datafactory_model_conpontent_freepage();
		
		return $oModel->replaceIndex($aParam['id'] , $aParam['data']['channel'], $aParam['data']['page_url']);
	}
	public function afterDelete($aParam){
		$oModel = new adm_datafactory_model_conpontent_freepage();
		return $oModel->deleteByPageDataId($aParam['id']);
	}
	public function afterUpdate($aParam){
		$oModel = new adm_datafactory_model_conpontent_freepage();
		if($aParam['status'] == adm_datafactory_model_data::STATUS_NORMAL){
			return $oModel->replaceIndex($aParam['id'] , $aParam['data']['channel'], $aParam['data']['page_url']);
		}
		return true;
	}
}