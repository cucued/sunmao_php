<?php

class adm_datafactory_data extends datafactory_controller{

	private $typeInfo = array();
	private $dataConfig;
	private $basicData = array('title','description','status');
	private $mixField = array();

	const COMPONTENT_AFTERINSERT = 'afterInsert';
	const COMPONTENT_AFTERUPDATE = 'afterUpdate';
	const COMPONTENT_AFTERDELETE = 'afterDelete';


	function _construct(){
		$type_id = (int)Tool_input::input('type');

		$oType = new adm_datafactory_model_type();
		$oType->getById($type_id) or $this->redirect('busy');
		$this->typeInfo = $oType->get_data();
		
		$this->tpl_data = array(
			'type_id' => $type_id,
		);
		$this->linkParam = 'type='.$type_id;

		$this->setDataDefine('datafactory_data');
		
		$this->filterField = array('status');
		$this->searchField = array('title' , 'description');
		$this->sortableField = array();
		$this->modifyInListField = array('status');
		$this->addButton(L('add',true),'add','/datafactory/data/create?type='.$type_id , true);
		$this->tpl_name = array(
			'create' => 'createData',
			'modify' => 'createData',
		);

		$this->listTitle = $this->typeInfo['title'];

		//合并出数据配置
		$dataDefinePath = $this->get_module_path().'/config/dataDefine/adm_df_define_'.$this->typeInfo['data_key'].'.xml';
		//检查权限
		$dataConfig_data = Tool_xmlReader::xml2array(file_get_contents($dataDefinePath));
		$this->check_power($dataConfig_data['option'][0]['power_id']);

		$dataConfig_common = Tool_xmlReader::xml2array(file_get_contents($this->get_module_path().'/config/adm_dataConfig_datafactory_data.xml'));
		$this->dataConfig = array(
			'option' => $dataConfig_data['option'],
			'field' => array_merge($dataConfig_data['field'] , $dataConfig_common['field']),
		);

		foreach ($dataConfig_data['field'] as $row) {
			if($row['type'] == 'mix'){
				$this->mixField[] = $row;
			}
		}

		$this->listField = array('title' , 'create_user' , 'status');
		if($dataConfig_data['option'][0]['listField']){
			$listField = explode(',' , $dataConfig_data['option'][0]['listField']);
			$this->listField = array_merge($this->listField , $listField);
		}
		if($dataConfig_data['option'][0]['dataForm']){
			$this->tpl_data['dataForm'] = explode(',' , $dataConfig_data['option'][0]['dataForm']);
		}
		

		if($dataConfig_data['morefield']){
			$oTool = new ml_lib_dataFactory_tool();

			foreach ($dataConfig_data['morefield'] as $row) {

				$aMorefield[] = array(
					'name' => $row['name'],
					'cn_name' => $row['cn_name'],
					'html' => $oTool->getMoreField($row),
				);
			}

			$this->tpl_data['morefield'] = $aMorefield;
		}


		
	}
	//覆盖数据配置加载方法
	protected function getDefineXml(){
		return $this->dataConfig;
	}
	protected function _fetchForModify($id){

		$oType = new adm_datafactory_model_data();
		$oType->getById($id) or $this->redirect('busy');
		$aData = $oType->get_data();
		
		$aExtraData = (array)$aData['data'];
		unset($aData['data']);

		$a = $aData+$aExtraData;
		$this->tpl_data['mix'] = json_encode($aExtraData['mix']);
		return $a;
	}
	protected function getMixInput(){
		
		$aMixName = array();
		$aMixValue = array();
		foreach ($this->mixField as $row) {
			$aMixName[] = $row['name'];
		}
		foreach (array_keys($_POST) as $k) {
			$a = explode('_',$k);
			if(count($a)<3)
				continue;
			if(in_array($a[0] , $aMixName)){
				$aMixValue[$a[0]][$a[2]][$a[0].'_'.$a[1]] = $_POST[$k];
			}
		}
		foreach ($aMixValue as &$row) {
			$row = array_values($row);
		}
		

		return $aMixValue;
	}
	protected function _doCreate($aData){
		
		if($this->mixField){
			$aData['mix'] = $this->getMixInput();
		}

		$oType = new adm_datafactory_model_data();
		foreach ($this->basicData as $row) {
			$basic[$row] = $aData[$row];
			unset($aData[$row]);	
		}
		$oType->insertData($this->typeInfo['data_key'], $basic['title'], $basic['description'], $aData , $basic['status'], $this->adm_id) or $this->output_json(APICODE_BUSY);
		$id = $oType->insert_id();

		$this->_callCompontent(self::COMPONTENT_AFTERINSERT , array('id' => $id , 'data' => $aData)) === false && $this->output_json(APICODE_BUSY) ;

		return $id;
	}
	protected function _doModify($id , $aData){
		if($this->mixField){
			$aData['mix'] = $this->getMixInput();
			
		}


		$backup = Tool_input::input('backup');
		$backup_memo = Tool_input::input('backup_memo');

		$oType = new adm_datafactory_model_data();

		if($backup){
			$oType->backup($id , $backup_memo , $this->adm_id);
		}

		foreach ($this->basicData as $row) {
			$basic[$row] = $aData[$row];
			unset($aData[$row]);	
		}
		
		$basic['data'] = $aData;

		$oType->modifyDataById($id, $basic , $this->adm_id) or $this->output_json(APICODE_BUSY);

		$basic['id'] = $id;
		$this->_callCompontent(self::COMPONTENT_AFTERUPDATE , $basic) === false && $this->output_json(APICODE_BUSY) ;
		return true;
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$aConditionValue['data_key'] = $this->typeInfo['data_key'];
		$oType = new adm_datafactory_model_data();
		$oType->listByCondition($this->listField , $aConditionValue , $page , $pagesize , $sort , $order) or $this->redirect('busy');
		
		$rows =  $oType->get_data();
		
		foreach ($rows as &$row) {
			$row = array_merge($row , (array)$row['data']);
		}
		return $rows;
	}
	protected function _fetchListCount($aConditionValue){
		$aConditionValue['data_key'] = $this->typeInfo['data_key'];
		$oType = new adm_datafactory_model_data();

		$oType->countByCondition($aConditionValue) or $this->redirect('busy');
		return $oType->get_data();
	}
	protected function _doChange($id , $field , $value){
		$oType = new adm_datafactory_model_data();
		$oType->getById($id) or $this->output_json(APICODE_BUSY);
		$aRow = $oType->get_data();
		if(!$aRow){
			$this->output_json(APICODE_HACK);
		}

		$aData = array(
			$field => $value
		);
		$oType->modifyDataById($id , $aData , $this->adm_id) or $this->output_json(APICODE_BUSY);
		if($field == 'status'){
			if($value == adm_datafactory_model_data::STATUS_NORMAL){
				$this->_callCompontent(self::COMPONTENT_AFTERINSERT , $aRow) === false && $this->output_json(APICODE_BUSY) ;
			}else{
				$this->_callCompontent(self::COMPONTENT_AFTERDELETE , array('id' => $id)) === false && $this->output_json(APICODE_BUSY) ;
			}
		}
		return;
	}

	private function _callCompontent($action , $param){
	
		if($this->dataConfig['option'][0]['withConpontent'] == 1){
			$class = 'adm_datafactory_dataCompontent_'.$this->typeInfo['data_key'];
		
			$o = new $class();
			
		
			return $o->$action($param);
		}
		return null;
	}
}