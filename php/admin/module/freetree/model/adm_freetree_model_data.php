<?php
class adm_freetree_model_data extends ml_model_freetree{
	
    
    
    private function _getKVByType($type){

        if(!$this->init_db())
            return false;

        $sql = 'select value,name from '.$this->table.' where type = "'.$type.'"';
        $rs = $this->fetch($sql);
        if(!$rs){
            return false;
        }
        
        $aValue2name = Tool_array::format_2d_array($this->get_data() , 'value' , Tool_array::FORMAT_VALUE2VALUE2,'name');
        $aValue2name[0] = '无';
        $this->set_data($aValue2name);
        return true;
    }
    

    static public function getKVByType($type){
        
        $oSelf = new adm_freetree_model_data();
        $rs = $oSelf->_getKVByType($type);
        if(!$rs){
            return $rs;
        }

        return $oSelf->get_data();
    }
    
}