<?php
class adm_freetree_index extends datafactory_controller{

	private $free_fields = array();
	private $type;
	function _construct(){
		$this->setListTypeTreeGrid();
		$this->treefield = 'name';

		$this->type = Tool_input::input('type');
		if(strpos($this->type , '_') !== false){
			list($type_module , $type_name) = explode('_',$this->type);
			$type_module = ADM_ROOT_PATH.'module/'.$type_module;
		}else{
			$type_module = $this->get_module_path();
			$type_name = $this->type;
		}



		//合并成数据定义
		$dataDefinePath = $type_module.'/config/dataDefine/adm_freetree_'.$type_name.'.xml';
		
		if(!is_file($dataDefinePath)){
			$this->redirect(404);
		}

		$dataConfig_data = Tool_xmlReader::xml2array(file_get_contents($dataDefinePath));
		$dataConfig_data = Tool_xmlReader::xml2array(file_get_contents($dataDefinePath));
		$option = $dataConfig_data['option'][0];
		if($option){
			$this->check_power($option['power_id']);
		}

		$oFdTool = new ml_lib_dataFactory_tool();
		$oFdTool->setDataConfig($dataConfig_data['field']);
		$this->free_fields = $oFdTool->getFieldNames();
		/*
		为了能在表单中使用SELECT决定PARENT_ID，需要把TYPE当成PARENT_ID REFERENCE数据获取方法的PARAM。由于EDITFORM和TOOL是拆分的，所以不能实现。如果在数据DEFINE中定义很不优雅，所以以下通过程序对数据定义做出修改，
		我自己都不明白我在说什么。
		反正这样就可以在表单中选择PARENT_ID了。
		 */
		$dataConfig_common = Tool_xmlReader::xml2array(file_get_contents($this->get_module_path().'/config/adm_dataConfig_freetree_basic.xml'));
		$dataConfig_common['field'] = Tool_array::format_2d_array($dataConfig_common['field'],'name',Tool_array::FORMAT_FIELD2ROW);
		$dataConfig_common['field']['parent_id']['reference'][0]['param'] = $this->type;
		
		$this->dataConfig = array(
			'field' => array_merge($dataConfig_common['field'] , $dataConfig_data['field']),
		);

		$this->linkParam = 'type='.$this->type;

		$this->listField = array('id','name','value','sort_num','parent_id','status');
		if($option['listField']){
			$this->listField = array_merge($this->listField , explode(',',$dataConfig_data['option'][0]['listField']));
		}
		$this->filterField = array('status');
		$this->searchField = array('name');
		$this->sortableField = array('id','sort_num');
		if($option['sortableField']){
			$this->sortableField = array_merge($this->sortableField , explode(',',$dataConfig_data['option'][0]['sortableField']));
		}
		$this->modifyInListField = array('status');

		// $this->listTitle = L('data_type' , true);
		$this->tpl_name = array(
			// 'list' => 'try',
		);
		
		$this->addButton(L('add',true),'add','/freetree/index/create?type='.$this->type,true);
	}
	
	//覆盖数据配置加载方法
	protected function getDefineXml(){
		return $this->dataConfig;
	}

	protected function _fetchForModify($id){
		$oType = new adm_freetree_model_data();
		$oType->getById($id) or $this->redirect('busy');
		return $oType->get_data();
	}
	private function _formatFreedata2Extra($aData){
		$aData['type'] = $this->type;
		
		foreach ($this->free_fields as $field) {
			$extra[$field] = $aData[$field];
			unset($aData[$field]);
		}
		$aData['extra'] = $extra;
		return $aData;
	}
	protected function _doCreate($aData){
		$oType = new adm_freetree_model_data();
		$aData = $this->_formatFreedata2Extra($aData);
		$oType->create($aData , $this->adm_id) or $this->output_json(APICODE_BUSY);
		return $oType->insert_id();
	}
	protected function _doModify($id , $aData){

		$oType = new adm_freetree_model_data();
		$aData = $this->_formatFreedata2Extra($aData);

		return $oType->modifyById($id , $aData , $this->adm_id);
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$oType = new adm_freetree_model_data();
		
		$aConditionValue['type'] = $this->type;
		$oType->listByCondition($this->listField , $aConditionValue , $page , $pagesize , $sort , $order) or $this->redirect('busy');
		
		
		return $oType->get_data();
	}
	protected function _fetchListCount($aConditionValue){
		$oType = new adm_freetree_model_data();

		$oType->countByCondition($aConditionValue) or $this->redirect('busy');
		return $oType->get_data();
	}
	protected function _opHtml($row){
		$html = '<a href="/datafactory/data/run?type='.$row['id'].'">'.L('set_data',true).'</a>';
		return $html;
	}
	protected function _doChange($id , $field , $value){
		$oType = new adm_freetree_model_data();
		$aData = array(
			$field => $value
		);
		$oType->modifyById($id , $aData , $this->adm_id) or $this->output_json(APICODE_BUSY);
		return true;
	}
}