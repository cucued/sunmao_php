<?php
	adm_tool_htmlMaker::html_header(); 
	
?>
<body>
	<h2>Basic TreeGrid</h2>
	<div class="demo-info">
		<div class="demo-tip icon-tip"></div>
		<div>TreeGrid allows you to expand or collapse group rows.</div>
	</div>
	<div style="margin:10px 0;"></div>
	<table title="Folder Browser" class="easyui-treegrid" style="width:700px;height:250px"
			data-options="
				url: 'aaa?type=channel',
				method: 'get',
				idField: 'id',
				treeField: 'name'
			">
		<thead>
			<tr>
				<th data-options="field:'name'" width="220">Name</th>
				<th data-options="field:'size'" width="100" align="right">Size</th>
				<th data-options="field:'date'" width="150">Modified Date</th>
			</tr>
		</thead>
	</table>

</body>
</html>