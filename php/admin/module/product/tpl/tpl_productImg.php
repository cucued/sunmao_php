<?php 
adm_tool_htmlMaker::html_header('XX'); 
?>
<style type="text/css">
	.ulImgArea { margin:0px; padding:0px; margin-left:20px; }
	.img-box{float:left; padding:5px; width:200px; height:230px;  margin: 2px; text-align: center;list-style: none;position: relative;}

	
	.img-box .left{position: absolute;;left: 0;top: 0;}
	.img-box .right{position: absolute;;right: 0;top: 0;}
	
</style>
<body product_id="<?php echo $product_id; ?>">

<div class="easyui-panel" style="width:100" title="<?php L('product_images'); ?>">
	<div class="datagrid-toolbar">
		<span class="bs-dtgrid-menu-span right-border">
			<a href="javascript:;" id="aShowUpload" class="easyui-linkbutton" iconCls="icon-add">上传</a>
			<a href="javascript:;" id="aSaveSort" class="easyui-linkbutton" iconCls="icon-save">保存排序</a><span id="spResult"></span>
		</span>
	</div>
	<ul id="divPicArea" class="ulImgArea">
		<?php 
		if($imgs){
		foreach ($imgs as $img) { ?>
		<li class="img-box <?php echo $productInfo['img_id'] == $img['img_id'] ?'bs_border_alarm':'bs_border_normal'; ?>" img_id="<?php echo $img['img_id']; ?>">
			<div height:200px;>
				<img src="<?php echo $img['url'] ?>" width="200px"/>
			</div>
				<span class="right"><a href="javascript:;" class="easyui-linkbutton aDel" iconCls="icon-cancel" plain="true"></a></span>
				<span style="position: absolute;left:0px;top:0px;"><a href="javascript:;" class="easyui-linkbutton aSetCover" iconCls="icon-ok" plain="true"></a></span>
				<div style="width: 100%;text-align: left;" class="datagrid-pager" >
					<?php L('upload_time') ?>:<?php echo $img['ctime']; ?>
				</div>
			
		</li>


		<?php }//foreach?>
		<?php
		}//if ?>
	</ul>
</div>




<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
</body>
<script type="text/javascript" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/js/jquery.dragsort-0.5.2.min.js"></script>
<script type="text/javascript">

	$('#divPicArea').css('height',window.parent.getWorkSpaceHeight());

	$('.aDel').click(function(){
		if(window.confirm('确认删除嘛？')){
			var delImgid = $(this).parent().parent().parent().attr('img_id')
			$.get(
				'/product/image/doDelImage?product_id='+$('body').attr('product_id')+'&img_id='+delImgid,
				'',
				function(data){
					var rs = bs_str2obj(data);
					if(rs.code == APICODE_SUCC){
						$(".img-box[img_id='"+delImgid+"']").css('display','none');
					}
				}
			);	
		}
	})
	$('.aSetCover').click(function(){
		var setCoverImgid = $(this).parent().parent().parent().attr('img_id')
			$.get(
				'/product/image/doSetCover?product_id='+$('body').attr('product_id')+'&img_id='+setCoverImgid,
				'',
				function(data){
					var rs = bs_str2obj(data);
					if(rs.code == APICODE_SUCC){
						$('.bs_border_alarm').removeClass('bs_border_alarm').addClass('bs_border_normal')
						
						$(".img-box[img_id='"+setCoverImgid+"']").removeClass('bs_border_normal').addClass('bs_border_alarm');
					}
				}
			);
	})



	$('#aShowUpload').click(function(){
		$('#divUpload').attr('callback','uploadImage_cb');
		$('#formUpload').form({
			url:'/product/image/doUpload?product_id='+$('body').attr('product_id')
		});
		$('#divUpload').dialog('open');
	})
	function uploadImage_cb(img_id,img_url){
		window.location.reload();
	}

	$("#divPicArea").dragsort({ dragSelector: "div", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class='img-box'></li>" });
		
	function saveOrder() {
		// var data = $("#divPicArea li").map(function() { return $(this).children().html(); }).get();
		// alert(data.join("|"));
	};
	$('#aSaveSort').click(function(){
		var data = $("#divPicArea li").map(function() { return $(this).attr('img_id'); }).get();
		;
		$.get('/product/image/doSort?product_id='+$('body').attr('product_id')+'&img_ids='+data.join(","),'',function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text(rs.msg).show().fadeOut(2000);
            }else{
              $('#spResult').css('color','red').text(rs.msg).show();
            }
		});
	})


</script>
<style type="text/css">
	
</style>
</html>