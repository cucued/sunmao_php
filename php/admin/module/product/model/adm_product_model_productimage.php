<?php
class adm_product_model_productimage extends Lib_datamodel_db{
	const STATUS_NORMAL = 1;
    const STATUS_DEL = 9;
    const TABLE = 'adm_product_productimage';


    function __construct()
    {
        $this->_is_ctime = true;
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
        $this->table = self::TABLE;
    }
    public function addImage($product_id , $img_id){
    	if(!$this->init_db($product_id , self::DB_MASTER))
            return false;
    	$aData = array(
    		'product_id' => $product_id,
    		'img_id' => $img_id,
    	);
        // $this->table = self::TABLE;
    	return $this->insert($aData);
    }
    public function delImage($product_id , $img_id){
        if(!$this->init_db($product_id , self::DB_MASTER))
            return false;
        $aData = array(
            'status' => self::STATUS_DEL,
        );
        $where = 'img_id = "'.$img_id.'"';
        return $this->update($aData , $where , 1);
    }

    public function countImagesByProductid($product_id){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $where = 'product_id = '.$product_id.' and status='.self::STATUS_NORMAL;
        return $this->fetch_count($where);
    }
    public function getImgidByProductid($product_id){
    	if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $sql = 'select img_id from '.self::TABLE.' where product_id = '.$product_id.' and status='.self::STATUS_NORMAL.' order by sort_num asc';
        return $this->fetch($sql);
    }
    public function setOrderByIdImgid($product_id , $aImgids){
        if(!$this->init_db($product_id , self::DB_MASTER))
            return false;

        $sql = 'update '.self::TABLE.' set sort_num=find_in_set(img_id,"'.implode(',',$aImgids).'") where product_id = '.$product_id.' and status='.self::STATUS_NORMAL;
        return $this->query($sql,false);
    }
}