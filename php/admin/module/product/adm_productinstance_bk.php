<?php
class adm_productinstance_bk extends admin_controller{
	protected function _construct(){
		$this->check_power(ADM_POWER_PRODUCTINSTANCE);
	}
	protected function getProductXml(){
		return Tool_xmlReader::xml2array(file_get_contents($this->get_module_path().'/config/adm_dataConfig_product.xml'));
	}
	public function run(){
		$aCondition = array('type','material','color','room');
		$aSearchField = array('product_code','product_name',);
		$aFields = array('product_code','product_name','type','material','color','room','price','quantity','sale_status','update_user');
		$aSortable = array('price','quantity');
		foreach ($aCondition as $field) {
			$aConditionValue[$field] = Tool_input::input($field,'get');
		}

		$dataConfig = $this->getProductXml();

		$oDfTool = new ml_lib_dataFactory_tool();
		$oDfTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oDfTool->setDataConfig($dataConfig['field']);
		$aName2Cn = $oDfTool->fields2cnname($aFields);
		$aSearchName2Cn = $oDfTool->fields2cnname($aSearchField);
		

		foreach ($aCondition as $field) {
			$aConditionOption[$field] = $oDfTool->getOptions($field);
		}


		$aOutput = array(
			'field2cn' => $aName2Cn,
			'searchField2cn' => $aSearchName2Cn,
			'conditionOptions' => $aConditionOption,
			'condition' => $aConditionValue,
			'aSortable' => $aSortable,
			'page_id' => 'run',
		);



		$this->output_html('productlist' , $aOutput);

	}
	public function listJson(){
		$aCondition = array('type','material','color','room');
		$aFields = array('id','product_code','product_name','type','material','color','room','price','sale_status','quantity','update_user','img_id');

		//排序 公用
		$sort = Tool_input::input('sort','get');
		if(!in_array($sort , $aFields)){ $sort = 'id'; }
		$order = Tool_input::input('order','get');
		if(!in_array($order , array('asc' , 'desc'))){$order = 'desc';}
		//search
		$search_field = Tool_input::input('search_field','get');
		if($search_field){
			$aConditionValue['search'] = array(
				'field' => $search_field,
				'key' => Tool_input::input('search_key','get'),
			);
		}
		//price
		$max_price = Tool_input::input('max_price','get');
		$min_price = Tool_input::input('min_price','get');
		if($max_price > 0 || $min_price > 0){
			$aConditionValue['price'] = array(
				'max' => $max_price,
				'min' => $min_price,
			);
		}
		//filter
		foreach ($aCondition as $field) {
			$aConditionValue[$field] = Tool_input::input($field,'get');
		}

		$oProduct = new adm_productinstance_model_product();
		$oProduct->listByCondition($aFields , $aConditionValue , 1,10,$sort,$order,$search_field,$search_key);
		$aData = $oProduct->get_data();
		

		$oProduct->countByCondition($aConditionValue,$search_field,$search_key);
		$total = $oProduct->get_data();

		//输出的格式化
		$dataConfig = $this->getProductXml();

		$oDfTool = new ml_lib_dataFactory_tool();
		$oDfTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oDfTool->setDataConfig($dataConfig['field']);
		$aDataOutput = $oDfTool->dataOutput($aFields,$aData);

		$aSaleStatusOptions = $oDfTool->getEnumOptions('sale_status');

		foreach ($aDataOutput as $key => &$value) {
			$value['op'].='<a href="javascript:;" onclick="bs_adm_callNewWorkTab(\''.L('modify',true).'\',\'/productinstance/index/modify?id='.$value['id'].'\')">'.L('edit',true).'</a>';
			$value['op'].=' <a href="javascript:;" onclick="bs_adm_callNewWorkTab(\''.L('set_image',true).'\',\'/productinstance/image/productImg?id='.$value['id'].'\')">'.L('set_image',true).'</a>';

			$value['img'].='<img class="imgProduct" src="'.imgsrv_model_upload::get_display_url($value['img_id']).'" width="30"/>';
			$value['sale_status'] = adm_tool_htmlMaker::select($aSaleStatusOptions , $value['sale_status_ori'],'','',' class="sel_saleStatus"onchange="changeSaleStatusById(this)" product_id='.$value['id']);
		}



		$this->output_easyuiJson($total , $aDataOutput);
	}

	public function create(){
		$dataConfig = $this->getProductXml();

		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditForm->setDataConfig($dataConfig['field']);


		$aOutput = array(
			'oEditForm' => $oEditForm,
			'page_id' => 'create',
		);
		$this->output_html('productCreate' , $aOutput);
	}
	public function doCreate(){
		
		$dataConfig = $this->getProductXml();

		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();

		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field , 'post');
		}


		$oChecker = new ml_lib_dataFactory_checker();
		$oChecker->setDataConfig($dataConfig['field']);
		$rs = $oChecker->check_multi($aFields , $aInput);

		if(!$rs){
			$this->output_json(APICODE_PARAM);
		}

		$oStaff = new adm_productinstance_model_product();
		$rs = $oStaff->createProduct($aInput , $this->adm_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__);
			$this->output_json(APICODE_SUCC);
		}
	}

	public function modify(){
		$product_id = Tool_input::input('id');
		$oProduct = new adm_productinstance_model_product();
		$oProduct->getByProductId($product_id);
		$aProduct = $oProduct->get_data();

		//生成表单
		$dataConfig = $this->getProductXml();
		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditForm->setDataConfig($dataConfig['field']);
		$oEditForm->setData($aProduct);


		$aOutput = array(
			'oEditForm' => $oEditForm,
			'id' => $product_id,
			'page_id' => 'modify',
		);
		$this->output_html('productModify' , $aOutput);
	}
	public function doModify(){
		$id = Tool_input::input('id');
		$dataConfig = $this->getProductXml();

		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();

		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field , 'post');
		}

		$oChecker = new ml_lib_dataFactory_checker();
		$oChecker->setDataConfig($dataConfig['field']);
		$rs = $oChecker->check_multi($aFields , $aInput);

		if(!$rs){
			$this->output_json(APICODE_PARAM);
		}

		$oStaff = new adm_productinstance_model_product();
		$rs = $oStaff->modifyProduct($aInput,$id,$this->adm_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__);
			$this->output_json(APICODE_SUCC);
		}
	}
	public function changeSaleStatusById(){
		$id = Tool_input::input('id','get');
		$sale_status = Tool_input::input('sale_status','get');

		$oProduct = new adm_productinstance_model_product();
		$oProduct->changeSaleStatusById($id , $sale_status , $this->adm_id);

		$this->output_json(APICODE_SUCC);
	}
}