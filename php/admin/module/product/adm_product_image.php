<?php

class adm_product_image extends admin_controller{
	public function productImg(){
		$product_id = Tool_input::input('id');

		$oProduct = new adm_product_model_product();
		$oProduct->getById($product_id);
		$productInfo = $oProduct->get_data();
		if(!$productInfo){
			$this->redirect(404);
		}



		$oImage = new adm_product_model_productimage();
		$oImage->getImgidByProductid($product_id);
		$aImgids = $oImage->get_data();

		if($aImgids){
			foreach ($aImgids as $row) {
				$img_id = $row['img_id'];

				$imginfo[$img_id] = array(
					'url' => imgsrv_model_upload::get_display_url($img_id),
					'ctime' => date('Y-m-d H:i',imgsrv_model_upload::get_image_upload_time($img_id)),
					'img_id' => $img_id,
				);
			}
		}

		$aData = array(
			'product_id' => $product_id,
			'productInfo' => $productInfo,
			'imgs' => $imginfo,
		);
		$this->output_html('productImg',$aData);
	}
	public function doUpload(){
		$isWaterMark = Tool_input::input('isWaterMark') == 1 ? true : false;
		$key = 'image';
		$product_id = Tool_input::input('product_id' , 'get');
		$oProduct = new adm_product_model_product();
		$oProduct->getById($product_id);
		$aData = $oProduct->get_data();
		if(!$aData){
			$this->output_json(APICODE_FAIL , 'product not exists');
		}



		$oUploader = new imgsrv_model_upload();
		if($oUploader->upload($key,$isWaterMark))
		{
			$img_id = $oUploader->get_result();
			
			$oImage = new adm_product_model_productimage();
			$oImage->addImage($product_id , $img_id);

			//update img count and cover
			$oImage->countImagesByProductid($product_id);
			$n = $oImage->get_data();
			$aData = array(
					'img_count' => $n,
				);
			if($n == 1){
				$aData['img_id'] = $img_id;
			}
			$oProduct->modifyById($product_id , $aData , $this->adm_id);


			$img_url = imgsrv_model_upload::get_display_url($img_id);
			$this->output_json(APICODE_SUCC , $img_url);
		}else{
			$error2msg = array(
				'size' => 'upload_image_size_max',
				'type' => 'upload_image_type_error',
			);

			$error = $oUploader->get_result();
			$this->output_json(APICODE_FAIL , L($error2msg[$error],true));
		}
	}
	public function doDelImage(){

		$product_id = Tool_input::input('product_id');
		$img_id = Tool_input::input('img_id');

		$oProduct = new adm_product_model_product();
		$oProduct->getById($product_id);
		$aProduct = $oProduct->get_data();

		$oImage = new adm_product_model_productimage();
		$oImage->delImage($product_id , $img_id);

		//update img count and cover
		$oImage->countImagesByProductid($product_id);
		$n = $oImage->get_data();
		$aData['img_count'] = $n;
		if($img_id == $aProduct['img_id']){
			$aImgids = $oImage->getImgidByProductid($product_id);
			$cover_img_id = $aImgids[0]['img_id'];
			$aData['img_id'] = $cover_img_id;
		}
		$oProduct->modifyById($product_id , $aData , $this->adm_id);


		$this->output_json(APICODE_SUCC);
	}
	public function doSetCover(){
		$product_id = Tool_input::input('product_id');
		$img_id = Tool_input::input('img_id');

		$aData = array(
			'img_id' => $img_id,
		);
		$oProduct = new adm_product_model_product();
		$oProduct->modifyById($product_id , $aData , $this->adm_id);

		$this->output_json(APICODE_SUCC);
	}
	public function doSort(){
		$id = Tool_input::input('product_id');
		$img_ids = Tool_input::input('img_ids');
		if(preg_match('/^[0-9a-zA-Z,]$/',$img_ids)){
			$this->output_json(APICODE_PARAM);
		}
		$aImgids = explode(',',$img_ids);
		$oImage = new adm_product_model_productimage();
		$oImage->setOrderByIdImgid($id , $aImgids) or $this->output_json(APICODE_BUSY);
		$this->output_json(APICODE_SUCC);
	}
}