<?php
class adm_product_index extends datafactory_controller{
	protected function _construct(){
		$this->check_power(ADM_POWER_PRODUCTINSTANCE);
		$this->setDataDefine('product');
		$this->listField = array('id','product_code','product_name','type','material','color','room','length','width','height','sale_status','img_id','price','quantity');
		$this->filterField = array('type' ,'type','material','color','room','sale_status');
		$this->searchField = array('product_code','product_name','product_name_eng');
		$this->sortableField = array('pub_time','price');
		$this->modifyInListField = array('sale_status');
		
		$this->setModel(new adm_product_model_product);

		$this->field2formaterInList = array(
			'img_id' => '_listPreviewImg',
		);
	}
	
	protected function _opHtml($row){
		return '<a onclick="bs_adm_callNewWorkTab(\'设置图片\',\'/product/image/productImg?id='.$row['id'].'\')" href="javascript:;">设置图片</a>';
	}
}