<?php
/**//**
 * @todo 自己不能设置自己的权限
 * @todo 高级权限
 * @package default
 */
class adm_company_staff extends adm_company_root{
	protected function _construct(){
		$this->check_power(ADM_POWER_STAFF);
	}
	public function run(){
		$gender = Tool_input::input('gender','get');
		if($gender > 0){
			$aConditon['gender'] = $gender;
		}
		$department = Tool_input::input('department','get');
		if($department > 0){
			$aConditon['department'] = $department;
		}
		$job = Tool_input::input('job','get');
		if($job > 0){
			$aConditon['job'] = $job;
		}

		$aFields = array('staff_id','name','alias','gender','mobile','department','job');

		$dataConfig = $this->getStaffXml();

		$oDfTool = new ml_lib_dataFactory_tool();
		$oDfTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oDfTool->setDataConfig($dataConfig['field']);
		$aName2Cn = $oDfTool->fields2cnname($aFields);

		$aGender = $oDfTool->getEnumOptions('gender');
		$aDepart = $oDfTool->getReferenceOptions('department');
		$aJob = $oDfTool->getReferenceOptions('job');




		$aOutput = array(
			'field2cn' => $aName2Cn,
			'genders' => $aGender,
			'departments' => $aDepart,
			'jobs' => $aJob,

			'condition' => $aConditon,
			'page_id' => 'run',
		);

		$this->output_html('stafflist' , $aOutput);
	}
	public function listJson(){
		$aFields = array('staff_id','name','alias','gender','mobile','department','job');
		
		$page = Tool_input::input('page' , 'get');
		$gender = Tool_input::input('gender','get');
		if($gender > 0){
			$aConditon['gender'] = $gender;
		}
		$department = Tool_input::input('department','get');
		if($department > 0){
			$aConditon['department'] = $department;
		}
		$job = Tool_input::input('job','get');
		if($job > 0){
			$aConditon['job'] = $job;
		}

		//get staff data
		$oStaff = new adm_company_model_staff();
		$oStaff->listStuffByConditionAnd($page , $aConditon);
		$aStaff = $oStaff->get_data();
		//total
		$oStaff->getCountByConditionAnd($aConditon);
		$total = $oStaff->get_data();
		//用来标识哪些职员未绑定登录名
		$aSid2aid = Tool_array::format_2d_array($aStaff,'staff_id',Tool_array::FORMAT_VALUE2VALUE2 , 'adm_id');
			
		
		//输出的格式化
		$dataConfig = $this->getStaffXml();

		$oDfTool = new ml_lib_dataFactory_tool();
		$oDfTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oDfTool->setDataConfig($dataConfig['field']);
		$aStaffOutput = $oDfTool->dataOutput($aFields,$aStaff);


		//多语言支持
		include($this->get_lang_path());
		$L = $LANG[$this->get_module_name()];

		foreach ($aStaffOutput as &$row) {
			$icoUnbind = empty($aSid2aid[$row['staff_id']]) ? '<span style="color:#ff0000;">!</span>' : '';
			$row['op'] = '<a href="/company/staff/edit?staff_id='.$row['staff_id'].'">'.$icoUnbind.$L['edit'].'</a> | <a href="/company/staffpower/?staff_id='.$row['staff_id'].'">'.$L['power'].'</a>';
		}

		$aOutput = array(
			'total' => $total,
			'rows' => $aStaffOutput
		);

		
		echo json_encode($aOutput);die;
	}
	public function doSavePower(){
		
		
		$staff_id = Tool_input::input('staff_id','post');
		$aPower = explode(',' , Tool_input::input('powers' , 'post'));

		$oPower = new adm_company_model_power();
		$rs = $oPower->setPowerByStaffid($staff_id , $aPower , $this->staff_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->output_json(APICODE_SUCC);
		}
	}

	public function create(){

		$dataConfig = $this->getStaffXml();

		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditForm->setDataConfig($dataConfig['field']);


		$aOutput = array(
			'oEditForm' => $oEditForm,

			'page_id' => 'create',
		);
		$this->output_html('staffcreate' , $aOutput);
	}
	public function doCreate(){
		$dataConfig = $this->getStaffXml();

		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();

		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field , 'post');
		}

		$oChecker = new ml_lib_dataFactory_checker();
		$oChecker->setDataConfig($dataConfig['field']);
		$rs = $oChecker->check_multi($aFields , $aInput);

		if(!$rs){
			$this->output_json(APICODE_PARAM);
		}

		$oStaff = new adm_company_model_staff();
		$rs = $oStaff->createStaff($aInput,$this->adm_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__);
			$this->output_json(APICODE_SUCC);
		}
	}
	
	public function edit(){
		$staff_id = Tool_input::input('staff_id','get');
		$oStaff = new adm_company_model_staff();
		$rs = $oStaff->getByStaffid($staff_id);
		$aData = $oStaff->get_data();
		if(empty($aData)){
			$this->redirect('/company/staff/','没有这个用户',3);
		}

		if($aData['adm_id']){
			$oLogin = new adm_basic_model_login();
			$oLogin->getByAdm_id($aData['adm_id']);
			$aLogin = $oLogin->get_data();

		}


		$dataConfig = $this->getStaffXml();

		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditForm->setDataConfig($dataConfig['field']);
		$oEditForm->setData($aData);


		$aOutput = array(
			'oEditForm' => $oEditForm,
			'staff_id' => $staff_id,
			'aLogin' => $aLogin,
			'page_id' => 'edit',
		);
		$this->output_html('staffedit' , $aOutput);
	}
	public function doEdit(){
		$staff_id = Tool_input::input('staff_id' , 'get');

		$dataConfig = $this->getStaffXml();

		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();

		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field , 'post');
		}

		$oChecker = new ml_lib_dataFactory_checker();
		$rs = $oChecker->check_multi($aFields , $aInput);



		$oStaff = new adm_company_model_staff();
		$rs = $oStaff->updateStaffByStaffId($aInput,$staff_id,$this->adm_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $staff_id);
			$this->output_json(APICODE_SUCC);
		}
	}
	public function doBindLogin(){
		$staff_id = Tool_input::input('staff_id','get');
		$login_name = Tool_input::input('login_name','post');

		$oLogin = new adm_basic_model_login();
		$oLogin->getByLoginname($login_name);
		$aLogin = $oLogin->get_data();

		if(empty($aLogin)){
			$this->output_json(APICODE_FAIL);
		}else{
			$oStaff = new adm_company_model_staff();
			$oStaff->setFieldsByStaffid($staff_id , array('adm_id' => $aLogin['adm_id']));

			$this->log_action(__CLASS__.':'.__FUNCTION__ , $staff_id);
			$this->output_json(APICODE_SUCC);
		}
	}
	
}