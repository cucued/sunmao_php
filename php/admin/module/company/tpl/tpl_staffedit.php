<?php adm_tool_htmlMaker::html_header(); ?>
<body>
<?php include($this->get_module_path().'/tpl/_tpl_nav.php'); ?>

<table>
	<tr>
		<td width="400px">
			<div class="easyui-panel" title="<?php L('edit');?>">
			<?php

				echo $oEditForm->getFormHtml($aEditField , '/company/staff/doEdit?staff_id='.$staff_id);
			?>
			</div>
		</td>
		<td style="vertical-align: top;">
			<div class="easyui-panel" title="<?php L('bindAccount');?>">
				<input id='iptLoginname' value="<?php echo $aLogin['login_name']; ?>">
				<input id="btnBindLogin" value="<?php L('submit');?>" type="button">
			</div>
		</td>
	</tr>

</table>

	
	
</body>
<?php echo $oEditForm->getJs('/company/staff/doEdit?staff_id='.$staff_id); ?>

<script type="text/javascript">
	$('#btnBindLogin').click(function(){
		if($('#iptLoginname').val() != ''){
			$.post(
				'/company/staff/doBindLogin?staff_id=<?php echo $staff_id; ?>',
				'login_name='+$('#iptLoginname').val(),
				function(){
					
			})
		}
	})
</script>
</html>