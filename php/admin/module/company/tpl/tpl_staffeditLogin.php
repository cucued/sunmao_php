<?php adm_tool_htmlMaker::html_header(); ?>
<body>
<form id="formCreateLogin" action="/company/staff/doEditLogin?staff_id=<?php echo $staff_id ?>" method="post">
<div class="easyui-panel" title="修改登录权限 :<?php echo $staffInfo['name']; ?>" style="width: 3oopx" data-options="tools:'#tt'">
	<table>
	
		<tr>
			<td>登录名:</td>
			<td><input name="login_name" type="text" class="easyui-validatebox" data-options="required:true" value="<?php echo $login_name; ?>" validType="length[6,20]"></td>
		</tr>
		<tr>
			<td>密码:</td>
			<td><input name="passwd" type="text" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="保存"><span id="spResult"></span></td>
		</tr>
	</table>
</div>
</form>
<div id="tt">
<a href="javascript:void(0)" status="<?php echo $status; ?>" class="icon-<?php echo $status==1?'ok':'cancel'; ?>" id="aStatus" onclick="javascript:;"></a>
</div>
</body>
<script type="text/javascript">
	
	$('#formCreateLogin').form({
		url:'/company/staff/doEditLogin?staff_id=<?php echo $staff_id; ?>',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text('修改成功！').show().fadeOut(2000);;
            }else{
              $('#spResult').css('color','red').text('修改失败，请与管理员联系！');
            }
		}
	});
		

		$('#aStatus').tooltip({
			position: 'bottom',
			content: 'aaaa',
		});

</script>
</html>