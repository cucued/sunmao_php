<?php adm_tool_htmlMaker::html_header(); ?>
<body style="margin: 10px;">
	<?php include($this->get_module_path().'/tpl/_tpl_nav.php'); ?>
	
	<table class="easyui-datagrid" id="tt" data-options="singleSelect:true,collapsible:true,url:'/company/staff/listJson?<?php echo http_build_query($condition); ?>',method:'get',pagination:true,pageSize:10,toolbar:'#dgMenu'">
		<thead>
			<tr>
			<?php foreach ($field2cn as $key => $value) {?>
				<th data-options="field:'<?php echo $key; ?>'"><?php echo $value; ?></th>
			<?php } ?>
				<th data-options="field:'op'"><?php L('edit'); ?></th>
			</tr>
		</thead>
	</table>
			
	<div id="dgMenu">
		<span class="bs-dtgrid-menu-span icon-filter bs-icon-left">
		<?php L('gender'); ?>: 
		<?php echo adm_tool_htmlMaker::select($genders , $condition['gender'],'selGender' , L('all',1)) ?>
		<?php L('department'); ?>: 
		<?php echo adm_tool_htmlMaker::select($departments , $condition['department'],'selDepart' , L('all',1)) ?>
		<?php L('job'); ?>: 
		<?php echo adm_tool_htmlMaker::select($jobs , $condition['job'],'selJob' , L('all',1)) ?>
		<input id="btnSelect" type="button" value="<?php L('select'); ?>">
		</span>
	</div>
			
<script type="text/javascript">
	$('#btnSelect').click(function(){
		
		var aCondition = Array();
		if($('#selGender').val()!="<?php L('all'); ?>"){
			aCondition.push('gender='+$('#selGender').val());
		}
		if($('#selDepart').val()!="<?php L('all'); ?>"){
			aCondition.push('department='+$('#selDepart').val());
		}
		if($('#selJob').val()!="<?php L('all'); ?>"){
			aCondition.push('job='+$('#selJob').val());
		}

		var sCondition = aCondition.join('&');
		
		$('#tt').datagrid({  
    		url:'/company/staff/listJson?'+sCondition,  
		});
		
	})

</script>


</body>
</html>