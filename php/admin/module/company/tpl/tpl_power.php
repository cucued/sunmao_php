<?php adm_tool_htmlMaker::html_header(); ?>
<body>
<table width="100%">
	<tr>
		<td>
			<?php if($staff_id){ ?>
			<div id="divEditPanel" class="easyui-panel" staff_id="<?php echo $staff_id; ?>" title="设置员工 <font color='red'><?php echo $staffInfo['name']; ?></font> 的权限：" style="padding: 10px;">

				<ul id="ulPower" class="easyui-tree" data-options="url:'/company/staffpower/powerJson?staff_id=<?php echo $staff_id; ?>',method:'get',animate:true,checkbox:true"></ul>
				<a id="btnSavePower" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'">保存</a><span id="spResult"></span>
			</div>
			<?php } ?>
		</td>
		<td width="200px" valign="top">
			<div class="easyui-panel" title="查找员工" style="padding: 10px;">
			输入员工编号：<input type="text" id="iptStaffid"><a id="btnSetStaffid" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'">查找</a>
			</div>
		</td>
	</tr>
</table>

</body>
<script type="text/javascript">
	$('#btnSetStaffid').click(function(){
		var staff_id = $('#iptStaffid').val();
		window.location.href='/company/staffpower/?staff_id='+staff_id;
	});


	$('#btnSavePower').click(function(){
		var b = $('#ulPower').tree('getChecked');
		var s = '';
		for(var i=0; i<b.length; i++){
        				if (s != '') s += ',';
        				s += b[i].power_id;//例如:菜单的menuID
    			}

    	staff_id = $('#divEditPanel').attr('staff_id');
    	$.post('/company/staffpower/doSavePower', 
    		{'staff_id':staff_id,'powers':s},
    		 function(data, textStatus, xhr) {
    			var rs = bs_str2obj(data);
	            if(rs.code == APICODE_SUCC){
	              $('#spResult').css('color','green').text('修改成功！').show().fadeOut(2000);
	            }else{
	              $('#spResult').css('color','red').text('修改失败，请与管理员联系！');
	            }
    	});
	})
</script>
</html>