<?php
class adm_company_personal extends adm_company_root{
	
	

	public function editInfo(){
		$dataConfig = $this->getStaffXml();

		$aFields = array('email' , 'mobile' , 'qq' , 'weixin' , 'gender' , 'birthday');
		
		$oStaff = new adm_company_model_staff();
		$rs = $oStaff->getByAdm_id($this->adm_id);
		if(!$rs){
			//
		}
		$aStaff = $oStaff->get_data();


		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditForm->setDataConfig($dataConfig['field']);
		$oEditForm->setData($aStaff);

		$aOutput = array(
			'aFields' => $aFields,
			'oEditForm' => $oEditForm,
		);


		$this->output_html('personal_editInfo' , $aOutput);
	}

	public function doEditInfo(){
		$aFields = array('email' , 'mobile' , 'qq' , 'weixin' , 'gender' , 'birthday');
		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field,'post');
		}

		$dataConfig = $this->getStaffXml();

		$oChecker = new ml_lib_dataFactory_checker();
		$oChecker->setDataConfig($dataConfig['field']);
		$rs = $oChecker->check_multi($aFields , $aInput);
		if(!$rs){
			$this->output_json(APICODE_PARAM);
		}

		$oStaff = new adm_company_model_staff();
		$rs = $oStaff->setFieldsByStaffid($this->staff_id , $aInput);
		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__);
			$this->output_json(APICODE_SUCC);
		}



	}
}