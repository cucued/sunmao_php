<?php
/**
 * 
 */
class adm_company_model_staff extends Lib_datamodel_db 
{
    const STATUS_NORMAL = 1;
    const STATUS_DEL = 9;
    const TABLE = 'adm_com_staff';
/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        $this->_is_ctime = true;
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }
    
    public function createStaff($aData , $adm_id){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $aData['create_user'] = $adm_id;
        $this->table = self::TABLE;
        return $this->insert($aData);
    }
    public function updateStaffByStaffId($aData , $staffId , $adm_id){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $aData['update_user'] = $adm_id;
        $this->table = self::TABLE;
        $where = 'staff_id='.$staffId;
        return $this->update($aData , $where);
    }
    public function getByStaffid($staffId){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $sql = 'select * from '.self::TABLE.' where staff_id = '.(int)$staffId;
        return $this->fetch_row($sql);
    }
    public function getByAdm_id($adm_id){
        if(!$this->init_db($adm_id , self::DB_SLAVE))
            return false;
        $sql = 'select * from '.self::TABLE.' where adm_id = '.(int)$adm_id;
        return $this->fetch_row($sql);
    }

    public function setFieldsByStaffid($staffId , $key2value){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $this->table = self::TABLE;
        return $this->update($key2value , 'staff_id = '.(int)$staffId , 1);
    }

    public function listStuffByConditionAnd($page=1 , $aCondition=array() , $pagesize = 10){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $start = (abs($page)-1)*$pagesize;

        $sWhere = $this->_formatCondition($aCondition);
        
        if(!empty($sWhere)){
            $sWhere = ' where ' . $sWhere;
        }
        $sql = 'select * from '.self::TABLE.$sWhere.' limit '.$start.','.$pagesize;
        return $this->fetch($sql);
    }
    public function getCountByConditionAnd($aCondition=array()){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $this->table = self::TABLE;
        $sWhere = $this->_formatCondition($aCondition);
        return $this->fetch_count($sWhere);
    }
    private function _formatCondition($aCondition){
        $sWhere = '';
        if(!empty($aCondition)){
            foreach ($aCondition as $key => $value) {
                $aConditionS[] = '`'.$key.'` = ' . (is_numeric($value) ? $value : '"'.$value.'"');
            }
            $sWhere = implode(' and ' , $aConditionS);
        }
        return $sWhere;
    }
}
