<?php
/**
 * 
 */
class adm_company_model_power extends Lib_datamodel_db 
{
    const STATUS_NORMAL = 1;
    const STATUS_DEL = 9;
    const TABLE = 'adm_staff_power';
    const POWER_GROUP_UNIT = 10000;
/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }
    
    
    public function getPowerListByStaffid($staffId){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        $sql = 'select power_id from '.self::TABLE.' where staff_id = '.(int)$staffId;
        if(!$this->fetch($sql)){
            return false;
        }
        
        $powerlist = Tool_array::format_2d_array($this->get_data() , 'power_id' , Tool_array::FORMAT_VALUE_ONLY);
        $this->set_data($powerlist);
        return true;
    }
    static public function powers2Groups($aPowers){
        $aGroup = array();
        foreach ($aPowers as $power) {
            $group = $power - ($power % self::POWER_GROUP_UNIT);
            $aGroup[] = $group;
        }

        return $aGroup;
    }

    public function setPowerByStaffid($staffId , $aPowers , $op_staff_id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $this->table = self::TABLE;

        if(!$this->delete('`staff_id` = '.$staffId))
        {
            return false;
        }

        foreach ($aPowers as $power_id) {
            $aData[] = array(
                'staff_id' => $staffId,
                'power_id' => $power_id,
                'op_staff_id' => $op_staff_id,
            );
        }
        $rs = $this->insert_multi($aData);
        return $rs;
    }

    public function isStaffHavePower($staffId , $powerId){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $this->table = self::TABLE;
        $where = 'staff_id='.$staffId.' and power_id='.$powerId;
        return $this->fetch_count($where);
    }
}