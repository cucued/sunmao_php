<?php
$LANG['company'] = array(
	'stuffs' => 'stuffs',
	'gender' => 'gender',
	'department' => 'department',
	'job' => 'job',
	'all' => 'all',
	'select' => 'select',
	'find' => 'find',
	'add' => 'add',
	'editLogin' => 'modify account',
	'createLogin' => 'create account',
	'power' => 'power',
	'edit' => 'edit',
	'submit' => 'save',
	'bindAccount' => 'bind account',
);