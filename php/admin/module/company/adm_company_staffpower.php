<?php
/**//**
 * @todo 自己不能设置自己的权限
 * @todo 高级权限
 * @package default
 */
class adm_company_staffpower extends admin_controller{
	protected function _constract(){
		$this->check_power(ADM_POWER_STAFF_POWER);
	}
	public function run(){
		

		$staff_id = Tool_input::input('staff_id' , 'get');

		$aOutput = array();

		if($staff_id){
			$oStaff = new adm_company_model_staff();
			$oStaff->getByStaffid($staff_id);
			$aStaffInfo = $oStaff->get_data();

			$aOutput['staff_id'] = $staff_id;
			$aOutput['staffInfo'] = $aStaffInfo;
		}

		
		$this->output_html('power' , $aOutput);
	}
	public function powerJson(){
		

		$staff_id = Tool_input::input('staff_id','get');

		$oPower = new adm_company_model_power();
		$oPower->getPowerListByStaffid($staff_id);
		$aPowerListNow = $oPower->get_data();

		$aMenuData = $this->_combine_menu_data();


		$aPowerJson = array();

		foreach ($aMenuData as $group) {

			if(empty($group['power_id'])){
				continue;
			}



			$aFolderInfo = array(
				'power_id' => $group['power_id'],
				'text' => $group['title'],
			);
			if(is_array($group['element'])){
				foreach ($group['element'] as $powerRow) {
					$aPower = array(
						'power_id' => $powerRow['power_id'],
						'text' => $powerRow['title'].' <a href="">xx</a>',
						'iconCls' => 'icon-blank',
						'checked' => in_array($powerRow['power_id'],$aPowerListNow)
					);
					$aFolderInfo['children'][] = $aPower;
				}
			}
			$aPowerJson[] = $aFolderInfo;
		}
		// var_dump($aFolderInfo);die;
		echo json_encode($aPowerJson);die;
	}
	public function doSavePower(){
		
		
		$staff_id = Tool_input::input('staff_id','post');
		$aPower = explode(',' , Tool_input::input('powers' , 'post'));

		$oPower = new adm_company_model_power();
		$rs = $oPower->setPowerByStaffid($staff_id , $aPower , $this->staff_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->output_json(APICODE_SUCC);
		}
	}
}