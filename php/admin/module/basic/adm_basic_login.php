<?php

class adm_basic_login extends admin_controller
{
	public $is_need_login = false;
	function run(){
		if($this->check_login()){
			$this->redirect('/home/');
		}

		$this->output_html('login' , array());
	}

	function doLogin(){

		$vcode = Tool_input::input('vcode','post');
		$sessVcode = $this->get_session('vcode');
		$this->set_session('vcode',mt_rand(10000,99999));
		if(!Tool_input::checkValidate($vcode , $sessVcode)){
			$this->output_json(APICODE_PARAM,'验证码不正确');
		}

		$login_name = Tool_input::input('login_name');
		$passwd = Tool_input::input('passwd');

		$oLogin = new adm_basic_model_adminer();
		$oLogin->checkLogin($login_name , $passwd);
		$userInfo = $oLogin->get_data();
		if(empty($userInfo)){
			$this->output_json(APICODE_FAIL,'用户不存在或已被冻结，请联系管理员');
		}else{
			$this->set_session('adm_id',$userInfo['id']);
			$this->adm_id = $userInfo['id'];
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $userInfo['adm_id']);
			$this->output_json(APICODE_SUCC);
		}
	}
	function logout(){
		$this->set_session('adm_id',0);
		$this->redirect('/');
	}
}