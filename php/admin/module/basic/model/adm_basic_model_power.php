<?php
/**
 * 
 */
class adm_basic_model_power extends Lib_datamodel_db 
{
    const STATUS_NORMAL = 1;
    const STATUS_DEL = 9;
    const TABLE = 'adm_admin_power';
    const POWER_GROUP_UNIT = 10000;
/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }
    
    
    public function getPowerListByadm_id($adm_id){
        if(!$this->init_db($adm_id , self::DB_SLAVE))
        {
            
            return false;
        }
        $sql = 'select power_id from '.self::TABLE.' where adm_id = '.(int)$adm_id;
        
        if(!$this->fetch($sql)){

            return false;
        }
        
        $powerlist = Tool_array::format_2d_array($this->get_data() , 'power_id' , Tool_array::FORMAT_VALUE_ONLY);
        $this->set_data($powerlist);
        return true;
    }
    static public function powers2Groups($aPowers){
        $aGroup = array();
        foreach ($aPowers as $power) {
            $group = $power - ($power % self::POWER_GROUP_UNIT);
            $aGroup[] = $group;
        }

        return $aGroup;
    }

    public function setPowerByadm_id($adm_id , $aPowers , $op_adm_id){
        if(!$this->init_db($adm_id , self::DB_MASTER))
            return false;
        $this->table = self::TABLE;

        if(!$this->delete('`adm_id` = '.$adm_id))
        {
            return false;
        }

        foreach ($aPowers as $power_id) {
            $aData[] = array(
                'adm_id' => $adm_id,
                'power_id' => $power_id,
                'op_adm_id' => $op_adm_id,
            );
        }
        $rs = $this->insert_multi($aData);
        return $rs;
    }

    public function isStaffHavePower($adm_id , $powerId){
        if(!$this->init_db($adm_id , self::DB_SLAVE))
            return false;

        $this->table = self::TABLE;
        $where = 'adm_id='.$adm_id.' and power_id='.$powerId;
        return $this->fetch_count($where);
    }
}