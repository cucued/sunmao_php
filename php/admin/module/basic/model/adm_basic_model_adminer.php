<?php

class adm_basic_model_adminer extends ml_lib_datamodel_common 
{
    var $table = 'adm_basic_adminer';
    const SALT = 'll0901';
/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        /**
         * 加载数据库配置
         */
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        /**
         * 构造函数
         * 参数：
         * 1，当前模型名称
         * 2，相关数据库配置
         */
        parent::__construct('admin' , $db_config);
    }
    
    public function checkLogin($loginName , $passwd)
    {
        if(!$this->init_db())
            return false;
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        $sql = 'select * from '.$this->table.' where login_name = "'.$this->escape($loginName).'" and password = "'.$salted_passwd.'" and status ='.self::STATUS_NORMAL;
        return $this->fetch_row($sql);
    }

    public function getByLoginname($login_name){
        if(!$this->init_db())
            return false;
        $sql = 'select * from '.$this->table.' where login_name = "'.$login_name.'"';
        return $this->fetch_row($sql);
    }
    public function listLoginname($page , $pagesize = 10 , $status = self::STATUS_NORMAL){
        if(!$this->init_db())
            return false;

        if($pagesize > 0){
            $start = ($page - 1) * $pagesize;
            $limit = ' limit '.$start.','.$pagesize;
        }
        $sql = 'select * from '.$this->table.' order by adm_id desc'.$limit;
        return $this->fetch($sql);
    }
    public function countAll(){
        if(!$this->init_db())
            return false;
        return $this->fetch_count('');
    }

    public function editPasswdByadm_id($passwd,$id,$loginName , $adm_id){
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        if(!$this->init_db())
            return false;
        $aData = array(
            'password'=>$salted_passwd,
            'update_user' => $adm_id,
        );
        return $this->update($aData,'id='.$id,1);
    }

    public function create($aData , $adm_id){
        $aData['password'] = $this->saltPasswd($aData['password'] , $aData['login_name']);

        $aData['create_user'] = $adm_id;
        if(!$this->init_db())
            return false;

        return $this->insert($aData);
    }
    public function modify($id , $aData , $adm_id){

        if(!$this->init_db())
            return false;

        $aData['update_user'] = $adm_id;
        
        $where = 'id = '.$id;

        return $this->update($aData,$where);
    }
    private function saltPasswd($passwd,$loginName){
        return md5($passwd.self::SALT.substr($loginName,1,3));
    }
    public function changeStatus($adm_id , $status){
        if(!$this->init_db())
            return false;
        $aData = array(
            'status' => $status,
        );
        
        return $this->update($aData , 'id = '.$adm_id);
    }
    public function editNick($id , $nick){
        if(!$this->init_db())
            return false;
        $aData = array(
            'nick' => $nick,
        );
        
        return $this->update($aData , 'id = '.$id);
    }
    static public function getLoginById($id){
        $oSelf = new adm_basic_model_login();
        $oSelf->getByAdm_id($id);
        $aData = $oSelf->get_data();
        return $aData['login_name'];
    }
}
?>