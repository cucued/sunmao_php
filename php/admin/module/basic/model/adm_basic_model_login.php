<?php

class adm_basic_model_login extends Lib_datamodel_db 
{
    const STATUS_NORMAL = 1;
    const STATUS_DEL = 9;
    const TABLE = 'adm_basic_login';
    const SALT = 'll0901';
/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        /**
         * 加载数据库配置
         */
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        /**
         * 构造函数
         * 参数：
         * 1，当前模型名称
         * 2，相关数据库配置
         */
        parent::__construct('admin' , $db_config);
    }
    
    public function checkLogin($loginName , $passwd)
    {
        if(!$this->init_db())
            return false;
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        $sql = 'select * from '.self::TABLE.' where login_name = "'.$this->escape($loginName).'" and password = "'.$salted_passwd.'" and status ='.self::STATUS_NORMAL;
        return $this->fetch_row($sql);
    }
    public function getByAdm_id($adm_id){
        if(!$this->init_db())
            return false;
        $sql = 'select * from '.self::TABLE.' where adm_id = '.(int)$adm_id;
        return $this->fetch_row($sql);
    }
    public function getByAdm_ids($adm_ids){
        if(!$this->init_db())
            return false;
        $sql = 'select * from '.self::TABLE.' where adm_id in ('.implode(',',$adm_ids).')';
        return $this->fetch($sql);
    }
    public function getByLoginname($login_name){
        if(!$this->init_db())
            return false;
        $sql = 'select * from '.self::TABLE.' where login_name = "'.$login_name.'"';
        return $this->fetch_row($sql);
    }
    public function listLoginname($page , $pagesize = 10 , $status = self::STATUS_NORMAL){
        if(!$this->init_db())
            return false;

        if($pagesize > 0){
            $start = ($page - 1) * $pagesize;
            $limit = ' limit '.$start.','.$pagesize;
        }
        $sql = 'select * from '.self::TABLE.' order by adm_id desc'.$limit;
        return $this->fetch($sql);
    }
    public function countAll(){
        if(!$this->init_db())
            return false;
        $this->table = self::TABLE;
        return $this->fetch_count('');
    }

    public function editPasswdByadm_id($passwd,$adm_id,$loginName){
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        if(!$this->init_db())
            return false;
        $this->table = self::TABLE;
        
        return $this->update(
            array('password'=>$salted_passwd),
            'adm_id='.$adm_id,
            1);
    }

    public function createLogin($loginName , $passwd , $nick){
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        if(!$this->init_db())
            return false;

        $aData = array(
            'login_name' => $loginName,
            'password' => $salted_passwd,
            'nick' => $nick,
        );
        $this->table = self::TABLE;
        return $this->insert($aData);
    }
    public function editLogin($adm_id , $loginName , $passwd){
        $salted_passwd = $this->saltPasswd($passwd , $loginName);

        if(!$this->init_db())
            return false;

        $aData = array(
            'login_name' => $loginName,
            'password' => $salted_passwd,
        );
        $this->table = self::TABLE;
        $where = 'adm_id = '.$adm_id;
        return $this->update($aData,$where);
    }
    private function saltPasswd($passwd,$loginName){
        return md5($passwd.self::SALT.substr($loginName,1,3));
    }
    public function changeStatus($adm_id , $status){
        if(!$this->init_db())
            return false;
        $aData = array(
            'status' => $status,
        );
        $this->table = self::TABLE;
        return $this->update($aData , 'adm_id = '.$adm_id);
    }
    public function editNick($adm_id , $nick){
        if(!$this->init_db())
            return false;
        $aData = array(
            'nick' => $nick,
        );
        $this->table = self::TABLE;
        return $this->update($aData , 'adm_id = '.$adm_id);
    }
    static public function getLoginById($id){
        $oSelf = new adm_basic_model_login();
        $oSelf->getByAdm_id($id);
        $aData = $oSelf->get_data();
        return $aData['login_name'];
    }
}
?>