<?php
include(SYS_INC_PATH.'3rd/phpQuery.php');
class adm_basic_adminer extends datafactory_controller{

	function _construct(){
		$this->setDataDefine('adminer');
		$this->listField = array('id','login_name','status');
		$this->filterField = array('status');
		$this->searchField = array('login_name','nick');
		$this->sortableField = array();
		$this->modifyInListField = array('status');

		
		
	}

	protected function _fetchForModify($id){
		$this->tpl_data['aEditField'] = array('login_name','nick','staff_id');
		$oArticle = new adm_basic_model_adminer();
		$oArticle->getById($id);
		return $oArticle->get_data();
	}
	protected function _doCreate($aData){
		$oArticle = new adm_basic_model_adminer();

		return $oArticle->create($aData,$this->adm_id) or $this->redirect('busy');
	}
	protected function _doModify($id , $aData){

		$oArticle = new adm_basic_model_adminer();
		
		return $oArticle->modify($id , $aData,$this->adm_id) or $this->redirect('busy');
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$oArticle = new adm_basic_model_adminer();
		$oArticle->listByCondition($this->listField,$aConditionValue,$page , $pagesize,$sort,$order);
		return $oArticle->get_data();

		$oArticle->countByCondition();
		$total = $oArticle->get_data();
	}
	protected function _fetchListCount($aConditionValue){
		$oArticle = new adm_cms_model_article();
		$oArticle->countByCondition($aConditionValue);
		return $oArticle->get_data();
	}
	protected function _opHtml($row){
		$html = '<a href="/basic/adminer/modifyPassword?id='.$row['id'].'">'.L('editPassword',true).'</a>
		<a href="/basic/adminerPower/?adm_id='.$row['id'].'">'.L('assignPermission',true).'</a>';

		return $html;
	}
	protected function _listPreviewImg($img_id){

		return '<img width="60" class="imgPreview" src="'.imgsrv_model_upload::get_display_url($img_id).'"/>';
	}
	protected function _doChange($id , $field , $value){

		$oArticle = new adm_basic_model_adminer();
		$oArticle->getById($id);
		$aArticle = $oArticle->get_data();
		if(empty($aArticle)){
			$this->output_json(APICODE_HACK , 'no article');
		}

		$aData = array(
			$field => $value
		);
		$oArticle->modify($id , $aData , $this->adm_id) or $this->output_json(APICODE_BUSY);

	

		return;
	}

	public function fetchWXContent(){
		$url = Tool_input::input('url');
		
		$content = file_get_contents($url);
		phpQuery::newDocumentHTML($content);
		
		$content = pq('#js_content')->htmlOuter();
		
		$content = preg_replace('/<img .*data-src="(.*)".*>/U','<img src="\\1">',$content);
		$this->output_json(APICODE_SUCC,'',$content);

	}
	public function modifyPassword(){
		$id = abs(Tool_input::input('id'));
		$aAdminer = $this->_fetchForModify($id);

		$dataConfig = $this->getDefineXml();
		$oEditForm = new ml_lib_dataFactory_editForm();
		$oEditForm->setDataConfig($dataConfig['field']);
		$oEditForm->setSaveUrl('/basic/adminer/doModifyPassword?id='.$id);
		
		$aData = array(
			'oEditForm' => $oEditForm,
			'action' => 'modify',
			'modifyTitle' => $aAdminer['login_name'].'的密码',
			'aEditField' => array('password'),
		);
		$this->output_html('public_modify',$aData,'basic');
	}
	public function doModifyPassword(){
		$id = abs(Tool_input::input('id'));
		$password = Tool_input::input('password');
		
		$aAdminer = $this->_fetchForModify($id);

		$oArticle = new adm_basic_model_adminer();
		$oArticle->editPasswdByadm_id($password , $id , $aAdminer['login_name'] , $this->adm_id);
		$this->output_json(APICODE_SUCC);
	}
}