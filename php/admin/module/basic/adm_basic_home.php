<?php

class adm_basic_home extends admin_controller
{
	function run(){
		
		//menu
		$aMenuData = $this->_combine_menu_data(); //Tool_xmlReader::xml2array(file_get_contents(ADM_ROOT_PATH.'/config/menu.xml'));

		$aMenuData = $this->_format_menu_data($aMenuData);
		

		$theme_dir = ADM_ROOT_PATH.'css/themes';

		$aTheme = Tool_os::readdir($theme_dir , 'dir');
		
		$aThemes = array_combine($aTheme , $aTheme);

		$oLogin = new adm_basic_model_login();
		$oLogin->getByAdm_id($this->adm_id);
		$aLogin = $oLogin->get_data();

		$aOutput = array(
			'menudata' => $aMenuData,
			'loginInfo' => $aLogin,
			'themes' => $aThemes,
		);
		$this->output_html('home' , $aOutput);
	}
	
	private function _format_menu_data($data){
		$aMenu = array();

		$oPower = new adm_basic_model_power();
		$oPower->getPowerListByadm_id($this->adm_id);
		$aPowerList = $oPower->get_data();
		
		$aGroup = adm_basic_model_power::powers2Groups($aPowerList);


		$title = $this->get_lang() == 'cn' ? 'title' : 'title_'.$this->get_lang();

		foreach ($data as $group) {

			if(!empty($group['power_id']) && !in_array($group['power_id'] , $aGroup)){


				continue;
			}

			$aSubMenu = array();
			foreach ($group['element'] as $element) {
				if(!empty($element['power_id']) && !in_array($element['power_id'] , $aPowerList)){
					continue;
				}

				

				$aSubMenu[] = array(
					'type' => $element['type'],
					'title' => $element[$title],
					'icon' => $element['icon'],
					'url' => $element['url'],
				);
				
			}
			$aMenu[] = array(
				'title' => $group[$title],
				'icon' => $group['icon'],
				'subMenu' => $aSubMenu,
			);
			
		}
		
		return $aMenu;
	}

	function doUploadImage(){

		$isWaterMark = Tool_input::input('isWaterMark') == 1 ? true : false;
		
		$key = 'image';
		$oUploader = new imgsrv_model_upload();
		if($oUploader->upload($key , $isWaterMark))
		{
			$img_id = $oUploader->get_result();
			$img_url = imgsrv_model_upload::get_display_url($img_id);
			$rsData = array(
				'img_id' => $img_id,
				'img_url' => $img_url,
			);
			$this->output_json(APICODE_SUCC ,'',$rsData);
		}else{
			$error2msg = array(
				'size' => 'upload_image_size_max',
				'type' => 'upload_image_type_error',
			);

			$error = $oUploader->get_result();

			$this->output_json(APICODE_FAIL , L($error2msg[$error],true));
		}
		
	}
	function doChangeTheme(){
		$theme = Tool_input::input('theme');
		if(!preg_match('/^[0-9a-z\-]+$/' , $theme)){
			$this->output_json(APICODE_FAIL);	
		}
		$this->set_session('theme',$theme);
		$this->output_json(APICODE_SUCC);
	}
}