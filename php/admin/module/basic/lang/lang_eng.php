<?php
$LANG['basic'] = array(
	'login' => 'login',
	'logout' => 'logout',
	'hello' => 'Hello,',
	'username' => 'login name',
	'password' => 'password',
	'vcode' => 'validator',
	'reload' => 'refresh',

	'adminManage' => 'adminer management',
	'login_name' => 'account',
	'nick' => 'nick',
	'power' => 'permission',

	'set' => 'set',
	'createAdmin' => 'create adminer',
	
	'assignPermission' => 'assign permission',
	'editPassword' => 'modify password',
	'editNick' => 'modify nick',
	'editLoginInfo' => 'modify login information',
	'setPower' => 'assign permission',

	'oldPasswd' => 'old password',
	'newPasswd' => 'new password',
	'rePasswd' => 'type again',

	
	
	'actions' => 'actions',
	'actionName' => 'action',
	'adminer' => 'admin account',

	
);