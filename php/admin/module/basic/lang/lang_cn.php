<?php
$LANG['basic'] = array(
	'login' => '登录',
	'logout' => '退出',
	'hello' => '你好,',
	'username' => '登录名',
	'password' => '密码',
	'vcode' => '验证码',
	'reload' => '刷新',

	'adminManage' => '管理后台管理员',
	'login_name' => '账号',
	'nick' => '昵称',
	'power' => '权限',

	'set' => '设置',
	'createAdmin' => '添加管理员',
	
	'assignPermission' => '分配权限',
	'editPassword' => '修改密码',
	'editNick' => '修改昵称',
	'editLoginInfo' => '修改登录信息',
	'setPower' => '设置权限',

	'oldPasswd' => '旧的密码',
	'newPasswd' => '新的密码',
	'rePasswd' => '确认密码',

	
	
	'actions' => '行为',
	'actionName' => '行为',
	'adminer' => '管理员',

	
);