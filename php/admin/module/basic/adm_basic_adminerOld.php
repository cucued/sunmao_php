<?php
class adm_basic_adminerOld extends admin_controller{
	
	public function run(){
		$this->output_html('adminerList');
	}
	public function runListJson(){
		include($this->get_lang_path());
		$l = $LANG[$this->get_module_name()];



		$oLogin = new adm_basic_model_login();
		$oLogin->listLoginname(1);
		$aLoginname = $oLogin->get_data();
		foreach ($aLoginname as &$row) {

			$row['status_show'] = $row['status'] == adm_basic_model_login::STATUS_NORMAL 
													? '<a href="javascript:;" adm_id="'.$row['adm_id'].'" onclick="changeStatus('.$row['adm_id'].');" class="adminStatus icon-ok bs-iconsp">'.$l['normal'].'</a>' 
													: '<a href="javascript:;" adm_id="'.$row['adm_id'].'" onclick="changeStatus('.$row['adm_id'].')" class="adminStatus icon-cancel bs-iconsp">'.$l['disable'].'</a>';
			$row['edit'] = '<a href="javascript:;" onclick="openEditWindow(\'/basic/adminer/editLogin?adm_id='.$row['adm_id'].'\')" class="bs-iconsp icon-edit">'.$l['set'].'</a>';
			$row['power'] = '<a href="javascript:;" onclick="openEditWindow(\'/basic/adminerPower/?adm_id='.$row['adm_id'].'\')" class="bs-iconsp icon-edit">'.$l['edit'].'</a>';
		}

		$oLogin->countAll();
		$total = $oLogin->get_data();

		return $this->output_easyuiJson($total , $aLoginname);
	}

	public function createLogin(){
		$adm_id = Tool_input::input('adm_id','get');
		$this->output_html('adminerCreateLogin' , $aOutput);
	}
	public function doCreateLogin(){
		$adm_id = Tool_input::input('adm_id','get');
		$login_name = Tool_input::input('login_name','post');
		$passwd = Tool_input::input('passwd','post');
		$nick = Tool_input::input('nick','post');

		$oLogin = new adm_basic_model_login();
		$rs = $oLogin->createLogin($login_name , $passwd, $nick);
		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $login_name);
			$this->output_json(APICODE_SUCC);
		}
	}

	public function editLogin(){
		$adm_id = Tool_input::input('adm_id','get');

		$oLogin = new adm_basic_model_login();
		$oLogin->getByAdm_id($adm_id);
		$aLogin = $oLogin->get_data();

		$aOutput = array(
			'adm_id' => $adm_id,
			'login_name' => $aLogin['login_name'],
			'nick' => $aLogin['nick'],
			'status' => $aLogin['status'],
		);
		$this->output_html('adminerEditLogin' , $aOutput);
	}
	public function doEditLogin(){
		$adm_id = Tool_input::input('adm_id','get');
		$login_name = Tool_input::input('login_name','post');
		$passwd = Tool_input::input('passwd','post');
		

		$oLogin = new adm_basic_model_login();
		$rs = $oLogin->editLogin($adm_id , $login_name , $passwd);
		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $adm_id);
			$this->output_json(APICODE_SUCC);
		}
	}
	public function doEditNick(){
		$adm_id = Tool_input::input('adm_id','get');
		$nick = Tool_input::input('nick','post');

		

		$oLogin = new adm_basic_model_login();
		$rs = $oLogin->editNick($adm_id , $nick);
		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $adm_id);
			$this->output_json(APICODE_SUCC);
		}
	}
	public function doChangeStatus(){
		$adm_id = Tool_input::input('adm_id','get');
		$status = Tool_input::input('status','get');

		$oLogin = new adm_basic_model_login();
		$rs = $oLogin->changeStatus($adm_id , $status);
		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $adm_id);
			$this->output_json(APICODE_SUCC);
		}
	}

	public function myEditPasswd(){
		$this->output_html('myEditPassword',array());
	}
	public function doMyEditPasswd(){

		$oldPasswd = Tool_input::input('oldPasswd');
		$passwd = Tool_input::input('passwd');

		$oLogin = new adm_basic_model_login();
		$oLogin->getByAdm_id($this->adm_id);
		$aInfo = $oLogin->get_data();
		$oLogin->checkLogin($aInfo['login_name'],$oldPasswd);
		$aCheckInfo = $oLogin->get_data();

		//
		if($aCheckInfo['adm_id'] !== $this->adm_id){
			$this->output_json(APICODE_PARAM);			
		}else{

			$rs = $oLogin->editPasswdByadm_id($passwd , $this->adm_id , $aInfo['login_name']);
			if(!$rs){
				$this->output_json(APICODE_FAIL);
			}else{
				$this->log_action(__CLASS__.':'.__FUNCTION__ , $adm_id);
				$this->output_json(APICODE_SUCC);
			}
		}
	}
}