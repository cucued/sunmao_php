<?php
/**//**
 * @todo 自己不能设置自己的权限
 * @todo 高级权限
 * @package default
 */
class adm_basic_adminerPower extends admin_controller{
	protected function _constract(){
		$this->check_power(ADM_POWER_STAFF_POWER);
	}
	public function run(){
		

		$adm_id = Tool_input::input('adm_id' , 'get');

		$aOutput = array();

		if($adm_id){
			$oLogin = new adm_basic_model_login();
			$oLogin->getByAdm_id($adm_id);
			$aLogin = $oLogin->get_data();

			$aOutput['adm_id'] = $adm_id;
			$aOutput['aLogin'] = $aLogin;
		}

		
		$this->output_html('power' , $aOutput);
	}
	public function powerJson(){
		

		$adm_id = Tool_input::input('adm_id','get');

		$oPower = new adm_basic_model_power();
		$oPower->getPowerListByadm_id($adm_id);
		$aPowerListNow = $oPower->get_data();
		if(empty($aPowerListNow)){
			$aPowerListNow = array();
		}
		$aMenuData = $this->_combine_menu_data();



		$aPowerJson = array();

		foreach ($aMenuData as $group) {

			if(empty($group['power_id'])){
				continue;
			}



			$aFolderInfo = array(
				'power_id' => $group['power_id'],
				'text' => $this->get_lang() == 'cn' ? $group['title'] : $group['title_eng'],
			);
			if(is_array($group['element'])){
				foreach ($group['element'] as $powerRow) {
					if($powerRow['type'] != 'link'){
						continue;
					}
					$aPower = array(
						'power_id' => $powerRow['power_id'],
						'text' => ($this->get_lang() == 'cn' ? $powerRow['title'] : $powerRow['title_eng']).' <a href="">xx</a>',
						'iconCls' => 'icon-blank',
						'checked' => in_array($powerRow['power_id'],$aPowerListNow)
					);
					$aFolderInfo['children'][] = $aPower;
				}
			}
			$aPowerJson[] = $aFolderInfo;
		}
		// var_dump($aFolderInfo);die;
		echo json_encode($aPowerJson);die;
	}
	public function doSavePower(){
		
		
		$adm_id = Tool_input::input('adm_id','post');
		$aPower = explode(',' , Tool_input::input('powers' , 'post'));

		$oPower = new adm_basic_model_power();
		$rs = $oPower->setPowerByadm_id($adm_id , $aPower , $this->adm_id);

		if(!$rs){
			$this->output_json(APICODE_FAIL);
		}else{
			$this->log_action(__CLASS__.':'.__FUNCTION__ , $adm_id);
			$this->output_json(APICODE_SUCC);
		}
	}
}