<?php

class adm_basic_actionReview extends admin_controller{
	private $dir;
	protected function _construct(){
		$this->dir = SERVER_ROOT_PATH.'/logs/action_log/';
	}
	public function run(){
		$date = Tool_input::input('date','get',date('Y/m/d'));
		$date = date('Y-m-d',strtotime($date));
		$adm_id = Tool_input::input('adm_id','get');

		$aFile = Tool_os::readdir($this->dir);

		$aDate = array();
		foreach ($aFile as $file) {
			$aDate[$file] = substr($file , 0 , 10);
		}

		$oLogin = new adm_basic_model_login();
		$oLogin->listLoginname(1,0);
		$aId2Nick = Tool_array::format_2d_array($oLogin->get_data(),'adm_id',Tool_array::FORMAT_VALUE2VALUE2 , 'nick');

		$aOutput = array(
			'dates' => $aDate,
			'id2nick' => $aId2Nick,
			'date' => $date,
			'adm_id' => $adm_id,
		);

		$this->output_html('actionReview_home' , $aOutput);
	}
	public function listJson(){
		$date = Tool_input::input('date','get');
		

		$adm_id = Tool_input::input('adm_id','get');

		$file = '';
		$aFile = Tool_os::readdir($this->dir);
		foreach ($aFile as $row) {
			if(substr($row , 0 , 10) == $date){
				$file = $row;
			}
		}

		$aLog = file($this->dir.$file);
		$total = count($aLog);
		


		$actions = $this->_readActionName();
		$nameKey = $this->get_lang() == 'cn' ? 'name' : 'eng_name';

		$aAdmId = array();
		foreach ($aLog as $line) {
			$aData = array();
			list($aData['datetime'],$aData['ip'],$aData['adm_id'],$aData['action'],$aData['url'],$aData['dest']) = explode('|' , $line);
			list($aData['date'],$aData['time']) = explode(' ' , $aData['datetime']);
			if(isset($actions[$aData['action']])){
				
				$aData['action'] = $actions[$aData['action']][$nameKey];
			}
			$aAdmId[$aData['adm_id']] = 1;
			if(($adm_id && $adm_id == $aData['adm_id']) || empty($adm_id)){
				$aRows[] = $aData;
			}
		}

		$aAdmId = array_filter(array_keys($aAdmId));

		$oLogin = new adm_basic_model_login();
		$oLogin->getByAdm_ids($aAdmId);
		$aId2Nick = Tool_array::format_2d_array($oLogin->get_data(),'adm_id',Tool_array::FORMAT_VALUE2VALUE2 , 'nick');

		foreach ($aRows as &$row) {
			$row['nick'] = $aId2Nick[$row['adm_id']];
		}

		$this->output_easyuiJson($total , $aRows);
	}

	private function _readActionName(){
		$aModule = Tool_os::readdir(ADM_ROOT_PATH.'/module');
		$allActions = array();
		foreach ($aModule as $module) {
			$pathAction = ADM_ROOT_PATH.'/module/'.$module.'/config/actionName.php';
			if(is_file($pathAction)){
				$actions = include($pathAction);
				$allActions = array_merge($allActions , $actions);
			}
		}
		return $allActions;
	}
}