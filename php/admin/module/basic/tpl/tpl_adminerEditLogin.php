<?php adm_tool_htmlMaker::html_header(); ?>
<body>

	<h4 class="bs-h4"><?php L('editLoginInfo'); ?></h4>
	<form id="formCreateLogin" action="/basic/adminer/doEditLogin?adm_id=<?php echo $adm_id ?>" method="post">
		<table>
			<tr>
				<td class="bs-tbKey"><?php L('login_name'); ?>:</td>
				<td><input name="login_name" type="text" class="easyui-validatebox" data-options="required:true" value="<?php echo $login_name; ?>" validType="length[6,20]"></td>
			</tr>
			<tr>
				<td class="bs-tbKey"><?php L('password'); ?>:</td>
				<td><input name="passwd" type="text" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></td>
			</tr>
			<tr>
				<td class="bs-tbKey"></td>
				<td><input type="submit" value="<?php L('submit'); ?>"><span id="spResult"></span></td>
			</tr>
		</table>
	</form>
	
	<br/>
	
	
	<h4 class="bs-h4"><?php L('editNick'); ?></h4>
	<form id="formEditNick" action="/basic/adminer/doEditLogin?adm_id=<?php echo $adm_id ?>" method="post">
		<table>
			<tr>
				<td class="bs-tbKey"><?php L('nick'); ?>:</td>
				<td><input name="nick" type="text" class="easyui-validatebox" data-options="required:true" validType="length[2,20]" value="<?php echo $nick; ?>"></td>
			</tr>
			<tr>
				<td class="bs-tbKey"></td>
				<td><input type="submit" value="<?php L('submit'); ?>"><span id="spResultNick"></span></td>
			</tr>
		</table>
	</form>
	
	
</body>
<script type="text/javascript">
	
	$('#formCreateLogin').form({
		url:'/basic/adminer/doEditLogin?adm_id=<?php echo $adm_id; ?>',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text(rs.msg).show().fadeOut(2000);;
            }else{
              $('#spResult').css('color','red').text(rs.msg);
            }
		}
	});
	$('#formEditNick').form({
		url:'/basic/adminer/doEditNick?adm_id=<?php echo $adm_id; ?>',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResultNick').css('color','green').text(rs.msg).show().fadeOut(2000);;
            }else{
              $('#spResultNick').css('color','red').text(rs.msg);
            }
		}
	});

</script>
</html>