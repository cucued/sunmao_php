<?php adm_tool_htmlMaker::html_header(); ?>
<body style="margin: 10px;">
	
	<div id="divEditWin" class="easyui-window" title="&nbsp;" closed="true" minimizable:"false" maximizable:"false" style="width:400px;height: 400px;padding:0px;" edit_id="">
	<iframe src="" id="ifrEdit" width="95%" height="350px" frameborder="0" scrolling="auto"></iframe>
	</div>

	<table class="easyui-datagrid" title="<?php L('adminManage'); ?>" style="width:700px;height:250px"
			data-options="singleSelect:true,collapsible:true,url:'/basic/adminer/runListJson',method:'get',pagination:true,pageSize:10">
		<thead>
			<tr>
				<th data-options="field:'adm_id',width:50">ID</th>
				<th data-options="field:'login_name',width:100"><?php L('login_name'); ?></th>
				<th data-options="field:'nick',width:100"><?php L('nick'); ?></th>
				<th data-options="field:'status_show',width:100"><?php L('status'); ?></th>
				<th data-options="field:'power',width:80"><?php L('power'); ?></th>
				<th data-options="field:'edit',width:540"><?php L('edit'); ?></th>
				
			</tr>
		</thead>
	</table>

	

</body>
	<?php 
		$lang = array(
			'normal' => 'normal',
			'disable' => 'disable'
		);
		adm_tool_htmlMaker::js_lang($lang); 
	?>
	<script type="text/javascript">
	function changeStatus(id){
		var dom=$(".adminStatus[adm_id='"+id+"']");
		if(dom.text() == L.normal){
			$.get('/basic/adminer/doChangeStatus?adm_id='+id+'&status=9','',function(data){
				dom.text(L.disable);
				dom.removeClass('icon-ok').addClass('icon-cancel');
			})
		}else if(dom.text() == L.disable){
			$.get('/basic/adminer/doChangeStatus?adm_id='+id+'&status=1','',function(data){
				dom.text(L.normal);
				dom.removeClass('icon-cancel').addClass('icon-ok');
			})
		}
	}

	$win = $('#divEditWin').window(
	{ 
	    minimizable: false,
	    maximizable: false, 
	   collapsible: false, });


	function openEditWindow(url){
		$('#ifrEdit').css('display','none').attr('src',url);
		$('#divEditWin').window('open');
		$('#ifrEdit').load(function(){
			$('#ifrEdit').css('display','block');
		})
		
	}
	
	</script>
</html>
