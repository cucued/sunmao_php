<?php adm_tool_htmlMaker::html_header(); ?>
<body>
<form id="formCreateLogin" action="/basic/adminer/doCreateLogin?staff_id=<?php echo $staff_id ?>" method="post">
<div class="easyui-panel" title="<?php L('createAdmin'); ?>:">
	<table>
	
		<tr>
			<td><?php L('login_name'); ?>:</td>
			<td><input name="login_name" type="text" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></td>
		</tr>
		<tr>
			<td><?php L('password'); ?>:</td>
			<td><input name="passwd" type="text" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></td>
		</tr>
		<tr>
			<td><?php L('nick'); ?>:</td>
			<td><input name="nick" type="text" class="easyui-validatebox" data-options="required:true" validType="length[2,20]"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="保存"><span id="spResult"></span></td>
		</tr>
	
	</table>
</div>
</form>
</body>
<script type="text/javascript">
	
	$('#formCreateLogin').form({
		url:'/basic/adminer/doCreateLogin?staff_id=<?php echo $staff_id; ?>',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text('修改成功！').show().fadeOut(2000);;
            }else{
              $('#spResult').css('color','red').text('修改失败，请与管理员联系！');
            }
		}
	});
	

</script>
</html>