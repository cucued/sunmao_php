
<?php adm_tool_htmlMaker::html_header(); ?>
<body class="easyui-layout">
	<div data-options="region:'north',border:false" style="height:60px;background:#B3DFDA;padding:5px">
		
		<div style="width:50px;float: left;">
			<img src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/admin/images/bhhzlogo.png">
		</div>
		<div style="float:right;width:200px;text-align: right;padding: 7px;line-height: 20px;overflow: hidden;">
			<?php echo adm_tool_htmlMaker::select($themes , $theme , 'selTheme'); ?><br/>
			<?php echo $L['hello'];?><?php echo $loginInfo['nick']; ?>
			<a href="/login/logout"><?php L('logout');?></a>
			
		</div>
	</div>
	<div data-options="region:'west',split:true,title:'&nbsp;'" style="width:150px;padding:0px;">
		<div id="divAccordion" class="easyui-accordion" style="width:143px;height:500px;border-right: 0px;border-bottom: 0px;">
			<?php foreach ($menudata as $menu_group) { ?>
			<div title="<?php echo $menu_group['title'] ?>" data-options="iconCls:'icon-<?php echo $menu_group['icon'] ?>'" style="padding:10px;">
				<ul class="bs-adm-menu">
				<?php foreach ($menu_group['subMenu'] as $element) {
					if($element['type'] == 'link'){
				?>
				<li><i class="fa fa-camera-retro"></i><a href="javascript:;" class="aMenuLink" ihref="<?php echo $element['url']; ?>" title="<?php echo $element['title']; ?>"><?php echo $element['title']; ?></a></li>
				<?php }elseif($element['type'] == 'line'){?>
				<hr/>
				<?php }//if
				}//foreach ?>
				</ul>
			</div>
			<?php } ?>
		</div>


	</div>
	<!--<div data-options="region:'east',collapsible:false,title:'消息'" style="width:50px;padding:10px;"></div>

	<div data-options="region:'south',border:false" style="height:30px;background:#A9FACD;padding:0px;">
		<div id="quotation" style="height: 20px;overflow: hidden;line-height: 20px;">
			<a href="javascript:;" id="aNoticePre" style="width:50px;float:left;margin-top: 5px;"><<</a>
			<ul style="list-style: none;margin-top: 5px;width:300px;">
				<li>aaaaaaaa...</li>
				<li>bbbbbbbbbb...</li>
				<li>cccccccccccccc...</li>
			</ul>
			<span style="float: right;width:100px;"></span>
		</div>
	</div>
	-->

	</div>
	<div data-options="region:'center'">
		<div class="easyui-tabs" id="divWordTabs">
		</div>
	</div>
</body>
<script type="text/javascript">
	var iframeHeight = $(window).height()-130;
	function getWorkSpaceHeight(){
		return iframeHeight-110;
	}
	function openWorkTab(title,url){
		$('#divWordTabs').tabs('add',{
		    title:title,
		    content:'<iframe src="'+url+'" style="border:0px;width:100%;height:'+iframeHeight+'px"></iframe>',
		    closable:true,
		});
	}
	
	$('#divAccordion').css('height' , iframeHeight);
	
	$('#aNoticePre').click(function(){
		var $ul = $("#quotation ul");
		$ul.find("li:eq(2)").hide();
		$ul.find("li:first").fadeOut(300, function() {
			$ul.find("li:first").appendTo($ul).show();
			$ul.find("li:first").fadeIn(300);
		});
		
	});
	$(function(){
		var scrtime;
		$("#quotation").hover(function(){
			clearInterval(scrtime);
		
		},function(){
		
		scrtime = setInterval(function(){
			var $ul = $("#quotation ul");
			
			$ul.find("li:first").fadeOut(1000,function(){
				$ul.find("li:last").prependTo($ul).hide();
				$ul.find("li:first").fadeIn(1000);
			});	
		},4000);  //设置滚动时间 这里是毫秒
		
		}).trigger("mouseleave");
	});

	//
	$('.aMenuLink').click(function(){
		
		var menuLink = $(this);
		$('#divWordTabs').tabs('add',{
		    title:menuLink.attr('title'),
		    content:'<iframe src="'+menuLink.attr('ihref')+'" style="border:0px;width:100%;height:'+iframeHeight+'px"></iframe>',
		    closable:true,
		});
	})
	$('#selTheme').change(function(){
		$.get('/basic/home/doChangeTheme?theme='+$('#selTheme').val(),'',function(){
			window.location.reload();
		})
	});
</script>
</html>
