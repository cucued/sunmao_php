<?php adm_tool_htmlMaker::html_header(); ?>
<body>
		<h4 class="bs-h4" adm_id="<?php echo $adm_id; ?>"><?php L('setPower'); ?></h4>
		<ul id="ulPower" class="easyui-tree" data-options="url:'/basic/adminerPower/powerJson?adm_id=<?php echo $adm_id; ?>',method:'get',animate:true,checkbox:true"></ul>
		<a id="btnSavePower" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save'"><?php L('submit'); ?></a><span id="spResult"></span>
	
</body>
<script type="text/javascript">

	$('#btnSavePower').click(function(){
		var b = $('#ulPower').tree('getChecked');
		var s = '';
		for(var i=0; i<b.length; i++){
        				if (s != '') s += ',';
        				s += b[i].power_id;//例如:菜单的menuID
    			}

    	adm_id = $('.bs-h4').attr('adm_id');
    	$.post('/basic/adminerPower/doSavePower', 
    		{'adm_id':adm_id,'powers':s},
    		 function(data, textStatus, xhr) {
    			var rs = bs_str2obj(data);
	            if(rs.code == APICODE_SUCC){
	              $('#spResult').css('color','green').text(rs.msg).show().fadeOut(2000);
	            }else{
	              $('#spResult').css('color','red').text(rs.msg);
	            }
    	});
	})
</script>
</html>