<?php adm_tool_htmlMaker::html_header(); ?>
<body>
  <div class="easyui-panel" title="<?php L('login'); ?>" style="width:400px">
    <div style="padding:10px 0 10px 60px">
      <form id="ff" method="post">
        <table>
          <tr>
            <td><?php L('username'); ?>:</td>
            <td><input class="easyui-validatebox" type="text" name="login_name" data-options="required:true"></input></td>
          </tr>
          <tr>
            <td><?php L('password'); ?>:</td>
            <td><input class="easyui-validatebox" type="password" name="passwd" data-options="required:true"></input></td>
          </tr>
          <tr>
            <td><?php L('vcode'); ?>:</td>
            <td><input class="easyui-numberbox" type="text" name="vcode" data-options="required:true"></input></td>
          </tr>
          <tr>
            <td></td>
            <td><img src="/validate/" alt="<?php L('vcode'); ?>" id="validate"><a href="javascript:;" id="revalidate"><?php L('reload'); ?></a></td>
          </tr>
        </table>
      </form>
      </div>
      <div style="text-align:center;padding:5px">
        <a href="javascript:void(0)" class="easyui-linkbutton" id="btnSubmit"><?php L('login'); ?></a>
        <span id="spanAlert" color="Red" style="width:100px;color:#ff0000">&nbsp;</span>
      </div>
  </div>
  <script>
    //验证码
    $('#revalidate').click(function(){
      $('#validate').attr('src','/validate/?'+Math.random());
    })
    

    $('#btnSubmit').click(function(){
      $('#ff').form('submit', {
        url:'/login/doLogin',
        onSubmit: function(){
            if($('#ff').form('validate') == false){
              return false;
            }
          return true;
        },
        success:function(data){
            var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              window.location.href='/home/';
            }else{
              $('#revalidate').trigger("click");
              $('#spanAlert').text(rs.msg);
            }
        }
      });
    })
  </script>
</body>
</html>