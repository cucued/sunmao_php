<?php adm_tool_htmlMaker::html_header(); ?>
<body>
	<form id="ff" action="/personal/doEditPasswd" method="post">
		<table>
			<tr>
				<td><?php L('oldPasswd'); ?>:</td>
				<td><input name="oldPasswd" type="password" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></input></td>
			</tr>
			<tr>
				<td><?php L('newPasswd'); ?>:</td>
				<td><input id="iptPasswd" name="passwd" type="password" class="easyui-validatebox" data-options="required:true" validType="length[6,20]"></input></td>
			</tr>
			<tr>
				<td><?php L('rePasswd'); ?>:</td>
				<td><input name="repass" type="password" class="easyui-validatebox  easyui-tooltip" data-options="required:true" validType="equalTo['#iptPasswd']"></input></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="<?php L('submit'); ?>"></input><span id="spResult"></span></td>
			</tr>
		</table>
	</form>
</body>
<script type="text/javascript">

	$('#ff').form({
		url:'/basic/adminer/doMyEditPasswd',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text(rs.msg).show().fadeOut(2000);;
            }else{
              $('#spResult').css('color','red').text(rs.msg);
            }
		}
	});

</script>
</html>