<?php adm_tool_htmlMaker::html_header(); ?>
<body>
	<table width="100%"><tr>
			<td>
				<table class="easyui-datagrid" title="<?php L('actions'); ?> <?php echo $date; ?>" width="100%" style="height:500px"
						data-options="singleSelect:true,collapsible:true,url:'/basic/actionReview/listJson?adm_id=<?php echo $adm_id; ?>&date=<?php echo $date; ?>',method:'get'">
					<thead>
						<tr>
							<th data-options="field:'date',width:80"><?php L('date'); ?></th>
							<th data-options="field:'time',width:80"><?php L('time'); ?></th>
							<th data-options="field:'nick',width:100"><?php L('nick'); ?></th>
							<th data-options="field:'action',width:200,align:'right'"><?php L('actionName'); ?></th>
							
						</tr>
					</thead>
				</table>
			</td>
			<td valign="top" width="200px">
				<div id="cc" class="easyui-calendar" style="width:200px;height:180px;"></div>
				<br/>
				<div id="p" class="easyui-panel" title="<?php L('adminer'); ?>" style="width:200px;height:200px;padding:10px;">
					<ul>
						<li><a href="/basic/actionReview/?date=<?php echo date('Y/m/d',strtotime($date)) ?>"><?php L('all'); ?></a></li>
						<?php foreach ($id2nick as $id => $nick) { ?>
						<li><a href="/basic/actionReview/?date=<?php echo date('Y/m/d',strtotime($date)) ?>&adm_id=<?php echo $id; ?>"><?php echo $nick; ?></a></li>
						<?php } ?>
					</ul>
				</div>

			</td>

	</tr></table>
</body>

<script type="text/javascript">
	var dt = new Date();
	dt.setYear('<?php echo date('Y',strtotime($date)); ?>');
	dt.setMonth('<?php echo date('n',strtotime($date))-1; ?>');
	dt.setDate('<?php echo date('d',strtotime($date)); ?>');
	$('#cc').calendar({
	    current:dt
	});
	$('#cc').calendar({
		onSelect: function(date){
			var ymd = date.toLocaleDateString();
			window.location.href="/basic/actionReview/?date="+ymd+"&adm_id=<?php echo $adm_id; ?>";
		}
	});


	$('#btnSubmit').click(function(){
		var aCondition = Array();
		if($('#selDate').val()!="<?php L('all'); ?>"){
			aCondition.push('file='+$('#selDate').val());
		}
		if($('#selAdmid').val()!="<?php L('all'); ?>"){
			aCondition.push('adm_id='+$('#selAdmid').val());
		}

		var sCondition = aCondition.join('&');
		

		window.location.href="?"+sCondition;
		
	})
</script>
</html>