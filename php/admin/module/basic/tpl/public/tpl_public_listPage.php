<?php
	adm_tool_htmlMaker::html_header(); 
	
?>

<body style="margin: 10px;" url_base="<?php echo $url_base; ?>" linkParam="<?php echo $linkParam; ?>">


<table class="easyui-<?php echo $list_type; ?>" id="tt" title="<?php echo $listTitle; ?>" data-options="singleSelect:false,selectOnCheck:true,collapsible:true,url:'<?php echo $list_url; ?>',method:'get',pagination:true,pageSize:10,toolbar:'#dgMenu'<?php if($list_type=='treegrid'){echo ',treeField:\''.$treefield.'\',idField:\'id\'';} ?>">
	<thead>
		<tr>
			<th field="ck" checkbox="true"></th>
			<th data-options="field:'id'"><?php L('id'); ?></th>
			
		<?php foreach ($field2name as $key => $value) {
				$sortSign = in_array($key , $aSortable) ? ' sortable="true"' : '';
			?>
			<th data-options="field:'<?php echo $key; ?>'"<?php echo $sortSign; ?>><?php echo $value; ?></th>
		<?php } ?>
			<th data-options="field:'op'"><?php L('edit'); ?></th>
		</tr>
	</thead>
</table>

<div id="dgMenu" style="padding:5px;height:auto">
	
	<div class="bs-dtgrid-menu-span">
		<?php if(count($buttons)){ ?>
		<span class="bs-dtgrid-menu-span right-border">
			<?php foreach ($buttons as $row) { ?>
			<a href="<?php echo $row['href']; ?>" class="easyui-linkbutton" iconCls="icon-<?php echo $row['icon'] ?>" plain="true"><?php echo $row['title'] ?></a>
			<?php } ?>
		</span>
		<?php } ?>
		<?php if($filterField2Options){ ?>
		<span class="bs-dtgrid-menu-span right-border">
			<?php foreach (array_keys($filterField2Options) as $field) { ?>
			<?php echo $field2name[$field]; ?>:
			<?php echo adm_tool_htmlMaker::select($filterField2Options[$field] , '','sel_'.$field , L('all',1) , 'field="'.$field.'"') ?>
			<?php } ?>
		</span>
		<?php } ?>
		<?php if($searchField2name){ ?>
		<span class="bs-dtgrid-menu-span right-border">
			<?php L('search');?>:<input id="iptSearch">
			<?php echo adm_tool_htmlMaker::select($searchField2name , 0, 'selSearchField','') ?>
		</span>
		<input id="btnSelect" type="button" value="<?php L('select'); ?>">
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
//filter
	$('#btnSelect').click(function(){
		var aCondition = Array();
		$(this).parent().parent().find('select').each(function(){
			if($(this).find('option:selected').attr('value') > 0){
				aCondition.push($(this).attr('field')+'='+$(this).val());
			}
		});

		if($('#iptSearch').val()){
			aCondition.push('search_field='+$('#selSearchField').val());	
			aCondition.push('search_key='+$('#iptSearch').val());	
		}

		var sCondition = aCondition.join('&');
		var url = $('body').attr('url_base')+'/listJson?'+sCondition+'&'+$('body').attr('linkParam');
		$('#tt').datagrid({  
    		url:url,  
		});
		
	})
//image preview
	$('#tt').datagrid({  
	    onLoadSuccess:function(data){  

	        $('.imgPreview').tooltip({
			    position: 'left',
			    content: function(){return '<img id="imgPreview" src="'+$(this).attr('src')+'" width="300"/>'}
		    });
		}  
	}) 
//
	function changeById(dom){
		var select = $(dom);
		$.get($('body').attr('url_base')+'/doChange?id='+select.attr('data_id')+'&field='+select.attr('field')+'&value='+select.val()+'&'+$('body').attr('linkParam'),'',function(data){
			var rs = bs_str2obj(data);
			if(rs.code != APICODE_SUCC){
				alert('error');
			}

		})
	}
</script>
</body>
</html>