<?php
	adm_tool_htmlMaker::html_header(); 

	$html = $oEditForm->getFormHtml();
	echo $oEditForm->getInclude(); 
?>

<body style="margin: 10px;" url_base="<?php echo $url_base; ?>" linkParam="<?php echo $linkParam; ?>">

<div class="easyui-panel" title="<?php L('create'); ?>">
    <?php echo $html; ?>
</div>

<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
</body>
<?php echo $oEditForm->getJs(); ?>
</html>