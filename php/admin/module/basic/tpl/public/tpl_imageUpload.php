<div id="divUpload" class="easyui-dialog" title="<?php L('upload_image'); ?>" data-options="iconCls:'icon-save'" closed="true" style="margin: 0px" modal="true">
	<div id="tt" class="easyui-tabs" style="width:500px;height:250px;">
	    <div title="<?php L('upload_image')?>" style="padding:20px;">
		    <form id="formUpload" enctype="multipart/form-data" method="post">
		    	<div><input type="file" name="image"/></div>
		    	<div><input type="checkbox" value="1" name="isWaterMark"/>是否加水印？</div>
		    	<div>
		    	<hr>
				<input type="submit" value="upload" /><span id="spResult"></span>
				</div>
			</form>
	    </div>
    </div>
</div>
<script type="text/javascript">
	$('#formUpload').form({
		url:'/basic/home/doUploadImage',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
            	var callback=$('#divUpload').attr('callback')+'("'+rs.data.img_id+'","'+rs.data.img_url+'")';
            	eval(callback);
            }else{
              $('#spResult').css('color','red').text(rs.msg);
            }
		}
	});
</script>