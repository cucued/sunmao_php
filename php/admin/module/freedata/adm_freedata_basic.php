<?php
class adm_freedata_basic extends admin_controller{
	const TYPE_GROUP = 0;
	public function run(){
		$this->output_html('home');
	}

	public function homeJson(){
		include($this->get_lang_path());
		$L = $LANG[$this->get_module_name()];

		$oFreedata = new adm_freedata_model_freedata();
		$oFreedata->getListByType(self::TYPE_GROUP);
		$aData = $oFreedata->get_data();

		$aDataJson = array();
		foreach ($aData as $row) {
			$sEditLink = '<a href="javascript:;" onclick="fd_edit('.$row['value'].')">'.$L['edit'].'</a>';
			// $sLastLink = '<a href="javascript:;" onclick="fd_last('.$row['value'].')">最后编辑</a>';
			$aDataJson[] = array(
				'text' => '&gt;'.$row['name'].'('.$row['value'].') '.$sEditLink.' '.$sLastLink,
				'iconCls' => 'icon-blank',
			);
		}

		echo json_encode($aDataJson);
	}
	public function doCreate(){
		$name = Tool_input::input('name','post');


		$oFreedata = new adm_freedata_model_freedata();
		$oFreedata->createGroup($name);

		$this->log_action(__CLASS__.':'.__FUNCTION__ , $name);
		$this->output_json(APICODE_SUCC);
	}
	public function editPage(){
		$type = Tool_input::input('type');

		$oFreedata = new adm_freedata_model_freedata();
		$oFreedata->getListByType($type);
		$aData = $oFreedata->get_data();

		$aOption = array();
		foreach ($aData as $row) {
			$aOption['opt_'.$row['id']] = array(
				'name' => $row['name'],
				'eng_name' => $row['eng_name'],
				'value' => (int)$row['value'],
			);
		}

		$aOutput = array(
			'type' => $type,
			'options' => $aOption,
		);
		$this->output_html('edit' , $aOutput);
	}
	public function editJson(){
		$type = Tool_input::input('type');

		include($this->get_lang_path());
		$L = $LANG[$this->get_module_name()];

		$oFreedata = new adm_freedata_model_freedata();
		$oFreedata->getByTypeValue(0,$type);
		$aGroupInfo = $oFreedata->get_data();


		$oFreedata->getListByType($type);
		$aData = $oFreedata->get_data();

		$aDataJson = array();
		
		foreach ($aData as $row) {
			$sEditLink = '<a href="javascript:;" onclick="openEditWindow('.$row['id'].')">'.$L['edit'].'</a>';
			$aDataJson[] = array(
				'text' => '<span id="sp_'.$row['id'].'">'.($this->get_lang() == 'cn' ? $row['name'] : $row['eng_name']).'</span> '.$sEditLink,
				'selected' => true,
				'iconCls' => 'icon-blank',
				'id' => 'opt_'.$i,
			);
			$i++;
		}
		$aRoot[] = array(
			'id' => 'treeroot',
			'text' => $this->get_lang() == 'cn' ? $aGroupInfo['name'] : $aGroupInfo['eng_name'],
			'iconCls' => 'icon-blank',
			'children' => $aDataJson
		);

		echo json_encode($aRoot);
	}

	public function doEdit(){
		$type = Tool_input::input('type','get');
		$aData = $_POST['data'];

		$oFreedata = new adm_freedata_model_freedata();

		$aValue = array();
		foreach ($aData as $key => $value) {
			$id = substr($key , 4);

			if(in_array($value['value'] , $aValue)){
				$this->output_json(APICODE_PARAM);
			}

			//老数据更新
			if(is_numeric($id)){
				$oFreedata->editById($value['name'] , $value['eng_name'] , $value['value'] , $id);
			}
			//新增加数据
			else if($id[0]=='t'){

				$oFreedata->insertByType($value['name'] , $value['eng_name'] , $value['value'] , $type);
			}
			$aValue[] = $value['value'];
		}

		$this->log_action(__CLASS__.':'.__FUNCTION__ , $type);
		$this->output_json(APICODE_SUCC);
	}

}