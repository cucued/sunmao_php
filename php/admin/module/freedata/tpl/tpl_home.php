<?php adm_tool_htmlMaker::html_header(); ?>
<body>
<table width="100%" border="0">
	<tr>
		<td width="200px" valign="top">
			<div id="divEditPanel" class="easyui-panel" title="选项" style="padding: 10px;">
				<ul id="ulPower" class="easyui-tree" data-options="url:'/freedata/index/homeJson',method:'get',animate:true"></ul>
			</div>
			<div class="easyui-panel" title="创建" style="padding: 10px;">
				名称:<input type="text" id="iptName"/>
				<input id="btnCreate" type="button" value="创建"/><span id="spRs"></span>

			</div>
		</td>
		<td>	
			<iframe id="ifrEdit" src="" width="100%;" height="100%" style="border: 0px"></iframe>
		</td>
	</tr>
</table>
</body>
<script type="text/javascript">
	function fd_edit(fd_type){
		$('#ifrEdit').attr('src','/freedata/index/editPage?type='+fd_type);
	}

	$('#btnCreate').click(function(){
		$.post('/freedata/index/doCreate', {'name':$('#iptName').val()}, function(data){
			var rs = bs_str2obj(data);
	            if(rs.code == APICODE_SUCC){
	              $('#spRs').css('color','green').text('修改成功！').show().fadeOut(2000);
	            }else{
	              $('#spRs').css('color','red').text('修改失败，请与管理员联系！');
	            }
		});
	});
</script>
</html>