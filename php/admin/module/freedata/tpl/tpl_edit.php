<?php adm_tool_htmlMaker::html_header(); ?>
<body style="margin:0px;">
<div id="divEditPanel" class="easyui-panel" title="<?php L('edit'); ?>" style="padding: 10px;">
<ul id="ulPower" fd_type="<?php echo $type; ?>" class="easyui-tree" data-options="url:'/freedata/index/editJson?type=<?php echo $type; ?>'
													,method:'get'
													,animate:true"></ul>
</div>

<div id="divEditWin" class="easyui-window" title="<?php L('edit'); ?>" closed="true" minimizable:"false" maximizable:"false" style="width:300px;padding:5px;" edit_id="">
	<table>
		<tr>
			<td><?php L('name'); ?></td>
			<td><input id="iptName" type="text"/></td>
		</tr>
		<tr>
			<td><?php L('eng_name'); ?></td>
			<td><input id="iptEngName" type="text"/></td>
		</tr>
		<tr>
			<td><?php L('value'); ?></td>
			<td><input id="iptValue" type="text"/></td>
		</tr>
		<tr>
			<td></td>
			<td><input id="btnSave" value="<?php L('submit'); ?>" type="button"/></td>
		</tr>
	</table>
</div>
<input type="button" id='btnAddOption' value="<?php L('add'); ?>">
<input type="button" id='btnSaveData' value="<?php L('submit'); ?>"><span id="spRs"></span>


</body>
<script type="text/javascript">
	var options = <?php echo empty($options) ? '{}' : json_encode($options); ?>;

	$win = $('#divEditWin').window(
	{ 
	    minimizable: false,
	    maximizable: false, 
	   collapsible: false });


	function openEditWindow(i){
		
		$('#divEditWin').attr('edit_id',i);
		$('#iptName').val(options['opt_'+i]['name']);
		$('#iptEngName').val(options['opt_'+i]['eng_name']);
		$('#iptValue').val(options['opt_'+i]['value']);
		
		$('#divEditWin').window('open');
	}

	$('#btnSave').click(function(event) {

		var key = $('#divEditWin').attr('edit_id');

		options['opt_'+key]['name'] = $('#iptName').val();
		options['opt_'+key]['eng_name'] = $('#iptEngName').val();
		options['opt_'+key]['value'] = $('#iptValue').val();

		$('#sp_'+key).text($('#iptName').val());

		$('#divEditWin').window('close');
	});

	$('#btnAddOption').click(function(){

		var node = $('#ulPower').tree('find', 'treeroot');
		// if (node){
			var n=0;
			var max_value = 0;
			for(var i in options){
				max_value = max_value > options[i]['value'] ? max_value : options[i]['value'];
				n++;
			}
			//临时下标
			n='t'+n;
			max_value = parseInt(max_value);

			options['opt_'+n] = {
				'name':'新增',
				'eng_name':'tmp',
				'value':max_value+1,
			};

			var nodes = [{
				"id":13,
				"text":"<span id=\"sp_"+n+"\">new</span> <a href=\"javascript:;\" onclick=\"openEditWindow('"+n+"')\"><?php L('edit'); ?></a>",
				"iconCls":"icon-blank"
			}];
			

			
			$('#ulPower').tree('append', {
				parent:node.target,
				data:nodes
			});
		// }
		
	});

	$('#btnSaveData').click(function(event){
		var valueList = [];
		
		for(var key in options){
			if(valueList[options[key]['value']] == 1){
				$('#spRs').text('数据值 '+options[key]['value']+' 在 "'+options[key]['name']+'" 项中重复使用了').css('color','red').show();
				return;
			}
			valueList[options[key]['value']] = 1;	
		}
		
		$.post('/freedata/index/doEdit?type='+$('#ulPower').attr('fd_type'), {'data':options}, function(data){
			var rs = bs_str2obj(data);
	            if(rs.code == APICODE_SUCC){
	              $('#spRs').css('color','green').text(rs.msg).show().fadeOut(2000);
	            }else{
	              $('#spRs').css('color','red').text(rs.msg);
	            }
		});
	});
</script>
</html>