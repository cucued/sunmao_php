<?php
class adm_freedata_model_freedata extends ml_model_freedata{
	
	/**
 * 创建构造函数
 *
 */
    function __construct()
    {
        /**
         * 加载数据库配置
         */
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        /**
         * 构造函数
         * 参数：
         * 1，当前模型名称
         * 2，相关数据库配置
         */
        parent::__construct('admin' , $db_config);
    }

    
    public function editById($name , $eng_name , $value , $id){
        if(!$this->init_db('',self::DB_MASTER))
            return false;

        $aData = array(
            'name' => $name,
            'eng_name' => $eng_name,
            'value' => $value,
        );
        $this->table = self::TABLE;

        $where = 'id='.$id;
        return $this->update($aData , $where);
    }
    public function insertByType($name , $eng_name , $value , $type){
        if(!$this->init_db('',self::DB_MASTER))
            return false;

        $aData = array(
            'type' => $type,
            'name' => $name,
            'eng_name' => $eng_name,
            'value' => $value,
        );
        $this->table = self::TABLE;
        return $this->insert($aData);
    }
    public function createGroup($name){
        if(!$this->init_db('',self::DB_MASTER))
            return false;

        $sql = 'select max(value) n from '.self::TABLE.' where type = 0';
        $this->fetch_row($sql);
        $aData = $this->get_data();
        $max_type = $aData['n'];
        $value = $max_type+1;

        $aData = array(
            'type' => 0,
            'name' => $name,
            'value' => $value
        );
        $this->table = self::TABLE;
        return $this->insert($aData);
    }

}