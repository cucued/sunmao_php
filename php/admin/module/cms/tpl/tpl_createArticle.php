<?php adm_tool_htmlMaker::html_header(); ?>
<?php
	$html = $oEditForm->getFormHtml($aEditField , '' , file_get_contents(dirname(__file__).'/tpl_articleForm.html'));
	
	echo $oEditForm->getInclude(); 
?>

<body url_base="<?php echo $url_base; ?>" linkParam="<?php echo $linkParam; ?>">
<div class="easyui-panel" title="<?php L('publish_article'); ?>">

<?php
	echo $html;
?>
</div>




</body>
<script type="text/javascript">
	//封面图预览
	$('#imgCover').tooltip({
	    position: 'right',
	    content: function(){return '<img src="'+$(this).attr('src')+'" width="300"/>'},
	    onShow: function(){
	    $(this).tooltip('tip').css({
	    borderColor: '#666'
	    });
	    }
    });

	function setCover(img_id,img_url){
		$('#iptCoverImgid').val(img_id);
		$('#imgCover').attr('src',img_url);
		$('#divUpload').dialog('close');
	}
	$('#aSetCover').click(function(){
		$('#divUpload').attr('callback','setCover');
		$('#divUpload').dialog('open');
	})

</script>


<?php echo $oEditForm->getJs(); ?>
<?php include(ADM_PUBLIC_TPL.'tpl_imageUpload.php'); ?>
</html>