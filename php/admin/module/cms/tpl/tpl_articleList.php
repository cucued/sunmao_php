<?php
adm_tool_htmlMaker::html_header(); ?>

<body style="margin: 10px;">
	<table class="easyui-datagrid" id="tt" title="<?php L('articles'); ?>" data-options="singleSelect:false,selectOnCheck:true,collapsible:true,url:'/cms/basic/listJson?<?php echo http_build_query($condition); ?>',method:'get',pagination:true,pageSize:10,toolbar:'#dgMenu'">
		<thead>
			<tr>
				<th field="ck" checkbox="true"></th>
			<?php foreach ($field2cn as $key => $value) {
					$sortSign = in_array($key , $aSortable) ? ' sortable="true"' : '';
				?>
				<th data-options="field:'<?php echo $key; ?>'"<?php echo $sortSign; ?>><?php echo $value; ?></th>
			<?php } ?>
				<th data-options="field:'op'"><?php L('edit'); ?></th>
			</tr>
		</thead>
	</table>
	<div id="dgMenu" style="padding:5px;height:auto">
		<!--
		<div>
			<span class="bs-dtgrid-menu-span right-border">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>
			</span>
		</div>
		-->
		<div class="bs-dtgrid-menu-span icon-filter bs-icon-left">
				<span class="bs-dtgrid-menu-span right-border">
					<?php foreach (array_keys($condition) as $field) { ?>
					<?php echo $field2cn[$field]; ?>:
					<?php echo adm_tool_htmlMaker::select($conditionOptions[$field] , $condition[$field],'sel_'.$field , L('all',1) , 'field="'.$field.'"') ?>
					<?php } ?>
				</span>
				<span class="bs-dtgrid-menu-span right-border">
					<?php L('search');?>:<input id="iptSearch">
					<?php echo adm_tool_htmlMaker::select($searchField2cn , 0, 'selSearchField','') ?>
				</span>
				<input id="btnSelect" type="button" value="<?php L('select'); ?>">
		</div>
	</div>

<script type="text/javascript">
//filter
	$('#btnSelect').click(function(){
		var aCondition = Array();
		$(this).parent().parent().find('select').each(function(){
			if($(this).find('option:selected').attr('value') > 0){
				aCondition.push($(this).attr('field')+'='+$(this).val());
			}
		});

		if($('#iptSearch').val()){
			aCondition.push('search_field='+$('#selSearchField').val());	
			aCondition.push('search_key='+$('#iptSearch').val());	
		}

		var sCondition = aCondition.join('&');
		
		$('#tt').datagrid({  
    		url:'/cms/basic/listJson?'+sCondition,  
		});
		
	})
//image preview
	$('#tt').datagrid({  
	    onLoadSuccess:function(data){  

	        $('.imgPreview').tooltip({
			    position: 'left',
			    content: function(){return '<img id="imgPreview" src="'+$(this).attr('src')+'" width="300"/>'}
		    });
		}  
	}) 
//
	function changeStatusById(dom){
		var select = $(dom);
		$.get('/cms/basic/doChangeStatus?id='+select.attr('article_id')+'&status='+select.val(),'',function(data){
			var rs = bs_str2obj(data);
			if(rs.code != APICODE_SUCC){
				alert('error');
			}

		})
	}
</script>


</body>
</html>