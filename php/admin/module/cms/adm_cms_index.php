<?php
include(SYS_INC_PATH.'3rd/phpQuery.php');
class adm_cms_index extends datafactory_controller{

	function _construct(){
		$this->setDataDefine('content');
		$this->listField = array('id','status','title','pub_time','cover_img_id','category','tag');
		$this->filterField = array('category','status');
		$this->searchField = array('title','eng_alias','tag');
		$this->sortableField = array('pub_time');
		$this->modifyInListField = array('status');

		$this->tpl_name = array(
			'create' => 'createArticle',
			'modify' => 'modifyArticle',
		);
		$this->field2formaterInList = array(
			'cover_img_id' => '_listPreviewImg',
			'pub_time' => '_listPubtime',
		);
	}

	protected function _fetchForModify($id){
		$oArticle = new adm_cms_model_article();
		$oArticle->getById($id);
		return $oArticle->get_data();
	}
	protected function _doCreate($aData){
		$oArticle = new adm_cms_model_article();
		$rs = $oArticle->create($aData,$this->adm_id) or $this->redirect('busy');
		$id = $oArticle->insert_id();

		$oTag = new adm_cms_model_tag2article();
		$tags = explode(' ',$aData['tag']);

	 	$oTag->replaceTags2article($id , $tags);
	 	return $id;
	}
	protected function _doModify($id , $aData){
		$oArticle = new adm_cms_model_article();
		$rs = $oArticle->modifyArticle($id , $aData,$this->adm_id) or $this->redirect('busy');

		$oTag = new adm_cms_model_tag2article();
		$tags = explode(' ',$aData['tag']);
		return $oTag->replaceTags2article($id , $tags);
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$oArticle = new adm_cms_model_article();
		$oArticle->listByCondition($this->listField,$aConditionValue,$page , $pagesize,$sort,$order);
		return $oArticle->get_data();

	}
	protected function _fetchListCount($aConditionValue){
		$oArticle = new adm_cms_model_article();
		$oArticle->countByCondition($aConditionValue);
		return $oArticle->get_data();
	}
	protected function _opHtml($row){
		$html = '<a href="/datafactory/data/run?type='.$row['id'].'">'.L('set_data',true).'</a>';

		return $html;
	}
	protected function _listPreviewImg($img_id){

		return '<img width="60" class="imgPreview" src="'.imgsrv_model_upload::get_display_url($img_id).'"/>';
	}
	protected function _listPubtime($time){
		return str_replace(' ','<br/>',$time);
	}
	protected function _doChange($id , $field , $value){

		$oArticle = new adm_cms_model_article();
		$oArticle->getById($id);
		$aArticle = $oArticle->get_data();
		if(empty($aArticle)){
			$this->output_json(APICODE_HACK , 'no article');
		}

		$aData = array(
			$field => $value
		);
		$oArticle->modifyArticle($id , $aData , $this->adm_id) or $this->output_json(APICODE_BUSY);

		if($field == 'status'){
			$oTag = new adm_cms_model_tag2article();

			if($value != adm_cms_model_article::STATUS_NORMAL){
				$oTag->delTags2article($id);
			}else{
				$tags = explode(' ',$aArticle['tag']);
				$oTag->replaceTags2article($id , $tags);
			}
		}

		return;
	}
	protected function _formatInputInForm($aInput){
		$aInput['pub_time'] = date('Y-m-d H:i:s' , strtotime($aInput['pub_time']));

		return $aInput;
	}

	public function fetchWXContent(){
		$url = Tool_input::input('url');
		
		$content = file_get_contents($url);
		phpQuery::newDocumentHTML($content);
		

		$title = strip_tags(pq('title')->htmlOuter());

		$content = pq('#js_content')->htmlOuter();
		$content = preg_replace('/<img .*data-src="(.*)".*>/U','<img src="\\1">',$content);

		$aData = array(
			'content' => $content,
			'title' => $title,
		);
		$this->output_json(APICODE_SUCC,'',$aData);

	}
}