<?php
class adm_cms_model_article extends ml_lib_datamodel_common{
    
    var $table = 'bs_cms_article';

    public function createArticle($aData , $staff_id){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData['create_user'] = $staff_id;

        
        return $this->insert($aData);
    }
    public function modifyArticle($id , $aData , $staff_id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        $aData['update_user'] = $staff_id;

        
        $where = 'id='.(int)$id;
        return $this->update($aData,$where,1);
    }
    
}