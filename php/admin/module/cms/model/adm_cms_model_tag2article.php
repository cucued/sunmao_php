<?php
class adm_cms_model_tag2article extends Lib_datamodel_db{
	const TABLE = 'bs_cms_tag2article';
	function __construct()
    {
        $this->_is_ctime = false;
        $this->_is_utime = false;
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }
    function replaceTags2article($id,$tags){
    	$rs = $this->delTags2article($id);
        if(!$rs){
            return $rs;
        }

    	foreach ($tags as $tag) {

    		$aData[] = array(
    			'article_id' => $id,
    			'tag' => $tag,
    		);
    	}
    	return $this->insert_multi($aData);
    }
    function delTags2article($id){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;

        $this->table=self::TABLE;

        $where = 'article_id = '.$id;
        return $this->delete($where);
    }
}