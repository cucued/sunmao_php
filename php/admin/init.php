<?php
/************************************
*   熊店
*   @file:/server/bearshop/__admin/__global.php
*   @intro:
*   @author:new_shop
*   @email:new_shop@163.com    
*   @date:Tue Feb 09 22:47:22 CST 2010
************************************/

require('../__sys_inc/init.php');
require('../__biz_inc/init.php');

define('ADM_ROOT_PATH' , dirname(__file__).'/');
define('ADM_PUBLIC_TPL' , ADM_ROOT_PATH.'module/basic/tpl/public/');



$dp = opendir(ADM_ROOT_PATH.'/module');
while ($name = readdir($dp)) {
    $module_dir = ADM_ROOT_PATH.'/module/'.$name;
    if($name != '' && $name != '' & is_dir($module_name)){
        echo $module_dir.'/config/config.php';die;
        include $module_dir.'/config/config.php';
    }
}


function adm_autoload($class){
    $aClass = explode('_', $class);
    $sign = array_shift($aClass);

    // echo $class;
    if($sign == 'adm'){
        
        if(in_array($aClass[0] , array('tool',))){

            $s = ADM_ROOT_PATH.Autoload::class2path(implode('_' , $aClass),'adm_');    
        }else{
            $s = ADM_ROOT_PATH.'/module'.Autoload::class2path(implode('_' , $aClass),'adm_');    
            if(!is_file($s)){
                $s = ADM_ROOT_PATH.'/module/'.Autoload::class2path(implode('_' , $aClass),'adm_');    
            }
        }
        return $s;
    }else{
        return false;
    }
}
Autoload::register_autoload_function('admin' , 'adm_autoload');

function L($key,$return = 0){
    global $L;
    if($return){
        return $L[$key];
    }else{
        echo $L[$key];
    }
    
}


include('adm_controller.php');
include('datafactory_controller.php');
?>