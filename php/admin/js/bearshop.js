//常量
var APICODE_SUCC = 'A00006';

/**
 * 把JSON字符串转成对象
 * 用于服务器接口返回数据处理
 * @param  string str [description]
 * @return object    [description]
 */
function bs_str2obj(str){
	return eval('('+str+')');
}
function bs_adm_callNewWorkTab(title , url){
    window.parent.openWorkTab(title,url);
}

//表单检查器扩展～～～～～～～～～～～～～～～～～～～～～～～～～～～～～
/* 判断再次密码输入是否一样 */
$.extend($.fn.validatebox.defaults.rules, {  
    /*必须和某个字段相等*/
    equalTo: {
    	validator:function(value,param){
    		return $(param[0]).val() == value;
    	},
    	message:'两次密码不相同'
    }
          
});
//手机号验证
$.extend($.fn.validatebox.defaults.rules, {  
    mobile: {
    	validator:function(value,param){
    		return value.length==11;
    	},
    	message:'手机号格式不正确'
    }
          
});
//手机号验证
$.extend($.fn.validatebox.defaults.rules, {  
    preg: {
        validator:function(value,param){
            var re=new RegExp(String(param[0]));
            return re.test(value);
            
        },
        message:'输入格式不正确'
    }
          
});

function countByteLength(str,cnCharByteLen)
{
    if(!(cnCharByteLen>0)){
        cnCharByteLen = 1;
    }
    var byteLen = 0;
    for(var i=0; i<str.length; i++)
    {
    //alert（str.charAt（i））;
    if ((/[\x00-\xff]/g).test(str.charAt(i)))
    byteLen += 1;
    else
    byteLen += cnCharByteLen;
    //alert（byteLen）;
    }
    return byteLen;
} 