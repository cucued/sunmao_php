
    // Define the hello button
    $.cleditor.buttons.hello = {
        name: "hello",
        image: "image.gif",
        title: "Hello World",
        command: "inserthtml",
        popupName: "hello",
        popupClass: "cleditorPrompt",
        popupContent: "<form enctype=multipart/form-data method=post><input type=file name=image /><input type=checkbox name=isWaterMark value=1/>使用水印?<input type=submit value=upload /></form>",
        buttonClick: helloClick
    };
 
    // Add the button to the default controls before the bold button
    $.cleditor.defaultOptions.controls = $.cleditor.defaultOptions.controls
    .replace("bold", "hello bold");
 
    // Handle the hello button click event
    function helloClick(e, data) {
        // Get the editor
        var editor = data.editor;
        // var clData = data;
        $(data.popup).children("form").form({
            url:'/basic/home/doUploadImage',
            
            success:function(rs){
                var rs = bs_str2obj(rs);
                if(rs.code == APICODE_SUCC){

                        // Insert some html into the document
                var html = "<img src='"+rs.data.img_url+"'/>";
                editor.execCommand(data.command, html, null, data.button);
     
                // Hide the popup and set focus back to the editor
                editor.hidePopups();
                editor.focus();
     
                }else{
                  alert(rs.msg);
                }
            }
        });
    }
 
