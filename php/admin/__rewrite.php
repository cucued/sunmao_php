<IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^/(css|js|font)/(.*) /admin/$1/$2 [L]
        RewriteRule ^(.*) /admin/index.php

</IfModule>   