<?php
class admin_controller
{
    protected $adm_id;
    private $session_data;
    protected $is_need_login = true;
    private $lang = 'cn';
    static private $self;
    public $theme = 'gray';

    public function __construct(){

        self::$self = $this;
        $this->session_data = ml_factory::getSession()->getVal('__ADMIN');
        if($this->is_need_login && !$this->check_login()){

            $this->no_login();
        }

        //语言=语言通用包+本模块语言包
        global $L;
        include(ADM_ROOT_PATH.'module/basic/lang/lang_common_'.$this->lang.'.php');
        $L = $LANG['common'];
        
        $lang_file = $this->get_lang_path();
        if(is_file($lang_file)){
            include($lang_file);
            $L += (array)$LANG[$this->get_module_name()];
        }

        $theme = $this->get_session('theme');

        if($theme){
            $this->theme = $theme;
        }

        if(is_file($this->get_module_path().'/config/power_define.php')){

            include($this->get_module_path().'/config/power_define.php');
        }

        $this->_construct();
    }
    public function get_instance(){
        return self::$self;
    }
    public function get_lang(){
        return $this->lang;
    }
    protected function _construct(){}

    protected function check_login(){
        
        $adm_id = $this->get_session('adm_id');
        if($adm_id < 1)
        {
            return false;
        }else{
            $this->adm_id = $adm_id;
            return true;
        }
    }
    protected function check_power($power_id){
        
        $oPower = new adm_basic_model_power();
        
        if($oPower->isStaffHavePower($this->adm_id , $power_id) && $oPower->get_data() > 0){
            return true;
        }else{
            die('你没有权限');
        }

    }
    private function over(){
        if(SYSDEF_DEBUG && extension_loaded('xhprof'))
        {
            $arr = xhprof_disable();
            $filename = get_class($this).'.xhprof';
            Tool_logger::oneLog('xhprof' , $filename , serialize($arr) );
        }
    }
    protected function no_login(){
        echo '<script>window.top.location="/login/";</script>';
        die;
    }


    private function run(){
        

    }
    
    protected function get_module_name(){
        $aClass = explode('_' , get_class($this));
        return $aClass[1];
    }
    protected function get_module_path(){
        
        return ADM_ROOT_PATH.'module/'.$this->get_module_name();
    }
    protected function get_lang_path(){
        return $this->get_module_path().'/lang/lang_'.$this->lang.'.php';
    }
    
    /**//**
     * Description
     * @param type $tpl 
     * @param type $data 
     * @return type
     */
    protected function output_html($tpl , $data = array() , $module = ''){
        
        $module_dir = ADM_ROOT_PATH.'/module/'.($module ? $module : $this->get_module_name());
        $tpl_path = $module_dir.'/tpl/'.Autoload::class2path($tpl,'tpl_');

        Tool_logger::debugLog(get_class($this),'load_tpl:'.$tpl_path);
        if(is_file($tpl_path)){

            $module_path = $this->get_module_path();
            $theme = $this->theme;
            extract($data);
            include($tpl_path);
            $this->over();
            return;
        }else{
            die('tpl not found:'.$tpl_path);
        }
    }
    protected function output_json($api_code , $msg = '',$data = ''){
        if(empty($msg)){
            $aCode2msg_cn = array(
                APICODE_SUCC => '操作成功',
                APICODE_FAIL => '操作失败，请联系管理员',
            );
            $aCode2msg_eng = array(
                APICODE_SUCC => 'Success!',
                APICODE_FAIL => 'Error!',
            );

            $aMsg = $this->lang == 'cn' ? $aCode2msg_cn : $aCode2msg_eng;
            $msg = $aMsg[$api_code];
        }

        echo json_encode(array(
                'code' => $api_code,
                'data' => $data,
                'msg' => $msg,
            ));
        $this->over();
        die;
    }
    protected function output_easyuiJson($total,$rows){
        $aOutput = array(
            'total' => $total,
            'rows' => $rows
        );

        
        echo json_encode($aOutput);die;
    }

    protected function redirect($url , $lazy_msg = '' , $sec = 1)
    {
        if(in_array($url , array(404 , 'busy'))){
            die($url);
        }
        if(headers_sent() || $lazy_msg)
        {
            echo '<meta http-equiv="refresh" content="'.$sec.'; url='.$url.'" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            echo $lazy_msg.'<br/>'.$sec.'秒后跳转...';
        }
        else 
        {
            header('Location:'.$url);
        }
        $this->_over();
        die;
    }
    protected function _combine_menu_data(){
        $module_path = ADM_ROOT_PATH.'/module/';
        $dp = opendir($module_path);

        $aGroup = array();
        while ($dir = readdir($dp)) {
            if(is_dir($module_path.$dir) && is_dir($module_path.$dir.'/config/')){

                if(is_file($module_path.$dir.'/config/menu.xml')){

                    $aMenuData = Tool_xmlReader::xml2array(file_get_contents($module_path.$dir.'/config/menu.xml'));

                    foreach ($aMenuData['element_group'] as $group) {

                        if(!isset($aGroup[$group['title']])){
                            $aGroup[$group['title']] = array(
                                'title' => $group['title'],
                                'title_eng' => $group['title_eng'],
                                'icon' => $group['icon'],
                                'power_id' => $group['power_id'],
                                'element' => $group['element'],
                            );
                        }else{
                            foreach ($group['element'] as $element) {
                                $aGroup[$group['title']]['element'][] = $element;
                            }
                        }
                    }
                    
                }
            }

            
        }
        return $aGroup;
    }
    //session
    protected function set_session($key , $value)
    {
     
        $this->session_data[$key] = $value;
        $oSess = ml_factory::getSession();
        $oSess->setVal('__ADMIN' , $this->session_data);
    }
    public  function get_session($key)
    {
        return $this->session_data[$key];
    }
    protected function del_session($key)
    {
        unset($this->session_data[$key]);
        ml_factory::getSession()->setVal('__ADMIN' , $this->session_data);
    }


    protected function log_action($function , $dest = 0){
        Tool_logger::baseActionLog($this->adm_id , $function , array($dest));
    }
}
