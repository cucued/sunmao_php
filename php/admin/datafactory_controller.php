<?php

class datafactory_controller extends admin_controller{
	private $model;
	private $list_type = 'datagrid';			//数据表格类型 默认 datagrid 可换成treegrid
	protected $treefield;						//treegrid表格需要的

	private $dataDefineXml = '';				//数据定义文件的关键词 见getDefineXml 方法
	protected $listField = array();				//数据列表中显示的字段
	protected $listTitle = '';					//数据列表的名称
	protected $filterField = array();			//可以用来进行数据过滤的字段 需要是enum类型 比如status gender等
	protected $searchField = array();			//可以用来进行搜索的字段 比如 name description等
	protected $sortableField = array();			//可以用来排序的字段 
	protected $modifyInListField = array();		//可以在列表中直接修改的字段 ，目前主要为status

	protected $list_url = '';					//列表取数据的接口地址，默认为当前类的listJson方法
	protected $linkParam = '';					//如果在当前模块各页面间跳转需要带上固定的参数，可以定义在此，比如 始终要带上type=1参数
	protected $tpl_name = array();				//指定模板文件名，默认会使用basic模块中的公用模板
	protected $field2formaterInList = array();	//定义在列表中需要特殊显示的字段及其处理方法
	private $_buttons = array();				//列表上方的按钮，比如 新增，见addButton方法 
	protected $tpl_data = array();				//需要额外增加到模板中的数据

	protected function setModel($model){
		$this->model = $model;
	}
	/**
	 * 设置成treegrid
	 */
	public function setListTypeTreeGrid(){
		$this->list_type = 'treegrid';
	}
	/**
	 * 设置数据定义文件名称
	 * @param string $dataDefineXml [description]
	 */
	public function setDataDefine($dataDefineXml){
		$this->dataDefineXml = $dataDefineXml;
	}
	/**
	 * 获取数据定义
	 * @return array array('field'=>'字段定义',‘option’=>‘其他配置’)
	 */
	protected function getDefineXml(){
		return Tool_xmlReader::xml2array(file_get_contents($this->get_module_path().'/config/adm_dataConfig_'.$this->dataDefineXml.'.xml'));
	}
	/**
	 * 取当前类名 比如 /cms/index/run cms为模块名 index为类名 run为方法名
	 * @return [type] [description]
	 */
	private function getClassName(){
		$class = get_class($this);
		$prefix_length = strpos($class , $this->get_module_name())+strlen($this->get_module_name())+1;
		return substr(get_class($this),$prefix_length);
	}
	/**
	 * 在列表顶增加按钮
	 * @param [type]  $title   [description]
	 * @param [type]  $icon    [description]
	 * @param [type]  $href    [description]
	 * @param boolean $new_tab [description]  //新页签打开
	 */
	protected function addButton($title , $icon , $href , $new_tab = false){

		if($new_tab){
			$href = "javascript:bs_adm_callNewWorkTab('".$title."','".$href."');";
		}
		$this->_buttons[] = array(
			'title' => $title,
			'icon' => $icon,
			'href' => $href,
		);
	}
	//类型列表 创建
	public function run(){
		$dataConfig = $this->getDefineXml();

		$oDfTool = new ml_lib_dataFactory_tool();
		$oDfTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oDfTool->setDataConfig($dataConfig['field']);

		//field name in list
		$aName2Cn = $oDfTool->fields2name($this->listField);
		//search field name
		$aSearchName2Cn = $oDfTool->fields2name($this->searchField);
		

		//filter select options
		foreach ($this->filterField as $field) {
			$aConditionOption[$field] = $oDfTool->getOptions($field);
		}
		
		$url_base = '/'.$this->get_module_name().'/'.$this->getClassName();
		if(empty($this->list_url)){
			$this->list_url = $url_base.'/listJson'.($this->linkParam ? '?'.$this->linkParam:'');
		}
		
		
		$aData = array(
			'field2name' => $aName2Cn,
			'searchField2name' => $aSearchName2Cn,
			'filterField2Options' => $aConditionOption,
			'aSortable' => $this->sortableField,
			'page_id' => 'run',
			'url_base' => $url_base,
			'list_url' => $this->list_url,
			'linkParam' => $this->linkParam,
			'listTitle' => $this->listTitle,
			'buttons' => $this->_buttons,
			'list_type' => $this->list_type,
			'treefield' => $this->treefield,
		);
		

		if($this->tpl_name['list']){
			$this->output_html($this->tpl_name['list'],$aData);
		}else{
			$this->output_html('public_listPage',$aData,'basic');
		}
	}
	public function listJson(){
		$dataConfig = $this->getDefineXml();

		$page = Tool_input::input('page');
		$pagesize = Tool_input::input('rows');

		//排序 公用
		$sort = Tool_input::input('sort','get');
		if(!in_array($sort , $this->sortableField)){ $sort = 'id'; }
		$order = Tool_input::input('order','get');
		if(!in_array($order , array('asc' , 'desc'))){$order = 'desc';}

		//search
		$search_field = Tool_input::input('search_field','get');
		if($search_field && in_array($search_field , $this->searchField)){
			$aConditionValue['search'] = array(
				'field' => $search_field,
				'key' => Tool_input::input('search_key','get'),
			);
		}

		//filter
		foreach ($this->filterField as $field) {
			if(Tool_input::issetInput($field)){
				$aConditionValue[$field] = Tool_input::input($field,'get');
			}
		}

		$aData = $this->_fetchList($aConditionValue , $page , $pagesize , $sort , $order);

		

		if(count($aData) < 1){
			$this->output_easyuiJson(0,array());
		}
		$total = $this->_fetchListCount($aConditionValue);


		$oTool = new ml_lib_dataFactory_tool();
		$dataConfig = $this->getDefineXml();
		$oTool->setDataConfig($dataConfig['field']);
		$oTool->setIsEng($this->get_lang() == 'cn' ? false : true);
		$aData = $oTool->dataOutput($this->listField , $aData , $this->modifyInListField);

		

		foreach ($aData as &$row) {
			$row['parent_id'] = $row['parent_id_ori'];
			//格式化某一字段
			if(count($this->field2formaterInList) > 0){
				foreach ($this->field2formaterInList as $field => $formater) {
					$row[$field] = $this->$formater($row[$field]);
				}
			}
			$editLink = '/'.$this->get_module_name().'/'.$this->getClassName().'/modify?id='.$row['id'].($this->linkParam ? '&'.$this->linkParam : '');
			$row['op'] .= '<a href="javascript:;" onclick="bs_adm_callNewWorkTab(\''.L('modify',true).'\',\''.$editLink.'\')">'.L('edit',true).'</a> '
						.$this->_opHtml($row);

		}

		if($this->list_type == 'treegrid'){
			
			$aData = formatTree($aData,0);
			
		}
		
		$this->output_easyuiJson($total , $aData);
	}
	protected function _fetchList($aConditionValue , $page , $pagesize ,  $sort , $order){
		$this->model->listByCondition($this->listField,$aConditionValue,$page , $pagesize,$sort,$order);
		return $this->model->get_data();
	}
	protected function _fetchListCount($aConditionValue){
		$this->model->countByCondition($aConditionValue);
		return $this->model->get_data();
	}
	protected function _opHtml($row){}

	private function _makeForm($action , $data = array() , $tpl_module = ''){

		$dataConfig = $this->getDefineXml();
		$oEditor = new ml_lib_dataFactory_editForm();
		$oEditor->setIsEng($this->get_lang() == 'cn' ? false : true);
		$oEditor->setDataConfig($dataConfig['field']);
		$oEditor->setSaveUrl('/'.$this->get_module_name().'/'.$this->getClassName().'/do'.ucfirst($action) . ($this->linkParam ? '?'.$this->linkParam : ''));

		if($action == 'modify' && $data['data']){
			$oEditor->setSaveUrl('/'.$this->get_module_name().'/'.$this->getClassName().'/do'.ucfirst($action).'?id='.$data['id'].($this->linkParam ? '&'.$this->linkParam : ''));
			$oEditor->setData($data['data']);
		}

		$url_base = '/'.$this->get_module_name().'/'.$this->getClassName();
		$aData = array(
			'oEditForm' => $oEditor,
			'action' => $action,
			'url_base' => $url_base,
			'linkParam' => $this->linkParam,
		);
		$aData += $this->tpl_data+$data;

		if($this->tpl_name[$action]){
			$this->output_html($this->tpl_name[$action],$aData);
		}else{
			$this->output_html('public_'.$action,$aData,'basic');
		}
	}
	public function create(){
		$this->_makeForm('create');
	}
	protected function _fetchForModify($id){
		$this->model->getById($id);
		return $this->model->get_data();
	}
	public function modify(){
		$id = Tool_input::input('id');
		$data = $this->_fetchForModify($id);
		if(empty($data)){
			$this->redirect(404);
		}
		$aData = array(
			'data' => $data,
			'id' => $id
		);

		$this->_makeForm('modify' , $aData);
	}
	public function doCreate(){
		$dataConfig = $this->getDefineXml();

		//param
		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();

		foreach ($aFields as $field) {
			$aInput[$field] = Tool_input::input($field , 'post');
		}
		$aInput = $this->_formatInputInForm($aInput);
		
		//check
		$oCheck = new ml_lib_dataFactory_checker();
		$oCheck->setDataConfig($dataConfig['field']);
		$rs = $oCheck->check_multi($aFields , $aInput);
		if(!$rs){
			$this->output_json(APICODE_PARAM,$oCheck->error());
		}

		$id = $this->_doCreate($aInput) or $this->output_json(APICODE_BUSY);



		$this->output_json(APICODE_SUCC,'',array('id'=>$id));
	}
	protected function _doCreate($aData){
		$rs = $this->model->create($aData,$this->adm_id) or $this->redirect('busy');
		return $this->model->insert_id();
	}
	public function doModify(){
		$id = Tool_input::input('id');
		$dataConfig = $this->getDefineXml();

		//param
		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$aFields = $oTool->getFieldNames();
		foreach ($aFields as $field) {
			if(Tool_input::issetInput($field)){
				$aInput[$field] = Tool_input::input($field , 'post');
			}
		}
		
		$aInput = $this->_formatInputInForm($aInput);
		
		$aFields = array_keys($aInput);

		//check
		$oCheck = new ml_lib_dataFactory_checker();
		$oCheck->setDataConfig($dataConfig['field']);
		$rs = $oCheck->check_multi($aFields , $aInput);
		if(!$rs){
			
			$this->output_json(APICODE_PARAM , $oCheck->error());
		}

		$this->_doModify($id , $aInput) or $this->output_json(APICODE_BUSY);

		$this->output_json(APICODE_SUCC);
	}
	protected function _doModify($id , $aData){

		return $this->model->modifyById($id , $aData,$this->adm_id) or $this->redirect('busy');

	}
	public function doChange(){
		$field = Tool_input::input('field');
		$value = Tool_input::input('value');
		$id = Tool_input::input('id');
		if(!in_array($field , $this->modifyInListField)){
			$this->output_json(APICODE_PARAM,'field');
		}

		$dataConfig = $this->getDefineXml();
		$oTool = new ml_lib_dataFactory_tool();
		$oTool->setDataConfig($dataConfig['field']);
		$options = $oTool->getEnumOptions($field);
		if(!isset($options[$value])){
			$this->output_json(APICODE_PARAM , 'value');
		}

		$this->_doChange($id , $field , $value);
		$this->output_json(APICODE_SUCC);
	}
	protected function _doChange($id , $field , $value){
		return $this->model->modifyById($id , $aData,$this->adm_id) or $this->redirect('busy');
	}
	/**
	 * format input data
	 * @param  array $aInput [description]
	 * @return array         [description]
	 */
	protected function _formatInputInForm($aInput){
		return $aInput;
	}

}