<?php

/**
 *@fileoverview: 日志投放
 *@important 
 *@author: 辛少普 <shaopu>
 *@date: Wed Apr 27 06:09:00 GMT 2011

 */
include(SYS_INC_PATH.'/tool/Tool_os.php');
include(SYS_INC_PATH.'/tool/Tool_ip.php');

class Tool_logger
{
    const LOG_LEVEL_ALERM = 'alarm';
    const LOG_LEVEL_ERROR = 'error';
    const LOG_LEVEL_NOTICE = 'notice';
    
    static private $log_path = '/www/web_logs/';
    static private $separator = '|';
    static $runningLog;
    
    public static function setLogPath($path){
        if(is_dir($path)){
            self::$log_path = $path;
        }
    }

    /**
     * 调试LOG
     * @param  string  $type  
     * @param  string  $msg   
     * @param  boolean $force 
     * @return boolean         
     */
    public static function debugLog ($type, $msg , $force = false) {
        if(SYSDEF_DEBUG || $force)
        {
            self::_firephplog($msg , $type);
            
            return self::_writeLogToFile($type, time() . self::$separator . $msg);
        }
        return true;
    }
    /**
     * 监控LOG
     * 根据LOG进行系统运行状态监控
     *
     * @param string $type
     * @param string $msg
     * @param string $level Tool_logger::LOG_LEVEL_ALERM
     */
    public static function monitorLog ($type , $msg , $level)
    {

        self::_firephplog($msg.' '.$level , $type);
        
        $msg = '['.$level.']'. $msg;
        
        return self::_writeLogToFile($type, $msg);
    }
    /**
     * 数据投放
     * 需要长期跟进的 需要设置数据回收机制
     *
     * @param string $type
     * @param string $msg
     * @param bool $force
     * @param bool $savebydate
     * @param bool $extinfo
     * @return unknown
     */
    public static function dataLog ($type , $msg , $force = true , $savebydate = true , $extinfo = false)
    {  
        return self::_writeLogToFile($dir, $msg , $savebydate , $extinfo);
    }
    
    public static function baseActionLog($usersign , $action_code , $data)
    {
        $dir = 'action_log';
        
        $sep = self::$separator;
        $msg = date('Y-m-d H:i:s') 
            .$sep. Tool_ip::get_real_ip() 
            .$sep. $usersign
            .$sep. $action_code
            .$sep. $_SERVER['SCRIPT_NAME']
            // .$sep. Tool_ip::getLocalLastIp()
            .$sep. self::_formatActLog($data);
        return self::_writeLogToFile($dir, $msg , true , false);
    }
    
    public static function runningLog($class , $act , $msg)
    {
        self::_firephplog($msg,$act);
        self::$runningLog[] = array(
            'class' => $class,
            'act' => $act,
            'msg' => $msg,
        );
        return true;
    }
    
    public static function saveRunningLog()
    {
        if(count(self::$runningLog) > 0)
        {
            $log =$_SERVER['REQUEST_URI']."\n";
            foreach (self::$runningLog as $value) {
                    $log.=$value['class'].' '.$value['act'].' '.$value['msg']."\n;";
            }
            $log.="\n";

            $dir = "/RUNNING";
            return self::_writeLogToFile($dir, $log , true , false);
        }
        else
            return true;
    }

    /**
     * 心跳LOG
     *
     * @param string $name
     * @return bool
     */
    public static function heartbeatLog($name)
    {
        $file = self::$log_path."/heartbeat";
        Tool_os::checkDir($dir);
        $file .= '/'.$name.'.log';
            
        return touch($name);
    }
    

    
    private static function _formatActLog($array)
    {
        return implode(',' , $array);
    }
    
    public static function oneLog($type , $name , $content)
    {
        
        $dir = self::$log_path . "/" . $type;
        Tool_os::checkDir($dir);
        
        $path = $dir.'/'.$name;
        return file_put_contents($path , $content);
        
    }
    
    /**//**
     * Description
     * @param type $path 相对路径
     * @param type $msg 
     * @param type $savebydate 
     * @param type $extinfo 当前状态信息 IP,请求地址，
     * @return type
     */
    private static function _writeLogToFile ($path, $msg, $savebydate = true , $extinfo = true) {

        if(!Tool_os::isSafePath($path)){
            $path = 'DANGER';
            $msg = 'DANGER_PATH '.$msg;
        }
        Tool_os::checkDir(self::$log_path.$path);

        $log_name = '';
        $now = time();
        $now_date = date("Y-m-d", $now);
        $now_time = date("Y-m-d H:i:s", $now);
        if ($savebydate) {
            $fname = $path . '/' . $now_date . "_" . Tool_ip::getLocalLastIp() . '.v7.log';
        } else  {
            $fname = $path . '/file.log';
        }
        
        $filepath = self::$log_path.$fname;


        if ($extinfo)  {
            $msg = $msg . SELF;
            $msg .= $now . self::$separator . $now_time;
            $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
            $msg .= self::$separator . ($ip == '' ? $_SERVER['REMOTE_ADDR'] : $ip);
            $msg .= self::$separator . $_SERVER['SCRIPT_NAME'];
        }
        $msg .= "\n";

        $fp = fopen($filepath, 'a');
        if ($fp)
        {
            $r = fwrite($fp, $msg);
            fclose($fp);
            return $r;
        }
        return false;
    }
    private static function _firephplog($s , $name)
    {
        // return;
        if(SYSDEF_DEBUG && isset($_SERVER['SERVER_NAME']))
        {
            require_once(SYS_INC_PATH.'/lib/firephp/Lib_firephp.php');
            $firephp = FirePHP::getInstance(true);
            $firephp->log($s, $name);
        }
        return ;
    }
    
}