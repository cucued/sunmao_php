<?php
class Tool_encrypt
{
    public function md5($str)
    {
        $salt = substr($str , 0 , 2).strlen($str);
        return md5($str.$salt);
    }
    public function convert10to62($num){  
	    if(57731386986<$num) return $num.'';  
	    $num_arr  = array(56800235584,916132832,14776336,238328,3844,62);  
	    $str = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';  
	      
	    $num_str = '';  
	    foreach($num_arr as $n){  
	        if($num>$n)  
	        {  
	            $num_idx = intval($num/$n);  
	            $num = $num % $n;  
	            $num_str.=$str[$num_idx];  
	        }else if($num_str!='') $num_str.='0';  
	    }  
	    return $num_str.$str[$num];  
	}
	function convert62to10($num){  
    $str = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';  
    $len = strlen($num);  
    $rtn = 0;  
    while($len>0){  
        $cur = substr($num,0,1);  
        $idx = strpos($str, $cur);  
        $rtn += pow(62,$len-1)*$idx;  
        $num = substr($num,1);  
        $len = strlen($num);  
    }  
    return $rtn;  
} 
}
// $t = time();
// echo $t.'--';
// $s = Tool_encrypt::convert10to62($t);
// echo $s.'--';
// echo Tool_encrypt::convert62to10($s);
?>