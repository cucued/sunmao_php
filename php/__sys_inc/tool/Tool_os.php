<?php
class Tool_os
{
    static public function run_cmd($cmd)
    {
        $fp = popen($cmd , 'r');
        while (!feof($fp))
            $rs .= fgets($fp);
        fclose($fp);
        
        return $rs;
    }
    static public function isSafePath($path){
    	return preg_match('/^[a-zA-Z0-9\/_]*$/' , $path);
    }
    static public function readdir($dir , $type = ''){
        $dp = opendir($dir);
        $aFile = array();
        while ($file = readdir($dp)) {
            if($file == '..' || $file == '.'){
                continue;
            }
            if($type){
                switch ($type) {
                    case 'dir':
                        if(is_dir($dir.'/'.$file)){
                            $aFile[] = $file;
                        }
                        break;
                    
                }
            }else{
                $aFile[] = $file;
            }
        }
        return $aFile;
    }
    /**
     * 检查并创建目录
     *
     * @param string $path
     */
    static public function checkDir($path)
    {
        if (! is_dir($path)) {

            mkdir($path , 0755 , true);
            //chown(SYSDEF_LOG_DEBUG_PATH, "www");
            //chgrp(SYSDEF_LOG_DEBUG_PATH, "www");
        }
        return ;
    }
}
