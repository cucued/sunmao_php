<?php
class Tool_xmlReader{
	static public function xml2array($string){
		$o = new SimpleXMLElement($string);
		return self::iterator($o);
	}
	static private function iterator($object){
		
		$rs = array();
		foreach ($object->attributes() as $key => $value) {
			$rs[$key] = (string)$value;
		}
		
		/*
		if($object->count() < 1 && !empty((string)$object)){
			// echo $object->getName();
			// var_dump((string)$object);
			// $rs['value'] = (string)$object;
		}else{
			*/
			foreach ($object->children() as $key => $second_gen) {
				$rs[$key][] = self::iterator($second_gen);

			}
		// }
		
		return $rs;
	}
}

/*
$a = <<<aa
<?xml version="1.0" encoding="UTF-8"?>
<menu>
	<element_group title="个人信息" icon="" power_id="10000">
		<element type="link" title="查看个人信息" icon="" url="/me/edit" power_id=""/>
		<element type="link" title="修改密码" icon="" url="/me/editpasswd" power_id=""/>
		<element type="line">
			<aa>xxxx</aa>
		</element>
	</element_group>
	<element_group title="组织架构及人员管理" icon="" power_id="20000">
		<element type="link" title="部门设置" icon="" url="/company/organize/" power_id="20110"/>
		<element type="link" title="职位设置" icon="" url="/company/job" power_id="20120"/>
		<element type="link" title="员工管理" icon="" url="/company/staff" power_id="20210"/>
		<element type="link" title="考勤管理" icon="" url="/company/attendance" power_id="20220"/>
	</element_group>
</menu>
aa;

$a = Tool_xmlReader::xml2array($a);
var_dump($a);
*/