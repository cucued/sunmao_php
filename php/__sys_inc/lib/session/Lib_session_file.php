<?php

class Lib_session_file extends Lib_session{
	private $dir_path = '/var/php_session/';
	public function set_dir($dir){

		session_save_path($dir);
	}
	protected function _init($session_id){
		if(!is_dir($this->dir_path)){
			mkdir($this->dir_path);
		}

		
		session_start();
	}
	protected function _dump(){
		
	}
}