<?php
class Lib_session
{
	private $cookie_name = 'ses';
	private $domain;
	private $expire = 86400;

	private $session_id;
	private $inited = false;
	static private $instance;

	private function __construct(){}

	static public function get_instance(){
		if(!is_object(self::$instance)){
			$class = get_called_class();

			self::$instance = new $class;
		}
		return self::$instance;
	}
	public function set_domain($domain){
		$this->domain = $domain;
	}
	public function set_cookie_name($cookie_name){
		$this->cookie_name = $cookie_name;
	}
	public function set_expire($expire){
		$this->expire = $expire;
	}
	public function init(){
		session_name($this->cookie_name);
		if(!$_COOKIE[$this->cookie_name]){
			$this->session_id = md5(uniqid(microtime()));
			session_id($this->session_id);
		}else{
			$this->session_id = $_COOKIE[$this->cookie_name];
		}
		
		session_set_cookie_params($expire, "/", $this->domain);
		session_start();
		return true;
	}

	public function getVal($key){
		return $_SESSION[$key];
	}
	public function setVal($key , $val){
		return $_SESSION[$key] = $val;
	}
	public function getValAll(){
		return $_SESSION;
	}


	
	protected function _dump(){
		
	}
}
