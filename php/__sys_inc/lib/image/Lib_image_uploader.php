<?php
class Lib_image_uploader
{
    private $key;
    private $dir;
    private $filename;
    static $image_type2sign = array(
        'image/gif' => 'ig',
        'image/png' => 'ip',
        'image/jpeg' => 'ij',
    );
    private $image_info = array();
    const MAX_SIZE = 5120;

    
    public function is_set($key)
    {
        return $_FILES[$key]['tmp_name'] ? true : false;
    }
    
    public function start($key)
    {
        $this->reset();
        
        if(!isset($_FILES[$key]))
            return false;
        $this->key = $key;
        
        return true;
    }
    /**
     * 设置保存目录
     *
     * @param string $dir
     * @return bool
     */
    public function set_save_dir($dir)
    {

        Tool_os::checkDir($dir);
        $this->dir = $dir;
        return true;
    }
    /**
     * 检查文件类型
     *
     * @param array $aList  //mime 数组
     * @return bool
     */
    public function check_content_type($aList = array())
    {

        $mime = $_FILES[$this->key]['type'];
        return isset(self::$image_type2sign[$mime]) ? true : false;
    }
    /**
     * 检查文件大小
     *
     * @param int $max  //k
     */
    public function check_file_size($max = self::MAX_SIZE)
    {

        $size_k = (int)$_FILES[$this->key]['size']/1024;

        if($size_k < 0 || $size_k > (int)$max)
            return false;
        
        return true;
        
    }
    public function get_image_info(){
        $image_info = getimagesize($_FILES[$this->key]['tmp_name']);
        // var_dump($image_info);
        $this->image_info['width'] = $image_info[0];
        $this->image_info['height'] = $image_info[1];
        $this->image_info['mime'] = $image_info['mime'];
    }
    public function encode_image_name($isWaterMark = true){
        $this->get_image_info();
        // var_dump($this->image_info);
        $md5 = md5(file_get_contents($_FILES[$this->key]['tmp_name']));
        return substr($md5 , 0 , 5)
                .($isWaterMark ? 1 : 0)
                .Tool_encrypt::convert10to62(time())  //6
                .self::$image_type2sign[$this->image_info['mime']]  //2
                .str_pad( Tool_encrypt::convert10to62( $this->image_info['width']) , 3 , '0' , STR_PAD_LEFT) //3
                .str_pad(Tool_encrypt::convert10to62($this->image_info['height']) , 3 , '0' , STR_PAD_LEFT)  //3
                ;
    }
    static public function decode_image_name($img_name){
        $sign2type = array_flip(self::$image_type2sign);
        $sign = substr($img_name , 12,2);
        $mime = $sign2type[$sign];
        $rs = array(
                'time' => Tool_encrypt::convert62to10(substr($img_name,6,6)),
                'width' => Tool_encrypt::convert62to10(substr($img_name,14,3)),
                'height' => Tool_encrypt::convert62to10(substr($img_name,17,3)),
                'isWaterMark' => substr($img_name , 5,1) == 1 ? true : false,
                'mime' => $mime
            );
        // var_dump($rs);
        return $rs;
    }
    /**
     * 设置保存文件名
     *
     * @param string $name
     * @return bool
     */
    public function set_file_name($name , $add_suffix = false)
    {
        $suffix = '';
        if($add_suffix)
        {
            $suffix = substr($_FILES[$this->key]['name'] , strrpos($_FILES[$this->key]['name'] , '.')) ;
        }
        
        $this->filename = $name.$suffix;
        return true;
    }
    public function get_file_name()
    {
        return $this->filename;
    }
    /**
     * 保存文件
     *
     * @param bool $is_overwritten
     * @return bool
     */
    public function save($is_overwritten = false)
    {
        
        $file_path = $this->dir.'/'.$this->filename;
        
        if(is_file($file_path) && !$is_overwritten)
        {
            return false;
        }
        $rs = move_uploaded_file($_FILES[$this->key]['tmp_name'] , $file_path);
        if($rs)
            return true;
        else 
            return false;
        
    }
    public function reset()
    {
        $this->key = '';
        $this->dir = '';
        $this->filename = '';
    }
}