<?php

class Lib_cache_file implements Lib_cache_base 
{
	private $path;
	private $expire_sec;
    public function init_cache($u , $s){
    	return true;
    }
    public function connect($server){
    	$this->path = $server;
    }
    
    public function get($key){
    	$file_path = $this->_calc_filepath($key);
    	if(is_file($file_path)){
    		$cache_data = unserialize(file_get_contents($file_path));
    		if($cache_data['expire'] != 0 && $cache_data['expire'] < time()){
    			return null;
    		}else{
    			return $cache_data['data'];
    		}
    	}else{
    		return null;
    	}
    }
    
    public function set($key , $value){
    	$file_path = $this->_calc_filepath($key);
    	$cache_data = array(
    		'expire' => ($this->expire_sec ? time()+$this->expire_sec : 0),
    		'data' => $value,
    	);
    	$cache_data = serialize($cache_data);
    	file_put_contents($file_path, $cache_data);
    	return true;
    }
    
    public function delete($key){
    	$file_path = $this->_calc_filepath($key);
    	unlink($file_path);
    }
    
    public function  close(){
    	return true;
    }
    
    public function expire($time){
    	$this->expire_sec = $time;
    }

    public function compressed(){

    }

    private function _calc_filepath($key){
    	return $this->path.'/c_'.$key.'.dat';
    }
}

?>
