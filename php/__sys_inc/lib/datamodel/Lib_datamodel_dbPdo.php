<?php
class Lib_datamodel_dbPdo extends Lib_datamodel_dbBase{
	private $_conn;                                 //当前DB连接
	private $_rs;
	private $_affected_rows;
	public function connect($host , $port , $user , $pw , $db_name){
		$param = "mysql:host=".$host.";dbname=".$db_name;
		try {
            $this->_conn = new PDO($param,$user,$pw); 
        } catch(PDOException $e){
        	
            $this->_error = $e->getMessage();
            return false;
        }
        return true;
	}
	public function query($sql){
		try{
        	$this->_rs = $this->_conn->query($sql);
        } catch(PDOException $e){
    		$this->_error = $e->getMessage();
            return false;
        }
        return true;
	}
	public function return_all_rows(){
		while($row = $this->_rs->fetch()){
            $data[] = $row;
        }
        return $data;
	}
	public function exec($sql){
		try{
			$this->_affected_rows = $this->_conn->exec($sql);
        } catch(PDOException $e){
    		$this->_error = $e->getMessage();
            return false;
        }
        return true;
	}
	public function escape($s){
		return addslashes($s);
	}
	public function last_insert_id(){
		return $this->_conn->lastInsertId();
	}
	public function get_affected_num(){
		return $this->_affected_rows;
	}

}