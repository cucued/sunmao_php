<?php
/**
 *@fileoverview: [群博客] 数据模型基类
 *@author: 辛少普 <shaopu>
 *@date: Tue Nov 30 05:16:19 GMT 2010

 *@
 */

class Lib_datamodel_db extends Lib_datamodel_abstract
{
    private $db_connecter_type = 'mysql';
    private $db_connecter;
    private $is_debug;

    private static $_connections;                   //连接池
    const LOG_DIR = 'ml_model';                     //log目录名
    const DEBUG = true;                             //调试模式 记录所有SQL
    const DB_MASTER = 'master';                        
    const DB_SLAVE = 'slave';    
    
    private $_datamodel_name;                       //数据模型名称
    private $_db_config;                            //当前模型的数据库配置
    private $_conn;                                 //当前DB连接
    private $_fetch_result_num = false;

    protected $_is_ctime = false;
    protected $_is_utime = true;
    
    protected $table;                               //当前的数据表名
    protected $field2type = array();


    /**
     * 构造函数
     * @todo 按项目进行DB配置分文件
     *
     */
    public function __construct($db_name , $db_stdconf)
    {
        //忽略容错

        $this->_datamodel_name = $db_name;
        $this->_db_config = $db_stdconf[$db_name];
    }
    public function setField2Type($arr){
        $this->field2type = $arr;
    }

//内部方法~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private function _get_connecter_class(){
        switch ($this->db_connecter_type) {
            case 'mysql':
                return 'Lib_datamodel_dbMysql';
                break;
            case 'pdo':
                return 'Lib_datamodel_dbPdo';
                break;
        }
    }

    /**
     * 连接MYSQL
     *
     * @param string $host      
     * @param string $port
     * @param string $user
     * @param string $pw
     * @param string $db_name
     * @return bool
     */
    private function _connect($host , $port , $user , $pw , $db_name)
    {

        //连接池的KEY
        $hash_key = md5($host.$port.$user.$db_name);
        
        if(isset(self::$_connections[$hash_key])){
            $this->_conn = self::$_connections[$hash_key];
            //is connecting?
        }else{
            $class = $this->_get_connecter_class();
            
            $this->_conn = new $class();
    
            $start = $this->_microtime();
            
            //连接数据库
            Tool_logger::runningLog(__CLASS__ , 'connect',$host.':'.$port);
            $rs = $this->_conn->connect($host , $port , $user , $pw , $db_name);

            if(!$rs){
                Tool_logger::monitorLog(__CLASS__ , 'connect_err '.$host.' '.$user.' '.$this->_conn->error() , Tool_logger::LOG_LEVEL_ALERM );
                return false;
            }
            
            
            $t = $this->_microtime() - $start;
            //记录时间
            if($t > 1)
                Tool_logger::monitorLog(__CLASS__ , 'conn '.$t.' '.$host.':'.$port , Tool_logger::LOG_LEVEL_NOTICE );
                
            self::$_connections[$hash_key] = $conn;
        }

        return true;
    }
    private function _microtime()
    {
        return array_sum(explode(' ' , microtime()));
    }
 
//子类中使用的方法~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /**
     * 数据库操作准备 每次数据库操作之前调用 内部封装数据库的分库和分表逻辑
     *
     * @param mix $hash_key         //哈希依据  博客默认为UID
     * @param unknown_type $type    //数据库类型 master=主库 slave=从库 other根据db配置选择（比如文章列表专用）
     * @return bool                 //连接是否成功
     */
    protected function init_db($hash_key = '' , $type = self::DB_MASTER)
    {

        if(!isset($this->_db_config['connect'][$type]))
        {
            
            Tool_logger::monitorLog(__CLASS__ , 'db_connect_type_err '.$this->_datamodel_name.' '.$type , Tool_logger::LOG_LEVEL_ALERM );
            return false;
        }
        
        //分库
        $db_key = 0;
        $n = count($this->_db_config['connect']['host'][$type]);
        if($n > 1)
            $db_key = Tool_shop::calc_hash_db($hash_key , $n);
            

        $host    = $this->_db_config['connect'][$type]['host'][$db_key];
        list($host , $port) = explode(':' , $host);
        $user    = $this->_db_config['connect'][$type]['user'];
        $pw      = $this->_db_config['connect'][$type]['pw'];
        $db_name = $this->_db_config['connect'][$type]['name'];
     
        //连接DB
        $rs = $this->_connect($host,$port,$user,$pw,$db_name);

        if(!$rs)
            return false;
            
        //分表
        if(!$this->table){
            if($this->_db_config['tb_n'] > 1)
                $this->table = $this->_db_config['tb_prefix'].$this->hash_table($hash_key);
            else 
                $this->table = $this->_db_config['tb_prefix'] ? $this->_db_config['tb_prefix'] : $this->table;
        }
        return true;
    }
    protected function hash_table($hash_key)
    {
        return Tool_shop::calc_hash_tbl($hash_key , $this->_db_config['tb_n']);;
    }
    
    /**
     * 执行SQL
     *
     * @param string $sql
     * @param bool $return_data //是否需要返回数据
     * @return bool             //操作是否成功
     */
    protected function query($sql , $return_data = false)
    {
        
        if ($this->is_debug){
            
            $this->lastSQL = $sql;
            $this->allSQL[] = $sql;
        }

        $this->_data = array();
        
        
            
        $start = $this->_microtime();
        if($return_data){
            $rs = $this->_conn->query($sql);
        }else{
            $rs = $this->_conn->exec($sql);
        }
        $t = $this->_microtime() - $start;
        
        Tool_logger::runningLog(__CLASS__ , 'query' , $sql.' '.$t);

        if(!$rs)
        {
            Tool_logger::monitorLog(__CLASS__ , 'query_err '.$sql.' '.$this->_conn->error() , Tool_logger::LOG_LEVEL_ALERM );
            return false;
        }
        if( $t > 1)
            Tool_logger::monitorLog(__CLASS__ , 'slow '.$sql.' '.$t , Tool_logger::LOG_LEVEL_NOTICE );
        
        if($return_data){
            $this->_data = $this->_conn->return_all_rows();
            $this->hook_after_fetch();
        }
        return true;
    }

    /**
     * 取记录
     *
     * @param string $sql
     * @return bool
     */
    protected function fetch($sql)
    {
        if(!$this->init_db('' , self::DB_SLAVE))
            return false;
        return $this->query($sql , true);
    }
    /**
     * 取单行记录
     *
     * @param string $sql
     * @return bool
     */
    protected function fetch_row($sql)
    {
        if(!$this->init_db('' , self::DB_SLAVE))
            return false;
        $rs = $this->query($sql , true);
        if(!$rs)
            return false;
            
        $this->_data = $this->_data[0];
        return true;
    }
    /**
     * 取行数
     *
     * @param string $where 条件
     * @return bool
     */
    protected function fetch_count($where = '')
    {
        if(!$this->init_db('' , self::DB_SLAVE))
            return false;

        $where ? $where = ' WHERE '.$where : '';
        $sql = 'SELECT count(*) n FROM '.$this->table.$where;
        
        $rs = $this->query($sql , true);
        if(!$rs)
            return false;
            
        $a = array_values($this->_data[0]);
        $this->_data = $a[0];

        return true;
    }
    /**
     * 插入新记录
     *
     * @param array $array
     * @return bool
     */
    protected function insert($array)
    {
        if(!$this->init_db('' , self::DB_MASTER))
            return false;

        $array = $this->hook_before_write($array);

        if($this->_is_ctime)
            $array['_ctime'] = date('Y-m-d H:i:s');
        
        $sql_set = $this->format_set_sql($array);
        
        if(!$sql_set)
            return false;
            
        $sql = 'INSERT INTO `'.$this->table.'` '.$sql_set;
        
        return $this->query($sql);
    }
    protected function insert_multi($array)
    {
        if(!$this->init_db('' , self::DB_MASTER))
            return false;

        $array = $this->hook_before_write($array);

        if($this->_is_ctime){
            foreach ($array as &$row) {
                $row['_ctime'] = date('Y-m-d H:i:s');
            }
        }

        $sql_set = $this->format_set_sql_multi($array);
        
        if(!$sql_set)
            return false;
            
        $sql = 'INSERT INTO `'.$this->table.'` '.$sql_set;
        
        return $this->query($sql);
    }
    /**
     * 覆盖操作
     *
     * @param array $array
     * @return bool
     */
    protected function replace($array)
    {
        if(!$this->init_db('' , self::DB_MASTER))
            return false;
        $array = $this->hook_before_write($array);

        if($this->_is_utime)
            $array['_utime'] = date('Y-m-d H:i:s');

        $sql_set = $this->format_set_sql($array);
        if(!$sql_set)
            return false;
            
        $sql = 'REPLACE INTO `'.$this->table.'` '.$sql_set;
        return $this->query($sql);
    }
    /**
     * 更新操作
     *
     * @param array $array
     * @param string $where
     * @param int $limit
     * @return bool
     */
    protected function update($array , $where , $limit = 0)
    {
        if(!$this->init_db('' , self::DB_MASTER))
            return false;

        $array = $this->hook_before_write($array);

        if($this->_is_utime)
            $array['_utime'] = date('Y-m-d H:i:s');

        $sql_set = $this->format_set_sql($array);
        if(!$sql_set)
            return false;
            
        $sql = 'UPDATE `'.$this->table.'` '.$sql_set.' WHERE '.$where.($limit > 0 ? ' LIMIT '.$limit : '');
        
        return $this->query($sql);
    }
    protected function delete($where){
        if(!$this->init_db('' , self::DB_MASTER))
            return false;

        $sql = 'DELETE FROM `'.$this->table.'` WHERE '.$where;
        
        return $this->query($sql);
    }
    
    /**
     * 安全过滤
     *
     * @param string $s
     * @return string
     */
    protected function escape($s)
    {
        if($this->_conn){
            return $this->_conn->escape($s);
        }else{
            return addslashes($s);
        }
    }
    /**
     * 格式化 SET `aa`='b',`cc`='d'
     *
     * @param array $array  //array([field] => {value},)
     * @return string
     */
    protected function format_set_sql($array)
    {
        if(!$array)
        {
            Tool_logger::monitorLog(__CLASS__ , __FUNCTION__.' data_null' , Tool_logger::LOG_LEVEL_ALERM );
            return '';
        }    
        
        
        $rs = '';
        foreach ($array as $k => $v)
        {
            $rs .= '`'.$k.'` = "'.$this->escape($v).'",';
        }
        $rs = ' SET '.rtrim($rs , ',');
        return $rs;
    }
    /**
     * 格式化 SET `aa`='b',`cc`='d'
     *
     * @param array $array  //array([field] => {value},)
     * @return string
     */
    protected function format_set_sql_multi($array)
    {
        if(!$array)
        {
            Tool_logger::monitorLog(__CLASS__ , __FUNCTION__.' data_null' , Tool_logger::LOG_LEVEL_ALERM );
            return '';
        }    
        
        
        $aRow = array();

        foreach ($array as $row)
        {
        
            $fields = '(`'.implode('`,`' , array_keys($row)).'`) values';


            $row = '("'.implode('","' , $row).'")';
            $aRow[] = $row;
        }
        $rs = $fields.' '.implode(',' , $aRow);
        
        return $rs;
    }
//公用方法~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    
    /**
     * 取最后INSERT 的数据ID
     *
     * @return int
     */
    public function insert_id()
    {
        return $this->_conn->last_insert_id();
    }
    /**
     * 取写操作影响的行数
     *
     * @return int
     */
    public function affected_rows()
    {
        return $this->_conn->get_affected_num();
    }
    
    
    /**
     * 在调试模式下获得最后一次执行的SQL语句
     * @return string
     */
    public function getLastSQL(){
        if (!SYSDEF_DEBUG){
            return 'please define SYSDEF_DEBUG = true';
        }
        return $this->lastSQL;
    }
    /**
     * 在调试模式下获得所有执行过的SQL语句数组
     * @return array
     */
    public function getAllSQL(){
        if (!SYSDEF_DEBUG){
            return 'please define SYSDEF_DEBUG = true';
        }
        return $this->allSQL;
    }
    
//钩子方法
    protected function hook_after_fetch(){}
    
    protected function hook_before_write($array)
    {
        return $array;
    }
}
?>