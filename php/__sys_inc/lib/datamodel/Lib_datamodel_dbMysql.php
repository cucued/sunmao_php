<?php
class Lib_datamodel_dbMysql extends Lib_datamodel_dbBase{
	private $_conn;     
	private $_rs;                            //当前DB连接
	public function connect($host , $port , $user , $pw , $db_name){
		$this->_conn = mysql_connect($host.':'.$port , $user , $pw);
		if(!$this->_conn){
			$this->_error =  'mysql_connect_error:'.mysql_error();
			return false;
		}
		mysql_set_charset('utf8' , $this->_conn);
        $rs = mysql_select_db($db_name , $this->_conn);
        if(!$rs){
        	$this->_error = 'mysql_select_db_error:'.mysql_error();
        	return false;
        }
        return true;
	}
	public function query($sql){
		$this->_rs = mysql_query($sql , $this->_conn);
		
		if(!$this->_rs){
			$this->_error = mysql_error();
			return false;
		}
		return true;
	}
	public function return_all_rows(){
		while (($row = mysql_fetch_array($this->_rs,MYSQL_ASSOC)) !== false)
        {
            $data[] = $row;
        }
        return $data;
	}
	public function exec($sql){
		return $this->query($sql);
	}
	public function escape($s){
		return mysql_real_escape_string($s , $this->_conn);
	}
	public function last_insert_id(){
		return mysql_insert_id($this->_conn);
	}
	public function get_affected_num(){
		return mysql_affected_rows($this->_conn);
	}

}