<?php
define('SERVER_ROOT_PATH', dirname(dirname(__file__)).DIRECTORY_SEPARATOR);
define('SYS_INC_PATH' , dirname(__file__).DIRECTORY_SEPARATOR);
define('SYSDEF_CACHE_ROOT_PATH','/tmp');

//php基础设置
date_default_timezone_set('Asia/Shanghai');
set_magic_quotes_runtime(0);
//系统调试开关
define('SYSDEF_DEBUG' , true);


if (SYSDEF_DEBUG){
    ini_set('display_errors' , 1);
    error_reporting(E_ALL ^ E_NOTICE);    
    if(extension_loaded('xhprof'))
        xhprof_enable();
}


require_once(SYS_INC_PATH.'/Autoload.php');
