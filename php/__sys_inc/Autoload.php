<?php
include(SYS_INC_PATH.'/tool/Tool_logger.php');
class Autoload
{

	static $aIncProjectSign = array(
		/*
    	'ml' => array(
    		'check_callback' => 'callback',
    		'dir' => 'dir',
    	),              //当前根目录下
    	*/
	);

	public static function register_autoload_function($name , $class2path_callback){
		$a = explode('::', $class2path_callback);


		if((strpos($class2path_callback,'::') !== false && method_exists($a[0], $a[1]))
			|| (is_string($class2path_callback) && function_exists($class2path_callback))){
			self::$aIncProjectSign[$name] = $class2path_callback;
		}
	}

	public static function class2path($className,$class_prefix=''){
		$aPath = explode('_' , $className);
		array_pop($aPath);
		$a = strtolower(implode(DIRECTORY_SEPARATOR , $aPath)).DIRECTORY_SEPARATOR.$class_prefix.$className.'.php';
		return $a;
	}
	/**
	 * 自动加载
	 * *通用规则
	 * 类名=目录+下划线
	 * 文件名=类名
	 * *INC
	 * 目录必须是首字母大写
	 * *具体项目
	 * 在$aIncProjectSign中定义项目缩写及库文件存在目录
	 *
	 * @param unknown_type $className
	 */
	public static function loadCalss($className)
	{
		
		Tool_logger::debugLog('autoload' , $className);

    	foreach (self::$aIncProjectSign as $callback) {

    		$path = call_user_func($callback , $className);
    		
    		if(is_string($path) && is_file($path)){    			
    			// Tool_logger::debugLog('autoload','load_class:'.$className.' path:'.$path);
    			include($path);
    			return true;
    		}
    	}
		return false;
	}
	public static function _baseClass2Path($className){


		if(preg_match('/^[A-Z]/' , $className)){

			return SYS_INC_PATH.self::class2path($className);
		}else{
			return false;
		}
	}

}

Autoload::register_autoload_function('sys_base' , 'Autoload::_baseClass2Path');

// $class = 'Autoload';
// $a = Autoload::loadCalss($class);
// var_dump($a);

 spl_autoload_register(array('Autoload','loadCalss'));
