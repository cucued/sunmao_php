<?php
class ml_lib_dataFactory_editForm{
	private $dataConfig;
	private $data;
	private $include = array();
	private $js = array();
	private $is_eng = false;
	private $saveUrl;
	private $isEasyui = true;


	public function setIsEng($is_eng){
		$this->is_eng = $is_eng;
	}
	public function setDataConfig($dataConfig){
		
		foreach ($dataConfig as $row) {
			$this->dataConfig[$row['name']] = $row;
		}
		return true;
	}
	public function setData($data){
		$this->data = $data;
	}
	public function setSaveUrl($saveUrl){
		$this->saveUrl = $saveUrl;
	}
	public function setIsEasyui(){
		$this->isEasyui = false;
	}

	private function _addInclude($filename , $type = 'css'){
		if($type == 'css'){
			$line = '<link rel="stylesheet" type="text/css" href="'.BIZSITE_DOMAIN_STATIC.'/admin/css/'.$filename.'">';
		}else if($type == 'js'){
			$line = '<script type="text/javascript" src="'.BIZSITE_DOMAIN_STATIC.'/admin/js/'.$filename.'"></script>';
		}
		$this->include[] = $line;
	}
	private function _addIncludeDir($dir , $type='css'){
		static $history = array();
		$dirname = ADM_ROOT_PATH.$type.'/'.$dir.'/';

		$dp = opendir($dirname) or die('fs');

		while ($filename = readdir($dp)) {

			if(!in_array($filename,array('.','..'))){

				$this->_addInclude($dir.'/'.$filename,$type);
			}
		}
		closedir($dp);
	}
	private function _addJs($name , $content){
		$this->js[$name] = $content;
	}
	private function _format_style($key){
		//编辑器样式
		if(isset($this->dataConfig[$key]['inputStyle'])){
			$style = '';
			$inputStyle = $this->dataConfig[$key]['inputStyle'][0];

			if($inputStyle['width']){
				$style .= 'style="width:'.$inputStyle['width'].'"';
			}
			return $style;
		}
		return '';
	}

	private function _form_input($field , $value = null){
		$html = '<input name="{name}" id="ipt_{name}" type="{type}" class="dfInput {class}" value="{value}" max_charcount="{max_charcount}" {extra}/><span class="bs_df_charcount">{charcount}</span><span class="bs_df_descspan">{desc}</span>';

		$extra = $class = $validType = array();
		$dataConfig = $this->dataConfig[$field];

		

		$style = $this->_format_style($field);
		if($style){
			$extra[] = $style;
		}


		if($this->isEasyui){
			if($dataConfig['format']){
				foreach ($dataConfig['format'] as $formatRow) {
					if(in_array($formatRow['type'] , array('preg'))){
						$validType[] = $formatRow['type'].'[\''.$formatRow['value'].'\']';	
					}else{
						$validType[] = $formatRow['type'].'['.$formatRow['value'].']';
					}
				}
				$extra[] = 'validType="'.implode(',' , $validType).'"';
			}else if(in_array($dataConfig['type'],array('mobile','email'))){
				$extra[] = 'validType="'.$dataConfig['type'].'"';
			}
			if($dataConfig['isRequired']==1){
				$extra[] = 'data-options="required:true"';
			}
			if(in_array($dataConfig['type'] , array('string' , 'email'))){
				$class[] = 'easyui-validatebox';
			}else if(in_array($dataConfig['type'] , array('number' , 'mobile'))){
				$class[] = 'easyui-numberbox';
			}else if(in_array($dataConfig['type'] , array('date'))){
				$class[] = 'easyui-datebox';
			}else if($dataConfig['type'] == 'derivative'){
				$extra[] = 'disabled';
				$this->_addJs('derivative_'.$dataConfig['name'],$this->initDerivativeJs($dataConfig['name'],$dataConfig['derivative_func']));
			}
		}

		$value = (is_null($value) && !is_null($dataConfig['default'])) ? $dataConfig['default'] : $value;

		$html = str_replace('{cn_name}' , $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'] , $html);
		$html = str_replace('{type}' , $dataConfig['type'] == 'password' ? 'password' : 'text' , $html);
		$html = str_replace('{name}' , $dataConfig['name'] , $html);
		$html = str_replace('{max_charcount}' , $dataConfig['max_charcount'] , $html);
		$html = str_replace('{class}' , implode(',',$class) , $html);
		if($dataConfig['type'] == 'string' && $dataConfig['max_charcount']){
			$html = str_replace('{charcount}' , Tool_string::count_all_character($value).'/'.$dataConfig['max_charcount'] , $html);
		}else{
			$html = str_replace('{charcount}','',$html);
		}

		if($extra)
			$html = str_replace('{extra}' , implode(' ' , $extra) , $html);
		if($dataConfig['type'] == 'password'){
			$value = '';
		}
		$html = str_replace('{value}' , $value , $html);
		$html = str_replace('{desc}' , $this->is_eng ? $dataConfig['desc_eng'] : $dataConfig['desc'] , $html);
		
		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html,
		);
	}
	private function _form_select($field , $value){
		$dataConfig = $this->dataConfig[$field];
		$html = '<select name="{name}" {extra}>
							{option}
						</select>
						<span class="bs_df_descspan">{desc}</span>';

		if($dataConfig['isRequired']==1){
			$extra[] = 'data-options="required:true"';
		}

		if($dataConfig['type'] == 'reference'){
			$oDfTool = new ml_lib_dataFactory_tool();
			$oDfTool->setIsEng($this->is_eng);
			$oDfTool->setDataConfig($this->dataConfig);

			$aV2cn = $oDfTool->getReferenceOptions($field);

			foreach ($aV2cn as $key => $v) {
				$options .= '<option value="'.$key.'"'.($key == $value ? ' selected' : '').'>'.$v.'</option>';
			}
		}else if($dataConfig['type'] == 'enum'){
			
			foreach ($dataConfig['option'] as $optionRow) {
				$options .= '<option value="'.$optionRow['value'].'"'.($optionRow['value'] == $value ? ' selected' : '').'>'.($this->is_eng ? $optionRow['name'] : $optionRow['cn_name']).'</option>';
			}
		}
		$style = $this->_format_style($field);
		if($style){
			$extra[] = $style;
		}

		$html = str_replace('{cn_name}' , $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'] , $html);
		$html = str_replace('{name}' , $dataConfig['name'] , $html);
		$html = str_replace('{option}' , $options , $html);
		$html = str_replace('{desc}' , $this->is_eng ? $dataConfig['desc_eng'] : $dataConfig['desc'] , $html);
		if($extra)
			$html = str_replace('{extra}' , implode(' ' , $extra) , $html);
		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html,
		);
	}
	private function _form_text($field , $value){

		$dataConfig = $this->dataConfig[$field];
		$html = '<textarea class="dfInput {class}" id="{name}" name="{name}" max_charcount="{max_charcount}" rows="5" {extra}>{value}</textarea>
						<span class="bs_df_charcount">{charcount}</span>
						<span class="bs_df_descspan">{desc}</span>';
		$html = str_replace('{cn_name}' , $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'] , $html);
		$html = str_replace('{name}' , $dataConfig['name'] , $html);
		$html = str_replace('{desc}' , $this->is_eng ? $dataConfig['desc_eng'] : $dataConfig['desc'] , $html);
		$html = str_replace('{value}' , $value , $html);
		$html = str_replace('{max_charcount}' , $dataConfig['max_charcount'] , $html);
		$html = str_replace('{charcount}' , Tool_string::count_all_character($value).'/'.$dataConfig['max_charcount'] , $html);
		
		

		$css_class = array();
		if($dataConfig['editor'] == 'cleditor'){
			$this->_addInclude('cleditor/jquery.cleditor.min.js','js');
			$this->_addInclude('cleditor/jquery.cleditor.xhtml.min.js','js');
			$this->_addInclude('cleditor/jquery.cleditor.table.min.js','js');
			$this->_addInclude('cleditor/jquery.cleditor.upload.js','js');
			$this->_addIncludeDir('cleditor','css');
$js = <<<js
	$(function() {
    $(".cleditor_text").cleditor();
});
js;
			
			$this->_addJs('cleditor',$js);
			$css_class[] = 'cleditor_text';
		}

		$html = str_replace('{class}' , implode(' ',$css_class) , $html);
		$style = $this->_format_style($field);
		if($style){
			$extra[] = $style;
		}

		if($extra)
			$html = str_replace('{extra}' , implode(' ' , $extra) , $html);

		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html,
		);
	}
	private function _form_datetime($field , $value){
		$dataConfig = $this->dataConfig[$field];
		$value = date('m/d/Y H:i:s',($value ? strtotime($value) : time()));
		$html = '<input name="{name}" class="easyui-datetimebox" required="required" value="{value}"><span class="bs_df_descspan">{desc}</span>';
		$html = str_replace('{name}' , $dataConfig['name'] , $html);
		$html = str_replace('{value}' , date('m/d/Y H:i:s',strtotime($value)) , $html);
		$html = str_replace('{desc}' , $this->is_eng ? $dataConfig['desc_eng'] : $dataConfig['desc'] , $html);
		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html
		);
	}
	private function _form_img_id($field , $value){
		$dataConfig = $this->dataConfig[$field];
		$src = imgsrv_model_upload::get_display_url($value , ML_IMG_SIZE_PINBOARD);
		$bigsrc = imgsrv_model_upload::get_display_url($value , ML_IMG_SIZE_BIG);
		$html = '<img width="50" id="img'.$field.'" src="{src}" bigsrc="{bigsrc}" /><input type="hidden" id="ipt'.$field.'" name="'.$field.'" value="{value}"/><a href="javascript:;" class="aSetCover" field="'.$field.'">upload</a></span><span class="bs_df_descspan">{desc}</span>';
		$html = str_replace('{src}',$src,$html);
		$html = str_replace('{bigsrc}',$bigsrc,$html);
		$html = str_replace('{value}',$value,$html);
		$html = str_replace('{desc}' , $this->is_eng ? $dataConfig['desc_eng'] : $dataConfig['desc'] , $html);
		$this->_addInclude('jquery.ocupload-1.1.2.packed.js','js');
$js = <<<js
$('.aSetCover').click(function(){
		$('#divUpload').attr('callback','uploadImage_cb');
		$('#divUpload').attr('uploadImage_field',$(this).prev('input').attr('name'));
		$('#divUpload').dialog('open');
	})
function uploadImage_cb(img_id,img_url){
		var field = $('#divUpload').attr('uploadImage_field');
		
		$('input[name="'+field+'"]').prev('img').attr('src',img_url);
		$('input[name="'+field+'"]').val(img_id);
		$('#divUpload').dialog('close');
	}
js;
$this->_addJs('add_img_id',$js);
		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html
		);
	}

	public function _form_mix($field , $value){
		$dataConfig = $this->dataConfig[$field];
		$oEditform = new ml_lib_dataFactory_editForm();
		// var
		$oEditform->setDataConfig($dataConfig['field']);
		$oEditform->setIsEasyui();
		$aMixRs = $oEditform->prepare();
		$html = '<table class="bs_adm_formtable">';
		$row_tpl='<tr><td class="nameTd">{cn_name}:</td><td>{html}</td></tr>';
		// var_dump($aMixRs);
		foreach ($aMixRs['fields'] as $key => $mix_row) {
			$mix_row['html'] = preg_replace('/id="[^\"]*"/', '', $mix_row['html']);
			$field_html = $row_tpl;
			$field_html = str_replace('{html}',$mix_row['html'],$field_html);
			$field_html = str_replace('{cn_name}',$mix_row['cn_name'],$field_html);
			$html .= $field_html;
		}
		$html .= '</table>';
		
		foreach ($oEditform->js as $key => $value) {
			$this->_addJs($key , $value);
		}
		foreach ($oEditform->include as $row) {
			$this->include[] = $row;
		}

		return array(
			'cn_name' => $this->is_eng ? $dataConfig['name'] : $dataConfig['cn_name'],
			'html' => $html,
		);
	}
	function prepare($aField = array()){
		$fields = array();

		foreach ($this->dataConfig as $key => $config) {
			if((!empty($aField) && !in_array($key , $aField))// 不需要编辑的
				|| $config['noInput']		//不需要人工输入的
				){
				continue;
			}

			if(in_array($config['type'] ,array('enum' , 'reference'))){
				$fields[$key] = $this->_form_select($key , $this->data[$config['name']]);
			}else if(in_array($config['type'] ,array('text'))){
				$fields[$key] = $this->_form_text($key , $this->data[$config['name']]);
			}else if(in_array($config['type'] ,array('date','time'))){
				$fields[$key] = $this->_form_datetime($key , $this->data[$config['name']]);
			}else if(in_array($config['type'] ,array('img_id'))){
				$fields[$key] = $this->_form_img_id($key , $this->data[$config['name']]);
			}else if(in_array($config['type'] ,array('mix'))){
				$mixfields[$key] = $this->_form_mix($key , $this->data[$config['name']]);
			}else{
				$fields[$key] = $this->_form_input($key , $this->data[$config['name']]);
			}
		}

		$txtSave = $this->is_eng ? 'save' : '保存';
		$submit = '<input type="submit" value="'.$txtSave.'"/><span id="spResult"></span>';

		return array(
			'fields' => $fields,
			'mixfields' => $mixfields,
			'submit' => $submit
		);
	}
	function getFormHtml($aField = array() ,$saveUrl='' , $html = ''){

		$rs = $this->prepare($aField);
		$fields = $rs['fields'];
		$submit = $rs['submit'];

		
		if(empty($html)){
			$html = '<table class="bs_adm_formtable">
					{fields}
					<tr>
					<td width="50px"></td>
					<td>'.$submit.'</td>
				</tr>
				</table>';
			$row = '<tr><td class="nameTd">{cn_name}:</td><td>{html}</td></tr>';

			foreach ($fields as $key => $value) {
				$tmp = str_replace('{cn_name}',$value['cn_name'],$row);
				$tmp = str_replace('{html}',$value['html'],$tmp);
				$rows[] = $tmp;
			}
			$html = str_replace('{fields}' , implode("\n",$rows) , $html);
		}else{
			foreach ($fields as $key => $value) {
				$html = str_replace('{'.$key.'_name}',$value['cn_name'],$html);
				$html = str_replace('{'.$key.'}',$value['html'],$html);
			}
			$html = str_replace('{submit}',$submit,$html);
		}
		$html = '<form id="dataFactoryForm" action="'.$this->saveUrl.'" method="post">'.$html.'</form>';
		return $html;
	}
	
	function getJs($saveUrl = ''){
		$js = <<<js
<script type="text/javascript">

	$('#dataFactoryForm').form({
		url:'{saveUrl}',
		onSubmit:function(){
			return $(this).form('validate');
		},
		success:function(data){
			var rs = bs_str2obj(data);
            if(rs.code == APICODE_SUCC){
              $('#spResult').css('color','green').text(rs.msg).show().fadeOut(2000);
              if(rs.data.id){
	              var url = $('body').attr('url_base')+'/modify?id='+rs.data.id+'&'+$('body').attr('linkParam');
	              window.location=url;
          		}
            }else{
              $('#spResult').css('color','red').text(rs.msg).show();
            }
		}
	});
	
	$('.dfInput').keydown(function(){
		if($(this).attr('max_charcount')){
			var n = countByteLength($(this).val());
			if(n>$(this).attr('max_charcount')){
				$(this).siblings('.bs_df_charcount').css('color','red');
			}
			$(this).siblings('.bs_df_charcount').html(n+'/'+$(this).attr('max_charcount'));
		}
	})
	$('.dfInput').change(function(){
		$(this).trigger('keydown');
	});
	$('.dfInput').focus(function(){
		$(this).trigger('keydown');
	});
	{morejs}

</script>
js;
		$url = $saveUrl ? $saveUrl : $this->saveUrl;
		$js = str_replace('{saveUrl}' , $url , $js);
		
		$js = str_replace('{morejs}' , implode("\n\n",$this->js) , $js);
		return $js;
	}
	function getInclude(){
		return implode("\n",$this->include);
	}

	private function initDerivativeJs($fieldname,$derivative_func){
		preg_match_all('/{[a-zA-z0-9_]*}/U' , $derivative_func , $a);

		$func = $derivative_func;
		foreach ($a[0] as $field) {
			$field = trim($field,'{}');

			$aField[] = $field;
			$func = str_replace('{'.$field.'}' , "$('input[name=\"".$field."\"]').val()" , $func);
		}

		$js_tpl = <<<js

$("#ipt_{field}").numberbox({
	"onChange":function(){
		$('#ipt_{derivative}').val({func});
	}
});
js;
		$js = '';
		foreach ($aField as $field) {
			$js_tmp = str_replace('{field}',$field,$js_tpl);
			$js_tmp = str_replace('{derivative}',$fieldname,$js_tmp);
			$js_tmp = str_replace('{func}',$func,$js_tmp);
			$js.=$js_tmp;
		}
	
		return $js;
	}
}