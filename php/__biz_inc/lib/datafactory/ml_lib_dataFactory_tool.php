<?php
class ml_lib_dataFactory_tool
{
	private $dataConfig;
	private $dataCache;
	private $is_eng = false;
	public function setIsEng($is_eng){
		$this->is_eng = $is_eng;
	}
	public function setDataConfig($dataConfig){
		$this->dataConfig = $this->dataCache = array();
		foreach ($dataConfig as $row) {
			$this->dataConfig[$row['name']] = $row;
		}
		return true;
	}
	public function getFieldNames(){
		foreach ($this->dataConfig as $key => $row) {
			if($row['type'] == 'derivative' || $row['noInput']){
				continue;
			}else{
				$fields[] = $key;
			}
		}
		return $fields;
	}

	public function fields2cnname($aField){
		$aRs = array();
		foreach ($this->dataConfig as $row) {
			if(in_array($row['name'],$aField)){
				$aRs[$row['name']]=$this->is_eng ? $row['name'] : $row['cn_name'];
			}
		}
		return $aRs;
	}
	public function fields2name($aField){
		$aRs = array();
		foreach ($this->dataConfig as $row) {
			if(in_array($row['name'],$aField)){
				$aRs[$row['name']]=$this->is_eng ? $row['name'] : $row['cn_name'];
			}
		}
		return $aRs;	
	}

	public function dataOutput($aField , $aData , $changeInListField = array()){
		$aOutput = array();

		foreach ($aData as &$row) {
			$aRow = array();
			foreach ($aField as $field) {
				//保存原值
				if(in_array($this->dataConfig[$field]['type'] , array('enum','reference','key2value','img_id'))){
					$row[$field.'_ori'] = $row[$field];
				}
				switch ($this->dataConfig[$field]['type']) {
					case 'enum':
						$aV2cn = $this->getEnumOptions($field);
						
						if(in_array($field , $changeInListField)){

							$row[$field] = adm_tool_htmlMaker::select($aV2cn , $row[$field],'','',' class="sel_'.$field.'"onchange="changeById(this)" data_id="'.$row['id'].'" field="'.$field.'"');
						}else{
							$row[$field] = $aV2cn[$row[$field]];
						}
						break;
					case 'reference':
						$aV2cn = $this->getReferenceOptions($field);

						$row[$field] = $aV2cn[$row[$field]];
						break;
					case 'key2value':
						$row[$field] = $this->getKey2value($field,$row[$field]);
						break;
				}
			}
		}
		return $aData;
	}

	private function _fieldOutput($field , $value){
		$fieldConfig = $this->dataConfig[$field];

		switch ($fieldConfig['type']) {
			case 'enum':

				$aV2cn = $this->getEnumOptions($field);
				return $aV2cn[$value];
				break;
			case 'reference':
				$aV2cn = $this->getReferenceOptions($field);
				return $aV2cn[$value];
				break;
			case 'key2value':
				$value = $this->getKey2value($field,$value);
				return $value;
				break;
			
			default:
				return $value;
				break;
		}
	}

	public function getOptions($field){
		return $this->dataConfig[$field]['type'] == 'reference' ? $this->getReferenceOptions($field) : $this->getEnumOptions($field);
	}

	public function getEnumOptions($field){
		$cacheKey = 'enum_'.$field;
		if(!$this->dataCache[$cacheKey]){
			foreach ($this->dataConfig[$field]['option'] as $row) {
				$aV2cn[$row['value']] = $this->is_eng ? $row['name'] : $row['cn_name'];
			}
			
			$this->dataCache[$cacheKey] = $aV2cn;
		}
		return $this->dataCache[$cacheKey];
	}
	public function getReferenceOptions($field){
		$aV2cn = array();

		$reference = $this->dataConfig[$field]['reference'][0];

		$cacheKey = 'refer_'.$field;
		if(!$this->dataCache[$cacheKey]){
			
			$aV2cn = $reference['class']::$reference['method']($reference['param'] , $this->is_eng);

			$this->dataCache[$cacheKey] = $aV2cn;
		}

		return $this->dataCache[$cacheKey];
	}
	public function getKey2value($field , $value){
		$reference = $this->dataConfig[$field]['reference'][0];
		$value = $reference['class']::$reference['method']($value , $this->is_eng);
		return $value;
	}

	public function getMoreField($morefield_config){
		$oEditform = new ml_lib_dataFactory_editForm();
		$oEditform->setDataConfig($morefield_config['field']);
		$aMixRs = $oEditform->prepare();

		$html = '<table>';
		$row_tpl='<tr><td>{cn_name}:</td><td>{html}</td></tr>';
		
		foreach ($aMixRs['fields'] as $key => $mix_row) {

			$tmp = preg_replace('/id="[^\"]*"/', '', $mix_row['html']);

			$field_html = $row_tpl;
			$field_html = str_replace('{html}',$tmp,$field_html);
			$field_html = str_replace('{cn_name}',$mix_row['cn_name'],$field_html);
			$html .= $field_html;
		}
		$html .= '</table>';

		return $html;
	}
}