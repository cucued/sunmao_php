<?php
class ml_lib_dataFactory_checker{
	private $stdChecker = array(
		'email','mobile','date','time','img_id'
	);
	const LANG_ENG = 1;
	const LANG_CHN = 2;
	const DATE_YYYYMMDD = 'Y-m-d';
	const DATE_YYYYMMDDHHIISS = 'Y-m-d H:i:s';

	private $error_field;
	private $error_type;
	private $dataConfig;
	private $lang = array(
		'preg' => '格式不正确',
		'length' => '长度不正确',
		'minmax' => '大小不正确',
		'datetime' => '日期/时间 不正确',
		'email' => '电子邮件格式不正确',
		'mobile' => '手机号格式不正确',
		'img_id' => '图片ID格式不正确',
		'undefined_checker' => '检查器不存在',
		'enum_error' => '所选项不存在',
	);

	public function __construct(){

	}
	public function error(){
		return $this->dataConfig[$this->error_field]['cn_name'].' '.$this->lang[$this->error_type];
	}
	
	public function setDataConfig($dataConfig){
		foreach ($dataConfig as $row) {
			$this->dataConfig[$row['name']] = $row;
		}
		return true;
	}
	public function setLang($aLang){
		$this->lang = $aLang;
	}
	public function check_one($key , $value){

		if(!isset($this->dataConfig[$key])){
			Tool_logger::monitorLog(get_class($this),'no_config:'.$key , Tool_logger::LOG_LEVEL_ERROR);
			return false;
		}
		$fieldConfig = $this->dataConfig[$key];


		//特定类型字段
		if(in_array($fieldConfig['type'] , $this->stdChecker)){

			$methodName = '_checker_'.$fieldConfig['type'];
			Tool_logger::debugLog('checker','check  '.$key.' '.$methodName);
			$rs = $this->$methodName($value);
			if(!$rs){
				$this->error_field = $key;
				Tool_logger::debugLog('checker','checkfalse-'.$methodName);
			}
			return $rs;
		}else if($fieldConfig['type'] == 'enum'){

			$valueList = Tool_array::format_2d_array($fieldConfig['option'],'value',Tool_array::FORMAT_VALUE_ONLY);
			Tool_logger::debugLog('checker','check  '.$key.' enum');
			if(!in_array($value , $valueList)){
				$this->error_field = $key;
				$this->error_type = 'enum_error';
				Tool_logger::debugLog(get_class($this),'check_enum_false:'.$fieldConfig['name']);
				return false;
			}
		}


		if(is_array($fieldConfig['format'])){

			foreach ($fieldConfig['format'] as $formatRow) {
				$methodName = '_checker_'.$formatRow['type'];
				Tool_logger::debugLog('checker' , 'check  '.$key.' '.$methodName);
				if(!method_exists($this , $methodName)){
					$this->error_field = $key;
					$this->error_type = 'undefined_checker';
					Tool_logger::monitorLog(get_class($this) , 'undefined_checker '.$fieldConfig['type'] , Tool_logger::LOG_LEVEL_ERROR);
					return false;
				}else if(!$this->$methodName($value , $formatRow['value'])){
					$this->error_field = $key;
					Tool_logger::debugLog(get_class($this),'checkfalse:'.$methodName);
					return false;
				}
			}
			return true;
		}else{ 
			return true;
		}
	}
	public function check_multi($fields , $fields2values){

		foreach ($fields as $key) {
			$rs = $this->check_one($key , $fields2values[$key]);

			if(!$rs){

				Tool_logger::debugLog(get_class($this),'check_false:'.$key.'---'.$fields2values[$key]);
				return false;
			}
		}
		return true;
	}
	



	private function _checker_preg($value , $format){
		if(!preg_match('/'.$format.'/' , $value)){
			$this->error_type = 'preg';
			return false;
		}else{
			return true;
		}
	}
	private function _checker_length($value , $format , $lang = self::LANG_ENG){
		switch ($lang) {
			case self::LANG_CHN:
				$length = Tool_string::str_width($value);
				
				break;
			case self::LANG_ENG:
			default:
				$length = strlen($value);
				break;
		}

		if(!$this->_checker_minMax($length , $format)){
			$this->error_type = 'length';
			return false;
		}else{
			return true;
		}
	}
	private function _checker_minMax($value , $format){
		//确定最大最小值
		$max = $min = $length = 0;
		//如果不是min,max格式
		if(strpos($format, ',') === false && is_numeric($format)){
			$max = $format;
		}else{
			$a = explode(',',$format);
			$min = (int)$a[0];
			$max = (int)$a[1];
		}

		if( ($min > 0 && $value < $min)
			|| ($max > 0 && $value > $max)){

			Tool_logger::debugLog('check_minmax' , 'v:'.$value.' min:'.$min.' max:'.$max);
			$this->error_type = 'minmax';
			return false;
		}else{
			return true;
		}
	}


	private function _checker_date($value , $format = self::DATE_YYYYMMDD){
		if(!date($format , strtotime($value)) == $value){
			$this->error_type = 'datetime';
			return false;
		}else{
			return true;
		}
	}
	private function _checker_time($value , $format = self::DATE_YYYYMMDDHHIISS){
		return $this->_checker_date($value , $format);
	}
	private function _checker_email($value){
		if(!filter_var($value,FILTER_VALIDATE_EMAIL) == $value){
			$this->error_type = 'email';
			return false;
		}else{
			return true;
		}
	}
	private function _checker_mobile($value){
		if(!(is_numeric($value) && strlen($value) == 11 && $value[0] == 1)){
			$this->error_type = 'mobile';
			return false;
		}else{
			return true;
		}
	}
	private function _checker_img_id($value){
		if(!preg_match('/^[0-9a-zA-Z]{20}$/',$value)){
			$this->error_type = 'img_id';
			return false;
		}else{
			return true;
		}
	}

}