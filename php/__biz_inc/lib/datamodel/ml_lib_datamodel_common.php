<?php
class ml_lib_datamodel_common extends Lib_datamodel_db{
    const STATUS_NORMAL = 1;
    const STATUS_PREPARE = 8;
    const STATUS_DEL = 9;
    private $enableCache = false;
    private $cacheListMaxPage = 3;
    protected $fetchall = false;
	function __construct()
    {
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
        $this->_is_ctime = true;
    }

    public function create($aData , $staff_id){
    	if(!$this->init_db('' , self::DB_MASTER))
            return false;
        $aData['create_user'] = $staff_id;
        
        return $this->insert($aData);
    }
    public function modifyById($id , $aData , $staff_id){
        if(!$this->init_db('' , self::DB_MASTER))
            return false;
        $aData['update_user'] = $staff_id;

        
        $where = 'id='.(int)$id;
        return $this->update($aData,$where,1);
    }

    public function getById($id){
        $cache_key = $this->table.'_id_'.$id;
        $cache = $this->getCache($cache_key);
        if($cache){
            $this->set_data($cache);
            return true;
        }

    	if(!$this->init_db('' , self::DB_MASTER))
            return false;
  		$sql = 'select * from '.$this->table.' where id = '.((int)$id);
  		$rs = $this->fetch_row($sql);

        if($rs && $this->get_data()){
            $this->setCache($cache_key , $this->get_data());
        }
        return $rs;
    }
    public function getByIds($aId){
        if(!$this->init_db('' , self::DB_MASTER))
            return false;
        
        $sql = 'select * from '.$this->table.' where id in ('.implode(',',$aId).')';
        return $this->fetch($sql);
    }


    private function formatCondition($aCondition){
        if(!isset($aCondition['status'])){
            $aCondition['status'] = self::STATUS_NORMAL;
        }
        if(!empty($aCondition)){
            foreach ($aCondition as $field => $value) {
                if($field == 'search'){
                    $aConditionWhere[] = $this->escape($value['field']).' like "%'.$this->escape($value['key']).'%"';
                }else if(!empty($value)){
                    $aConditionWhere[] = $field.'="'.$this->escape($value).'"';
                }
            }
            
            return implode(' and ',$aConditionWhere);
        }else{
            return '';
        }
    }
    public function listByCondition($aFields = array() , $aCondition = array() , $page = 1 , $pagesize = 10,$sort='id',$order='desc'){
        $where = $this->formatCondition($aCondition , $search_field , $search_key);

        if($page < $this->cacheListMaxPage){
            $cache_key = $this->table.'_'.md5($where).'_'.$page.'_'.$pagesize;
            
            $cache = $this->getCache($cache_key);
            if($cache){
                $this->set_data($cache);
                return true;
            }
        }

        if(!$this->init_db('' , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition , $search_field , $search_key);

        $start = ($page-1)*$pagesize;

        if(empty($aFields) || $this->fetchall){
            $fields = '*';
        }else{
            $aFields[] = 'id';
            $fields = '`'.implode('`,`' , array_unique($aFields)).'`';
        }
        $sql = 'select '.$fields.' from '.$this->table
            .($where ? ' where '.$where : '')
            .' order by '.$sort.' '.$order
            .($pagesize > 0 ? (' limit '.$start.','.$pagesize) : '');
            
        $rs = $this->fetch($sql);
        if($rs && $this->get_data() && $page < $this->cacheListMaxPage){

            $this->setCache($cache_key , $this->get_data());
        }
        return $rs;
    }
    public function countByCondition($aCondition = array()){
        if(!$this->init_db('' , self::DB_SLAVE))
            return false;
        $where = $this->formatCondition($aCondition);
        return $this->fetch_count($where);
    }

    protected function getCache($key){
        if($this->enableCache){
            $o = new Lib_cache_proxy('file');
            $o->connect(BIZ_DATACACHE_PATH);
            return $o->get($key);
        }else{
            return false;
        }
    }
    protected function setCache($key , $value , $expire = 600){
        if($this->enableCache){
            $o = new Lib_cache_proxy('file');
            $o->connect(BIZ_DATACACHE_PATH);
            return $o->set($key , $value , $expire);
        }else{
            return true;
        }
    }
}