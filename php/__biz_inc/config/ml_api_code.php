<?php
/**
 *@fileoverview: [群博客] 
 *@author: 辛少普 <shaopu>
 *@date: Tue Nov 30 06:47:05 GMT 2010

 */

define('APICODE_BUSY' , 'A00001');
define('APICODE_IP' , 'A00002');
define('APICODE_PARAM' , 'A00003');
define('APICODE_NOLOGIN' , 'A00004');
define('APICODE_HACK' , 'A00005');
define('APICODE_SUCC' , 'A00006');
define('APICODE_FAIL' , 'A00007');
define('APICODE_NOACTIVE' , 'A00008');

?>