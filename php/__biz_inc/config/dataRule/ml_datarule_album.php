<?php
/**
 *@fileoverview: [美啦] 基本常量定义
 *@author: 王涛 <wangtao5>
 *@date: 2012-07-13

 */

define('ML_ALBUM_ALBUMFEED_PAGE', 10); //画报的分享每页总数
define('ML_ALBUM_ALBUMREPLY_PAGE', 10); //画报回复每页总数
define('ML_ALBUM_ALBUMFEED_USERS', 12); //取得参数画报的总数
define('ML_ALBUM_ALBUMREPLY_USERS', 10); //取得画报分享回复数
