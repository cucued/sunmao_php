<?php

define('BIZ_INC_PATH' , dirname(__file__).DIRECTORY_SEPARATOR);

function getRootDomain($domain){
        $aDomainSuffix = array('com','net','org','cc','me','gov','edu','biz','info','name','mobi');
        $a = explode('.',$domain);
        if(count($a)==2){
            return $domain;
        }
        
        $a = array_slice($a,count($a)-3);
        if(in_array($a[1] , $aDomainSuffix)){
            return implode('.',$a);
        }else{
            return $a[1].'.'.$a[2];
        }
    }
$domain = $_SERVER['HTTP_HOST'];
$root_domain = getRootDomain($domain);
include_once(BIZ_INC_PATH.'/site_config/'.$root_domain.'/config.php');

//设置数据站点目录
define('BIZGLOBAL_DATAPATH',SERVER_ROOT_PATH.'/data/');
include_once(BIZ_INC_PATH.'/config/baseOnSiteConfig.php');


include_once(BIZ_INC_PATH.'/config/ml_config.php');
include_once(BIZ_INC_PATH.'/config/ml_api_code.php');
include_once(BIZ_INC_PATH.'/ml_factory.php');
include_once(BIZ_INC_PATH.'/ml_function_lib.php');
include_once(BIZ_INC_PATH.'/ml_dispatch.php');



function ml_autoload($class){
    if(substr($class,0,3) == 'ml_'){
        $s = BIZ_INC_PATH.Autoload::class2path(substr($class,3),'ml_');

        return $s;
    }else{
        return false;
    }
}
Autoload::register_autoload_function('ml' , 'ml_autoload');

function imgsrv_autoload($class){

    if(substr($class,0,7) == 'imgsrv_'){
        $s = SERVER_ROOT_PATH.'image_server/'.Autoload::class2path(substr($class,7),'imgsrv_');
        return $s;
    }else{
        return false;
    }
}

Autoload::register_autoload_function('imgsrv' , 'imgsrv_autoload');


$oSess = ml_factory::getSession();
$oSess->set_cookie_name(BIZSITE_SESSION_NAME);
$oSess->set_dir(BIZ_SESSION_SAVEPATH);
$oSess->set_domain(BIZSITE_SESSION_DOMAIN);
$oSess->set_expire(3600);
$oSess->init();


function formatTree($array, $parent_id = 0 , $Fparent_id = 'parent_id',$Fvalue = 'value'){
    $arr = array();
    $tem = array();
    foreach ($array as $v) {
        if ($v[$Fparent_id] == $parent_id) {

            $tem = formatTree($array, $v[$Fvalue],$Fparent_id,$Fvalue);
            
            //判断是否存在子数组
            $tem && $v['children'] = $tem;
            $arr[] = $v;
        }
    }
    return $arr;
}