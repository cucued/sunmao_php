<?php
class ml_dispatch
{
	private $index;
	private $prefix;
	private $project_root;
	private $ereg2path = array();

	public function setIndex($index){
		$this->index = $index;
	}
	public function setPrefix($prefix){
		$this->prefix = $prefix;
	}
	public function setRoot($root){
		$this->project_root = $root;
	}
	public function setEreg2path($array){
		$this->ereg2path = $array;
	}



    public function run(){
        $dispatch = $this->dispatch($this->prefix);

        Tool_logger::debugLog(get_class($this),'dispatch:'.$dispatch['class_path'].' '.$dispatch['action']);

        if(!$dispatch){

            die('error_dispatch');
        }else{
            if(is_file($dispatch['class_path'])){

                $init_path = $dispatch['module_path'].'/init.php';
                if(is_file($init_path)){
                    include($init_path);
                }
                include($dispatch['class_path']);

                if(class_exists($dispatch['class_name'])){

                    $controller = new $dispatch['class_name'];

                    if(method_exists($controller , $dispatch['action'])){
                        $controller->$dispatch['action']();
                    }else{
                        die('action_error');
                    }
                }else{
                    die('module_class_error');
                }
            }else{
                die('module_error');
            }
        }
    }

    /**//**
     * Description
     * @param type $prefix 
     * @return type
     */
    private function dispatch($prefix){
        $url = $_SERVER['REQUEST_URI'];
        $aUrl = parse_url($url);

        $rs = array(
                'class_name' => '',
                'class_path' => '',
                'action' => '',
                'module_path' => '',
            );

        //
        
        if(is_array($this->ereg2path)){
        	foreach ($this->ereg2path as $ereg => $info) {
        		if(preg_match('|'.$ereg.'|' , $aUrl['path'])){

        			$rs['action'] = $info['method'];
        			$filename = basename($info['path']);
        			$rs['class_name'] = substr($filename,0,strpos($filename,'.'));
        			$rs['class_path'] = $this->project_root.'module'.$info['path'];

                    $rel_path = ltrim($info['path'],'/');
        			$rs['module_path'] = $this->project_root.'module'.substr($rel_path,0,strpos($rel_path,'/'));
        			return $rs;
        		}
        	}
        }



        //地址示例 /a/b/c?aaaa
        //b是具体的类，c是方法，a或更多的是目录
        //所以如果不写C,则要补一个RUN /a/b/?fsf == /a/b/run?fsf
        if($aUrl['path'] == '/'){
            $aUrl['path'] = $this->index;
        }else if( $aUrl['path']{strlen($aUrl['path'])-1} == '/'){
            $aUrl['path'].='run';
        }
        
        //防止非法字符
        if(preg_match('/^[a-zA-Z0-9\/]*$/' , $aUrl['path'])){
            
            
            $aDir = array_filter(explode('/' , $aUrl['path']));

            if(count($aDir)<3){
                array_unshift($aDir , 'basic');
            }
            
            $rs['action'] = array_pop($aDir);
            
            $class = array_pop($aDir);
            $aDir = array_values(array_filter($aDir));


            $rs['class_name'] = $prefix.($aDir ? implode('_' , $aDir).'_':'').$class;
            $rs['module_path'] = $this->project_root.'module/'.$aDir[0];
            $rs['class_path'] = $this->project_root.'module/'.implode('/' , $aDir).'/'.$rs['class_name'].'.php';
            
            return $rs;
        }
        return false;
    	
    }
}