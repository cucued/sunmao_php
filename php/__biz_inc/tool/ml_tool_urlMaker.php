<?php
class ml_tool_urlMaker
{
    static public function article_show($id,$domain = true){
        return self::domain($domain).'/article/show/'.$id;
    }
    static public function tag_article_list($tag){
    	return self::domain($domain).'/article/tag/'.urlencode($tag);
    }
    static public function category_article_list($channel_alias,$ctg_val){
    	return self::domain($domain).'/'.$channel_alias.'/article/category/'.$ctg_val;
    }
    static private function domain($domain){
    	return $domain ? BIZSITE_DOMAIN_WEB : '';
    }

}

