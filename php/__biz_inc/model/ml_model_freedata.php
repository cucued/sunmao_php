<?php
class ml_model_freedata extends Lib_datamodel_db{
    const TABLE= 'adm_freedata_basic';
    /**
     * 创建构造函数
     *
     */
    function __construct()
    {
        /**
         * 加载数据库配置
         */
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        /**
         * 构造函数
         * 参数：
         * 1，当前模型名称
         * 2，相关数据库配置
         */
        parent::__construct('admin' , $db_config);
    }

    public function getListByType($type){
        if(!$this->init_db())
            return false;

        $sql = 'select * from '.self::TABLE.' where `type` = '.$type;
        return $this->fetch($sql);
    }
    
    public function getByTypeValue($type , $value){
        if(!$this->init_db())
            return false;

        $sql = 'select * from '.self::TABLE.' where `type` = '.$type.' and value='.$value;
        return $this->fetch_row($sql);
    }
    private function _getKVByType($type , $is_eng){

        if(!$this->init_db())
            return false;

        $sql = 'select eng_name,name,value from '.self::TABLE.' where type = '.$type;
        $rs = $this->fetch($sql);
        if(!$rs){
            return false;
        }
        $fldName = $is_eng ? 'eng_name' : 'name';
        $aValue2name = Tool_array::format_2d_array($this->get_data() , 'value' , Tool_array::FORMAT_VALUE2VALUE2 , $fldName);
        $this->set_data($aValue2name);
        return true;
    }
    

    static public function getKVByType($type , $is_eng = false){
        
        $oSelf = new ml_model_freedata();
        $rs = $oSelf->_getKVByType($type , $is_eng);
        if(!$rs){
            return $rs;
        }

        return $oSelf->get_data();
    }
}