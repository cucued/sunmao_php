<?php
class ml_model_freetree extends ml_lib_datamodel_common{
	
    function __construct()
    {
        $this->table = 'adm_freetree';
        parent::__construct();
        $this->fetchall = true;
    }

    
    protected function hook_after_fetch(){
        if($this->_data){
            foreach ($this->_data as &$row) {
                if($row['extra']){
                    $row = array_merge($row , json_decode($row['extra'] , 1));
                }
            }
        }
    }
    
    protected function hook_before_write($array)
    {
        if($array['extra']){
            $array['extra'] = json_encode($array['extra']);
        }
        
        return $array;
    }

    public function listByType($type){
        if(!$this->init_db($data_key , self::DB_SLAVE))
            return false;
        $sql = 'select * from '.$this->table.' where type = "'.$type.'" and status = '.self::STATUS_NORMAL;

        $aCondition = array(
            'type' => $type,
            'status' => self::STATUS_NORMAL,
        );
        return $this->listByCondition(array() , $aCondition , 1 , 0);

    }
}