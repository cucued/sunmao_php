<?php
class ml_model_datafactory extends ml_lib_datamodel_common{
    var $table = 'adm_datafactory_data';

    
    public function getListByDatakey($data_key , $page = 0 , $pagesize = 10){
        if(!$this->init_db($data_key , self::DB_SLAVE))
            return false;

        if($page > 0){
            $start = ($page - 1) * $pagesize;
            $limit = ' limit '.$start.','.$pagesize;
        }
        $sql = 'select * from '.$this->table.' where data_key="'.$this->escape($data_key).'" and status = '.self::STATUS_NORMAL.' order by id desc'.$limit;
        $rs = $this->fetch($sql);

    }

    protected function hook_after_fetch(){

        if($this->_data){
            foreach ($this->_data as &$row) {
                $row['data'] = $this->dataDecode($row['data']);
            }
        }
    }
    
    protected function hook_before_write($array)
    {
        $array['data'] = $this->dataEncode($array['data']);
        return $array;
    }
    private function dataEncode($data){
        return json_encode($data);
    }
    private function dataDecode($data){
        return json_decode($data , 1);
    }
}