<?php
return array(
	'^/s/(.*)' => array('path'=>'/basic/bs_basic_freepage.php','method'=>'run'),
	'^/static/(.*)' => array('path'=>'/basic/bs_basic_static.php','method'=>'run'),
	'^/article/tag/(.+)' => array('path'=>'/article/bs_article_list.php','method'=>'tagRun'),
	'^/(.*)/article/category/(.+)' => array('path'=>'/article/bs_article_list.php','method'=>'run'),
	'^/article/show/([0-9]+)' => array('path'=>'/article/bs_article_show.php','method'=>'runById'),
	'^/article/show/([0-9a-zA-Z\-_]+)' => array('path'=>'/article/bs_article_show.php','method'=>'runByAlias')
);