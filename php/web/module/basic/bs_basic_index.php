<?php
class bs_basic_index extends bs_web_controller{
	private $pagesize = 10;
	public function run(){
		$oArticle = new bs_article_model_article();
		$oArticle->listByCondition(array(),array(),1,$this->pagesize,$sort="pub_time");
		$aData = $oArticle->get_data();

		$oArticle->countByCondition();
		$total = $oArticle->get_data();
		

		$aOutputData = array(
			'articles' => $aData,
			'total' => (int)$total,
			'pagesize' => $this->pagesize,
		);

		$this->output_html('index',$aOutputData);
	}

	public function getMore(){
		$page = abs((int)Tool_input::input('page'));

		$oArticle = new bs_article_model_article();
		$oArticle->countByCondition();
		$total = $oArticle->get_data();
		$total_page = ceil($total/$this->pagesize);

		if($total_page < $page){
			$this->output_json(APICODE_PARAM);
		}
		$oArticle->listByCondition(array(),array(),$page,$this->pagesize,$sort="pub_time") or $this->output_json(APICODE_BUSY);
		$aData = $oArticle->get_data();

		

		$aOutputData = array(
			'articles' => $aData,
		);
		$html = $this->output_html('indexModArticles',$aOutputData,true);
		$aData = array(
			'html' => $html,
			'is_over' => $total_page == $page ? 1 : 0,
		);
		$this->output_json(APICODE_SUCC , '',$aData);
	}
}