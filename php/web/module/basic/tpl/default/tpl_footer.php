<div class="share-layer">
  <div class="share-button">
  <a href="javascript:;" onclick="window.open('<?php echo $share_sina_url; ?>');"> <i class="iconfont">&#xe6f6;</i></a>
  <a href="javascript:;" class="jiathis_button_weixin"> <i class="iconfont">&#xe6f7;</i></a>
  <a href="javascript:;" onclick="window.open('<?php echo $share_douban_url; ?>');"><i class="iconfont">&#xe6f0;</i></a>
  </div>
  <div class="hhz-shareLayerClose ">
    <a href="javascript:;" class="shareTrigger">取消</a>
  </div>
</div>
<footer class="footer hidden-sm hidden-xs">
  <div class="container" id="footer">
    <div class="row">
      
      <div class="col-md-6">
        <ul class="list-unstyled whitefont">
          <!--<li><a href="#"><img id="footlogo" height="25" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/logo2.png"></a></li>-->
          <li>
            <p>@2015 haohaozhu.com Allrights reserved</p>
          </li>
          <!--
          <li>
            <p>京ICP证 123989号</p>
          </li>
          -->
        </ul>
      </div>
      
      <div class="col-md-6 text-right" style="margin-bottom: 20px">
        <ul class=" footer-top  list-inline  ">
          <?php foreach ($FOOTER as $row) { ?>
          <li><a href="<?php echo $row['url'] ?>"<?php if($row['openNew']){ ?> target="_blank"<?php } ?>><?php echo $row['name']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</footer>


  
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/jquery.min.js"></script>
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/bootstrap.min.js"></script>
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/jquery.lazyload.min.js"></script>
    <script src="/static/js/default/basic-nav-footer+article-list/<?php echo BS_JS_VER ?>"></script>
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/init.js"></script>
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/float.js"></script>
    <script src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/js/bearshop.js"></script>
    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>

    <script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?08cdc64454583146c3c3fd3b443c0ac3";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

  </body>
</html>
