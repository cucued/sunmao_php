<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $PAGEINFO['description']?>">
    <meta name="author" content="">
    <meta name="tags" content="<?php echo $PAGEINFO['tags']; ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <link rel="icon" href="/favicon.ico">
    <title><?php echo $PAGEINFO['title']; ?></title>
    <link href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/css/hhz_w.css" rel="stylesheet">
  </head>