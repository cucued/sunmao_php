<?php include(BS_PUBLIC_TPL_PATH.'tpl_header.php'); ?>
  <body>
    <?php include(BS_PUBLIC_TPL_PATH.'tpl_navbar.php'); ?>
    <div id="hhz-waterfalls">
      <div class="hhz-box container">
        <div class= "row">
          <div>
            <?php echo $freepage['data']['content']; ?>
          </div>
          <?php 
          if($mixfield2id){
          foreach ($mixfield2id as $dfId){ 
                $df_row_data = $df_data[$dfId]['data'];

                include($TPL_DIR.'/freepage/tpl_freepage_'.$df_data[$dfId]['data_key'].'.php');
            ?>
            
          <?php }} ?>
        </div>
      </div>
    </div>
<?php include(BS_PUBLIC_TPL_PATH.'tpl_footer.php'); ?>