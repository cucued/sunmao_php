<div id="float">
<a href="/"  id="logoleft" class="logoleft hidden-xs hidden-sm"><img  src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/logoleft.png"></a>
  <div id="headimg" ><a href="javascript:;"><img width="30px" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/list.png"></a></div>
  <div id="top"><a href="#" class="backtop"><img width="30px" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/top.png"></a></div>
  <div id="share" class="hidden-lg hidden-md"><a href="javascript:;" class="shareTrigger"><img width="30px" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/share-mobile.png"></a></div>
</div>
<header>
  <div class="container hhz-header">
    <div class="row" id="header">
      <div class="headlogo"> <a href="/"><img width="50" height="50" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/logo.png"></a> <a href="/"><img height="25" src="<?php echo BIZSITE_DOMAIN_STATIC; ?>/web/images/haohaozhu/logo2.png"></a></div>
      <div><a data-action="backtotop" data-tip="回到顶部" href="#" class="btn-backtotop btn-action"><div class="arrow"></div><div class="stick"></div></a></div>
      <div>
        <ul class="nav nav-head">
          <?php 
            foreach ($NAV as $row) {
            ?>
            <li<?php if($NAV_ID == $row['value']){?> class="hhz-active"<?php } ?>><a href="<?php echo $row['ch_url']; ?>"<?php if($row['openNew']){ ?> target="_blank"<?php } ?>><?php echo $row['name']; ?></a></li>
          <?php 
            }
           ?>
        </ul>
      </div>
    </div>
  </div>
</header>

<div class="side-menu">
  <ul id="menuul">
    <h2></h2>
    
    <?php
      foreach ($NAV as $row) {
    ?>
    <li ><i class="iconhhz">&#x3432;</i>
    <?php if($row['children']){ ?>
    <a href="javascript:;" class="aSideBigNav"><?php echo $row['name']; ?></a>
    <?php }else{ ?>
    <a href="<?php echo $row['ch_url']; ?>"><?php echo $row['name']; ?></a>
    <?php } ?>
  
        <?php if($row['children']){?>
        <ul class="dropdown-menu right-list" style="display: none;">
        <?php foreach ($row['children'] as $row_child) { 
            $aUrl = explode('/',$row_child['ch_url']);
            if($aUrl[2].$aUrl[3] == 'articlecategory' && $aUrl[4] == $ctg_id){
              $isNow = true;
            }else{
              $isNow = false;
            }
          ?>
          <li<?php if($isNow){ ?> class="hhz-active"<?php } ?>>
            <a href="<?php echo $row_child['ch_url'] ?>"<?php if($row_child['openNew']){ ?> target="_blank"<?php } ?>>
              <?php echo Tool_string::un_html($row_child['name']) ?>
            </a>
          </li>
        <?php }//foreach children?>
        </ul>
        <?php } ?>
    </li>
    <?php }//foreach nav ?>
  </ul>
</div>
<div id="side_dis" class="side-dis">&nbsp;</div>