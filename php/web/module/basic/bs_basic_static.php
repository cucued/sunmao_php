<?php
class bs_basic_static extends bs_web_controller{
	public function run(){

		$PLUS = '+';
$SEP = '-';

$root = WEB_ROOT_PATH;

		$type = $this->getUrlInput(1);
		$tpl = $this->getUrlInput(1);
		$url = $this->getUrlInput(3);
		$version = $this->getUrlInput(4);


		if(!in_array($type , array('js','css'))){
			die('type');
		}

		if(!preg_match('/^[0-9a-z\-_\+\/]*$/s',$url)){
			die('name');
		}
		$cache_path = '';

		
		$a = explode($PLUS,$url);
		
		foreach ($a as $s) {

			$b = explode($SEP , $s);
			$module = array_shift($b);
			$dir = $root.'module/'.$module.'/'.$type.'/';

			foreach ($b as $name) {
				$file = $dir.$name.'.'.$type;
				
				if(!is_file($file)){
					continue;
				}

				$source .= file_get_contents($file);
			}
		}

		Tool_http::always_cache();

		include(SYS_INC_PATH.'3rd/JavascriptPacker.php');
		$packed = JSMin::minify($source);

		if($type == 'js'){
			header('Content-type: text/javascript');  
		}else if($type == 'css'){
			header('Content-type: text/css');  
		}

		echo '/*'.time().'*/';
		echo $packed;


	}
}