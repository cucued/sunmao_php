<?php
class bs_basic_freepage extends bs_web_controller{
	public function run(){

		$id = $this->getUrlInput(1);

		$oModelIdx = new bs_basic_model_freepageIndex();
		if(is_integer($id)){
			$oModelIdx->getById($id) or $this->redirect('busy');
		}else{
			$oModelIdx->getByPageurl($id);
		}

		$aIndex = $oModelIdx->get_data();
		
		if(empty($aIndex)){

			$this->redirect(404);
		}
		$data_id = $aIndex['page_data_id'];

		$oModelDf = new ml_model_datafactory();
		$oModelDf->getById($data_id) or $this->redirect('busy');
		$aFreepage = $oModelDf->get_data();

		$this->setPageTitle($aFreepage['data']['page_name']);
		$this->setPageDescription($aFreepage['data']['page_description']);
		$this->addPageTags(explode(' ',$aFreepage['data']['page_tags']));


		if($aFreepage['data']['mix']['pageModule']){
			foreach ($aFreepage['data']['mix']['pageModule'] as $key => $value) {
				$aModId[] = $value['pageModule_id'];
			}
		}
		
		if($aModId){
			$oModelDf->getByIds($aModId);
			$aModule = Tool_array::format_2d_array($oModelDf->get_data(),'id',Tool_array::FORMAT_FIELD2ROW);
		}
		$aOutput = array(
			'mixfield2id' => $aModId,
			'df_data' => $aModule,
			'freepage' => $aFreepage,
		);

		$this->output_html('freepage',$aOutput);
	}
}