  //right menu
  $('#aNavRight').click(function(){
    if($('#divRightMenu').css('right') =='-200px'){
      $('#divRightMenuDis').show();
      $('#divRightMenu').animate({right:'0px'});
      
    }else{
      $('#divRightMenuDis').hide();
      $('#divRightMenu').animate({right:'-200px'});
    }
  })
  $('#divRightMenuDis').mousedown(function(){
    $('#aNavRight').trigger('click');
  });
  $(document).scroll(function(){
    var navRightBtnDiv = $('#aNavRight').parent();
    if($(document).scrollTop()>30){
      if(navRightBtnDiv.hasClass('hidden-md')){
        navRightBtnDiv.fadeIn('fast').removeClass('hidden-md').removeClass('hidden-lg');
      }
      if($('#navFloatLogo').css('display')=='none'){
        $('#navFloatLogo').fadeIn('fast');
      }
    }else if($(document).scrollTop()<30){
      if(!navRightBtnDiv.hasClass('hidden-md')){

        navRightBtnDiv.fadeOut('fast',function(){
          navRightBtnDiv.addClass('hidden-md').addClass('hidden-lg');
        });
      }
      if($('#navFloatLogo').css('display')!='none'){
        $('#navFloatLogo').fadeOut('fast');
      }
    }
  })