<?php
class bs_article_tag extends bs_web_controller{
	private $pagesize = 10;
	public function run(){
		
		$tag = $this->getUrlInput(2);
		
		$oT2a = new bs_article_model_tag2article();
		$oT2a->countByTag($tag);
		$total = $oT2a->get_data();
		

		$oT2a->getIdsByTag($tag,1,$this->pagesize) or $this->redirect('busy');
		$aAid = Tool_array::format_2d_array($oT2a->get_data(),'article_id',Tool_array::FORMAT_VALUE_ONLY);

		if($aAid){

			$oArticle = new bs_article_model_article();
			$oArticle->getByIds($aAid) or $this->redirect('busy');
			$aArticles = $oArticle->get_data();
		}

		$aOutput = array(
			'tag'	=> $tag,
			'total'	=> $total,
			'pagesize' => $this->pagesize,
			'articles' => $aArticles,
			'channel_value' => 1,
			'channel_alias' => 'magzine',
		);
		$this->output_html('articleListTag',$aOutput);
	}
	public function getMore(){
		$tag = Tool_input::input('tag');
		$page = abs((int)Tool_input::input('page'));

		$oT2a = new bs_article_model_tag2article();
		$oT2a->countByTag($tag) or $this->output_json(APICODE_BUSY);
		$total = $oT2a->get_data();
		
		$total_page = ceil($total/$this->pagesize);

		$oT2a->getIdsByTag($tag,$page,$this->pagesize) or $this->redirect('busy');
		$aAid = Tool_array::format_2d_array($oT2a->get_data(),'article_id',Tool_array::FORMAT_VALUE_ONLY);

		if($aAid){

			$oArticle = new bs_article_model_article();
			$oArticle->getByIds($aAid) or $this->redirect('busy');
			$aArticles = $oArticle->get_data();
		}

		$aOutput = array(
			'articles' => $aArticles,
		);
		$html = $this->output_html('articleListRows' , $aOutput , true);

		$aData = array(
			'html' => $html,
			'is_over' => $page >=$total_page ? 1 : 0,
		);
		$this->output_json(APICODE_SUCC , '' , $aData);
	}
}