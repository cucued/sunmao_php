<?php
class bs_article_show extends bs_article_controller{
	private $article;
	public function runById(){
		$id = (int)$this->getUrlInput(2);

		$oArticle = new bs_article_model_article();

		$oArticle->getById($id) or $this->redirect('busy');

		$this->article = $oArticle->get_data();

		$this->_run();
	}
	public function runByAlias(){
		$id = $this->getUrlInput(2);

		$oArticle = new bs_article_model_article();
		$oArticle->getByAlias($id) or $this->redirect('busy');

		$this->article = $oArticle->get_data();
		$this->_run();
	}
	private function _run(){
		
		$this->article['content'] = preg_replace('/<img .*src="(.*)".*>/U','<img class="imgLazyLoad" data-original="\\1" width="600px">',$this->article['content']);
		$this->addPageTags($this->article['tag']);

		$ARTICLECATEGORY = $this->getOutput('ARTICLECATEGORY');
		$aCtg = Tool_array::format_2d_array($ARTICLECATEGORY,'value',Tool_array::FORMAT_FIELD2ROW);
		$ctg = $aCtg[$this->article['category']];
		$CHANNEL = $this->getOutput('CHANNEL');
		$aChannel = Tool_array::format_2d_array($CHANNEL,'value',Tool_array::FORMAT_FIELD2ROW);
		$channel = $aChannel[$ctg['channel']];


		$this->setShare(Tool_string::un_html($this->article['title']) 
					,ml_tool_urlMaker::article_show($this->article['id'])
					, imgid2url($this->article['cover_img_id'],ML_IMG_SIZE_BIG));
		$aOutput = array(
			'article' => $this->article,
			'ctg_id' => $this->article['category'],
			'channel_value' => $channel['value'],
			'channel_alias' => $channel['alias'],
		);
		$this->setPageTitle($this->article['title']);

		// $this->add_breadcrumbs($this->article['title'] , '');
		$this->output_html('articleShow',$aOutput);
	}
}
