<?php
class bs_article_list extends bs_web_controller{
	private $pagesize = 10;
	private function _checkCategory($category_value){
	

		//文章分类 
		$oFreetree = new ml_model_freetree();
        $oFreetree->listByType('articleCategory') or $this->redirect('busy');
        $aCategory = $oFreetree->get_data();

        $aCategory = Tool_array::format_2d_array($aCategory , 'value' , Tool_array::FORMAT_FIELD2ROW);
		

		if(!isset($aCategory[$category_value])){
			return false;
		}else{
			return true;
		}
	}
	public function run(){
		$oFreetree = new ml_model_freetree();
		$oFreetree->listByType('leftnavTag') or $this->redirect('busy');
		$aLeftTags = $oFreetree->get_data();

		$channel_alias = $this->getUrlInput(0);
		$aChannel = Tool_array::format_2d_array($this->getOutput('CHANNEL'),'alias',Tool_array::FORMAT_FIELD2ROW);
		$channel = $aChannel[$channel_alias];

		if(!$channel){
			$this->redirect(404);
		}


		$category_value = abs((int)$this->getUrlInput(3));
		if(!$this->_checkCategory($category_value)){
			$this->redirect(404);
		}

		$oArticle = new bs_article_model_article();
		$oArticle->listByCondition(array(),array('category'=>$category_value),1,$this->pagesize,'pub_time') or $this->redirect('busy');
		$aArticle = $oArticle->get_data();

		$oArticle->countByCondition();
		$total = $oArticle->get_data();
		
		$aOutput = array(
			'articles' => $aArticle,
			'total' => $total,
			'ctg_id' => $category_value,
			'pagesize' => $this->pagesize,
			'leftnavTag' => $aLeftTags,
			'channel_value' => $channel['value'],
			'channel_alias' => $channel['alias'],
		);
		$this->output_html('articleList',$aOutput);
	}
	public function getMore(){
		$category_value = abs((int)Tool_input::input('ctg'));
		$page = abs((int)Tool_input::input('page'));
		if(!$this->_checkCategory($category_value)){
			$this->output_json(APICODE_PARAM);
		}

		$oArticle = new bs_article_model_article();
		$oArticle->countByCondition(array('category'=>$category_value));
		$total = $oArticle->get_data();
		$total_page = ceil($total/$this->pagesize);

		if($total_page < $page){
			$this->output_json(APICODE_PARAM);
		}

		$oArticle->listByCondition(array(),array('category'=>$category_value),$page,$this->pagesize,'pub_time') or $this->redirect('busy');
		$aArticle = $oArticle->get_data();


		$aOutput = array(
			'articles' => $aArticle,
		);

		$aData = array(
			'html' => $this->output_html('articleListRows' , $aOutput , true),
			'is_over' => $total_page >= $page ? 1 : false,
		);
		$this->output_json(APICODE_SUCC , '' , $aData);
	}
	public function tagRun(){
		
		$tag = Tool_string::decodeUrlChinese($this->getUrlInput(2));

		
		$oFreetree = new ml_model_freetree();
		$oFreetree->listByType('leftnavTag') or $this->redirect('busy');
		$aLeftTags = $oFreetree->get_data();


		$oT2a = new bs_article_model_tag2article();
		$oT2a->countByTag($tag);
		$total = $oT2a->get_data();
		

		$oT2a->getIdsByTag($tag,1,$this->pagesize) or $this->redirect('busy');
		$aAid = Tool_array::format_2d_array($oT2a->get_data(),'article_id',Tool_array::FORMAT_VALUE_ONLY);

		if($aAid){

			$oArticle = new bs_article_model_article();
			$oArticle->getByIds($aAid) or $this->redirect('busy');
			$aArticles = $oArticle->get_data();
		}

		$aOutput = array(
			'tag'	=> $tag,
			'total'	=> $total,
			'pagesize' => $this->pagesize,
			'articles' => $aArticles,
			'leftnavTag' => $aLeftTags,
			'channel_value' => 1,
			'channel_alias' => 'magzine',
		);
		$this->output_html('articleListTag',$aOutput);
	}
	public function tagGetMore(){
		
		$tag = Tool_string::decodeUrlChinese(Tool_input::input('tag'));
		$page = abs((int)Tool_input::input('page'));

		$oT2a = new bs_article_model_tag2article();
		$oT2a->countByTag($tag) or $this->output_json(APICODE_BUSY);
		$total = $oT2a->get_data();
		
		$total_page = ceil($total/$this->pagesize);

		$oT2a->getIdsByTag($tag,$page,$this->pagesize) or $this->redirect('busy');
		$aAid = Tool_array::format_2d_array($oT2a->get_data(),'article_id',Tool_array::FORMAT_VALUE_ONLY);

		if($aAid){

			$oArticle = new bs_article_model_article();
			$oArticle->getByIds($aAid) or $this->redirect('busy');
			$aArticles = $oArticle->get_data();
		}

		$aOutput = array(
			'articles' => $aArticles,
		);
		$html = $this->output_html('articleListRows' , $aOutput , true);

		$aData = array(
			'html' => $html,
			'is_over' => $page >=$total_page ? 1 : 0,
		);
		$this->output_json(APICODE_SUCC , '' , $aData);
	}
}