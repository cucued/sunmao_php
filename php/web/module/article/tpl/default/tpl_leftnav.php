      <div class="hhz-left hidden-xs hidden-sm">
        <div class="hhz-column">
          <ul class="list-unstyled lefts">
            <h3>招牌栏目</h3>
            <?php 
            foreach (Tool_array::sort_2d_array($ARTICLECATEGORY,'sort_num') as $row) { 
              if($row['channel'] == $channel_value){
                if($row['value'] == $ctg_id){
            ?>  
            <li class="hhz-active"><?php echo Tool_string::un_html($row['name']) ?></li>
            <?php }else{ ?>
            <li><a href="<?php echo ml_tool_urlMaker::category_article_list($channel_alias,$row['value']) ?>"><?php echo Tool_string::un_html($row['name']) ?></a></li>
            <?php 
                }//now
              }//channel
            }//foreach
             ?>
          </ul>
        </div>
        <div class="hhz-keywords">
          <ul class="list-unstyled lefts">
            <h3>热门关键字</h3>
            <?php foreach ($leftnavTag as $tag) { ?>
            <li><a href="<?php echo ml_tool_urlMaker::tag_article_list($tag['tag']); ?>">#<?php echo $tag['tag']; ?></a></li>
            <?php } ?>   
          </ul>
        </div>
      </div>