<?php 


include(BS_PUBLIC_TPL_PATH.'tpl_header.php'); ?>

<body>
<?php include(BS_PUBLIC_TPL_PATH.'tpl_navbar.php'); ?>

<div   class="container" id="main">
  <div  class="row">
    <div class="col-md-3">
      <?php include($TPL_DIR.'/tpl_leftnav.php'); ?></div>
    <div class="hhz-box col-xs-12 col-md-6">

      <div class="hhz-detail">
        <div class="row mainrow">
          <div  class="col-md-9 col-sm-12 col-xs-12">
            <ul class="nav nav-detail">
              <?php foreach ($article['tag'] as $tag) { ?>
              <li>
                <a href="<?php echo ml_tool_urlMaker::tag_article_list($tag) ?>">#<?php echo Tool_string::un_html($tag); ?></a>
              </li>
              <?php } ?>
            </ul>
          </div>
          <div class="col-md-3 hidden-sm hidden-xs">
            <div class="login text-right">
              <ul class="list-inline share-top">
                <li>
                  <a href="javascript:;" onclick="window.open('<?php echo $share_sina_url; ?>');"> <i class="iconfont">&#xe6f6;</i></a>
                </li>
                <li>
                  <a href="javascript:;" class="jiathis_button_weixin"> <i class="iconfont">&#xe6f7;</i></a>
                </li>
                <li>
                  <a href="javascript:;" onclick="window.open('<?php echo $share_douban_url; ?>');"><i class="iconfont">&#xe6f0;</i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <h1>
          <?php echo Tool_string::un_html($article['title']); ?></h1>
        <div>
          <?php echo $article['content']; ?></div>
        <div class="detail_foot">
          <ul class="list-unstyled">
            <li class="hidden-sm hidden-xs">喜欢这篇的话，分享给更多朋友吧，他们应该住得更好</li>
            <li class="hidden-sm hidden-xs">
              <ul class="list-inline share">
                <li>
                  <a href="javascript:;" onclick="window.open('<?php echo $share_sina_url; ?>');"> <i class="iconfont">&#xe6f6;</i></a>
                </li>
                <li>
                  <a href="javascript:;" class="jiathis_button_weixin"> <i class="iconfont">&#xe6f7;</i></a>
                </li>
                <li>
                  <a href="javascript:;" onclick="window.open('<?php echo $share_douban_url; ?>');"><i class="iconfont">&#xe6f0;</i></a>
                </li>
              </ul>
            </li>
            <li>点击下面的关键词看更多相关文章</li>
          </ul>
        </div>
        <div id="big">
          <ul class="nav nav-big">
              <?php 
              $tags = !empty($article['recommend_tag']) ? $article['recommend_tag'] : $article['tag'];
              
              $tag_length = 0;
              $maxlen = 40 ; 
              foreach ($tags as $tag) { 
                $tag_length += Tool_string::str_width($tag);
                if($tag_length > $maxlen){
                  break;
                }else{
                  $tag_length+=8;
                }?>
              <li>
                <a href="<?php echo ml_tool_urlMaker::tag_article_list($tag) ?>
                  ">
                  <?php echo Tool_string::un_html($tag); ?></a>
              </li>
              <?php } ?>
            </ul>

        </div>
      </div>
    </div>
  </div>
</div>

<?php include(BS_PUBLIC_TPL_PATH.'tpl_footer.php'); ?>
<script type="text/javascript">
  $(".imgLazyLoad").lazyload({
    effect : "fadeIn"
  });
  $(".imgLazyLoad").load(function(){
    var theImage = new Image();
theImage.src = $(this).attr("src");
console.log(theImage.width);
// if(theImage.width > 550){
//   $(this).attr('width','550px');
// }else{
//   $(this).attr('width',theImage.width+'px');
// }
  });
</script>