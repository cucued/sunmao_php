<?php
class bs_article_controller extends bs_web_controller{
	public function __construct(){
		parent::__construct();

		$oFreetree = new ml_model_freetree();
		$oFreetree->listByType('leftnavTag');
		$atags = Tool_array::format_2d_array($oFreetree->get_data(),'sort_num',Tool_array::FORMAT_FIELD2ROW);
		ksort($atags);

		$this->addOutput('leftnavTag',$atags);
	}
}