<?php
class bs_article_model_article extends ml_lib_datamodel_common{
	
    var $table = 'bs_cms_article';

    public function getByClass($class = 0 , $page=1 , $pagesize = 10){
    	if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;

        $start = ($page-1) * $pagesize;
        $class_where = $class > 0 ? ' and class='.$class : '';
        $sql = 'select * from '.$this->table.' where status='.self::STATUS_NORMAL.$class_where.' order by pub_time desc limit '.$start.','.$pagesize;
        return $this->fetch($sql);
    }

    public function getByAlias($alias){
        if(!$this->init_db($staff_id , self::DB_MASTER))
            return false;
        
        $sql = 'select * from '.$this->table.' where eng_alias = "'.$this->escape($alias).'" and status = '.self::STATUS_NORMAL;
        return $this->fetch_row($sql);
    }

    protected function hook_after_fetch(){
        if($this->_data){
            foreach ($this->_data as &$row) {
                $row['tag'] = array_filter(explode(' ',$row['tag']));
                $row['recommend_tag'] = array_filter(explode(' ',$row['recommend_tag']));
            }
        }
    }
    
}