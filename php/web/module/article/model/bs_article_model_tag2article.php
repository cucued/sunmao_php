<?php
class bs_article_model_tag2article extends Lib_datamodel_db{
	var $table = 'bs_cms_tag2article';
	function __construct()
    {
        $db_config = ml_factory::load_standard_conf('dbAdmin');        //目前只有一个配置文件，所以
        parent::__construct('admin' , $db_config);
    }
    
    function getIdsByTag($tag , $page = 1 , $pagesize = 10){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;

        $start = ($page - 1) * $pagesize;
        $sql = 'select article_id from '.$this->table.' where tag = "'.$this->escape($tag).'"';
        return $this->fetch($sql);
    }
    function countByTag($tag){
        if(!$this->init_db($staff_id , self::DB_SLAVE))
            return false;
        return $this->fetch_count('tag="'.$this->escape($tag).'"');
    }
}