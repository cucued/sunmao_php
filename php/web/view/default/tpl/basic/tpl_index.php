<?php include(BS_PUBLIC_TPL_PATH.'tpl_header.php'); ?>
<body>
<?php include(BS_PUBLIC_TPL_PATH.'tpl_navbar.php'); ?>
<div class="headertwo">
      <ul>
      <?php 
      foreach ($NAV[1]['children'] as $row) {
      	?>
      	<li><a href="<?php echo $row['ch_url'];?>"><?php echo $row['name']; ?></a></li>
      <?php } ?>
      </ul>
    </div>
<div class="hhz-box container"  id="hhz-waterfalls">
  <div class= "row" id="divArticle"> 
	<?php include($TPL_DIR.'/tpl_indexModArticles.php'); ?>
  </div>
</div>

	<?php 
	if($total > $pagesize){ ?>
<div class="hhz-more" id="divMore">
  <div id="moretest">
    <ul class="nav nav-more">
      <li><a href="javascript:;" id="aLoadMoreArticle">加载更多</a></li>
    </ul>
  </div>
</div>
	<?php } ?>

<?php include(BS_PUBLIC_TPL_PATH.'tpl_footer.php'); ?>
<script type="text/javascript">
	var article_page = 1;
	
	
	$('#aLoadMoreArticle').click(function(){
		$.get('/basic/index/getMore?page='+(article_page+1),'',function(data){
			var rs = bs_str2obj(data);
			if(rs.code == APICODE_SUCC){
				article_page ++;
				$('#divArticle').append(rs.data.html);
				if(rs.data.is_over == 1){
					$('#divMore').hide();
				}
			}
		});
	})
</script>