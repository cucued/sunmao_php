<div>
<?php 
$i = 1;
$total_n = count($articles);
foreach ($articles as $row) { ?>
  
    <div class="hhz-board ">
      <div class="hhz-boardimg">
        <a href="<?php echo ml_tool_urlMaker::article_show($row['id']); ?>" title="<?php echo Tool_string::un_html($row['title']); ?>"><img class="img-responsive" src="<?php echo imgid2url($row['cover_img_id'],ML_IMG_SIZE_BIG) ?>" /></a>
      </div>
      <div class="hhz-boardtext">
        <h2><a href="<?php echo ml_tool_urlMaker::article_show($row['id']); ?>" ><?php echo Tool_string::un_html($row['title']); ?></a></h2>
        <p><?php echo Tool_string::un_html($row['desc']); ?></p>
        <ul class="nav nav-hhz">
          <?php 
            $tag_length = 0;
            $maxlen = 36 ; 

            foreach ($row['tag'] as $tag) { 
              $tag_length += Tool_string::str_width($tag);
              if($tag_length > $maxlen){
                break;
              }else{
                $tag_length+=4;
              }
            ?>
              <li><a href="<?php echo ml_tool_urlMaker::tag_article_list($tag) ?>">#<?php echo Tool_string::un_html($tag); ?></a></li>
            <?php } ?>
        </ul>
      </div>
    </div>
  <?php if($i%2==0 && $i < $total_n){ ?>
  <div style="clear: both;"></div>
  </div><div>
  <?php }else if($i == $total_n){?>
  <div style="clear: both;"></div>
  </div>
  <?php } ?>
<?php 
  $i++;
} 
?>