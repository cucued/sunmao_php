<?php foreach ($articles as $article) { ?>
      <div class="hhz-channel">
          <div class="hhz-channelimg"><a href="<?php echo ml_tool_urlMaker::article_show($article['id']); ?>" ><img class="img-responsive" src="<?php echo imgid2url($article['cover_img_id']) ?>" /></a></div>
          <div class="hhz-channeltext">
            <h1><a href="<?php echo ml_tool_urlMaker::article_show($article['id']); ?>" ><?php echo Tool_string::un_html($article['title']); ?></a></h1>
            <p><?php echo Tool_string::un_html($article['desc']); ?></p>
            <div>
              <ul class="nav nav-hhz">
                <?php foreach ($article['tag'] as $tag) { ?>
                <li>
                  <a href="<?php echo ml_tool_urlMaker::tag_article_list($tag) ?>">#<?php echo Tool_string::un_html($tag); ?></a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      <?php } ?>