<?php 

$share_sina_url = Tool_share::sina_weibo(ml_tool_urlMaker::article_show($article['id']),Tool_string::un_html($article['title']) , imgid2url($article['cover_img_id'],IMGSIZE_SMALL));
$share_douban_url = Tool_share::douban(ml_tool_urlMaker::article_show($article['id']),Tool_string::un_html($article['title']) , imgid2url($article['cover_img_id'],IMGSIZE_SMALL));

include(BS_PUBLIC_TPL_PATH.'tpl_header.php'); ?>

<body>
<?php include(BS_PUBLIC_TPL_PATH.'tpl_navbar.php'); ?>

<div class="container" id="main" ctg_id="<?php echo $ctg_id; ?>">
  <div class="row">
    <div class="col-md-3">
      <?php include($TPL_DIR.'/tpl_leftnav.php'); ?>
    </div>

    <div class="hhz-box col-md-6" id="divArticle">
      <?php include($TPL_DIR.'/tpl_articleListRows.php'); ?>
    </div>
  </div>
</div>

<?php if($total > $pagesize){ ?>
<div class="hhz-more" id="divMore">
  <div id="moretest">
    <ul class="nav nav-more">
      <li><a href="javascript:;" id="aLoadMoreArticle">加 载 更 多</a></li>
    </ul>
  </div>
</div>
<?php } ?>
  

<?php include(BS_PUBLIC_TPL_PATH.'tpl_footer.php'); ?>
<script type="text/javascript">
  var article_page = 1;
  
  
  $('#aLoadMoreArticle').click(function(){
    $.get('/article/list/tagGetMore?tag='+encodeURIComponent($('#main').attr('tag'))+'&page='+(article_page+1),'',function(data){
      var rs = bs_str2obj(data);
      if(rs.code == APICODE_SUCC){
        article_page ++;
        $('#divArticle').append(rs.data.html);
        if(rs.data.is_over == 1){
          $('#divMore').hide();
        }
      }
    });
  })
</script>