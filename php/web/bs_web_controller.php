<?php
class bs_web_controller
{
    private $page_title;
    private $page_description;
    private $page_tags= array();
	private $start_time;
    private $theme = 'default';
    private $breadcrumbs = array(
        array('title'=>'home','url'=>'/'),
    );
    private $nav_id;
    private $aOutput;

	public function __construct(){
		$this->start_time = time();
        $this->beforeOutput();
	}
    protected function setPageTitle($title){
        $this->page_title = $title;
    }
    protected function setPageDescription($str){
        $this->page_description = $str;
    }
    protected function addPageTags($aTag){
        $this->page_tags = array_merge((array)$aTag , $this->page_tags);
    }
    protected function getOutput($key){
        return $this->aOutput[$key];
    }
    protected function setShare($title , $url , $pic){
        $title = Tool_string::un_html($title);
        $url = urlencode($url);
        $pic = urlencode($pic);
        $this->addOutput('share_sina_url' , Tool_share::sina_weibo($url,$title, $pic));
        $this->addOutput('share_douban_url' , Tool_share::douban($url,$title, $pic));
    }

    public function addOutput($key , $value){
        $this->aOutput[$key] = $value;
    }
	protected function get_module_name(){
        $aClass = explode('_' , get_class($this));
        return $aClass[1];
    }
    protected function add_breadcrumbs($title , $url = ''){
        $this->breadcrumbs[] = array(
            'title' => $title,
            'url' => $url,
        );
        return true;
    }
    protected function set_nav_id($nav_id){
        $this->nav_id = $nav_id;
    }
	public function run(){

	}
	public function get_theme(){
		return $this->theme;
	}
	protected function getUrlInput($pos){

		$url = ltrim($_SERVER['REQUEST_URI'],'/');
		$aUrl = explode('/', $url);
		return $aUrl[$pos];
	}

    protected function beforeOutput(){
        
        $oFreetree = new ml_model_freetree();

        
        $oFreetree->listByType('cms_channel') or $this->redirect('busy');
        $aChannel = $oFreetree->get_data();
        $this->addOutput('CHANNEL',$aChannel);


        $oFreetree->listByType('cms_nav') or $this->redirect('busy');
        $aNav = $oFreetree->get_data();
        $aNav = formatTree($aNav);
        $aNav = Tool_array::sort_2d_array($aNav,'sort_num');
        foreach ($aNav as &$row) {
            if($row['children']){
                $row['children'] = Tool_array::sort_2d_array($row['children'],'sort_num');
            }
        }
        
        
        $this->addOutput('NAV' , $aNav);
        
        //文章分类 
        $oFreetree->listByType('cms_articleCategory') or $this->redirect('busy');
        $aCategory = $oFreetree->get_data();
        $aCategory = Tool_array::format_2d_array($aCategory , 'value' , Tool_array::FORMAT_FIELD2ROW);
        $this->addOutput('ARTICLECATEGORY' , $aCategory);
        //标准尾
        $oFreetree->listByType('cms_footer') or $this->redirect('busy');
        $aFooter = $oFreetree->get_data();
        
        $aCategory = Tool_array::format_2d_array($aFooter , 'value' , Tool_array::FORMAT_FIELD2ROW);
        $this->addOutput('FOOTER' , $aFooter);

        //seo
        $oFd = new ml_model_datafactory();
        $oFd->getById(7);
        $aSeo = $oFd->get_data();
        $pageinfo = array(
            'title' => $this->page_title.' '.$aSeo['data']['page_title'],
            'description' => $this->page_description.' '.$aSeo['data']['page_description'],
            'tags' => implode(' ',array_unique(array_merge($this->page_tags , explode(' ' , $aSeo['data']['page_tag'])))),
        );
        
        $this->addOutput('PAGEINFO',$pageinfo);


        $this->setShare('好好住',BIZSITE_DOMAIN_WEB,'');
        return true;
    }
	protected function output_html($tpl_name , $aData , $return = false){
		$view_dir = WEB_ROOT_PATH.'/view/'.$this->get_theme().'/';
        define('BS_PUBLIC_TPL_PATH' , $view_dir.'tpl/'.$this->get_module_name().'/');

        $tpl_path = $view_dir.'tpl/'.$this->get_module_name().'/tpl_'.$tpl_name.'.php';
        $TPL_DIR = dirname($tpl_path);
        $IMG_URL = $view_dir.'images/';


        $BREADCRUMBS = $this->breadcrumbs;
        $NAV_ID = $this->nav_id;

        
        extract($this->aOutput);

		extract($aData);

		

		Tool_logger::debugLog('tpl','load_tpl:'.$tpl_path);
		if(!is_file($tpl_path)){
			$this->error('tpl not found');
		}

		if($return){
			ob_start();
			include($tpl_path);
			return ob_get_clean();	
		}else{
			$this->_over();
			include($tpl_path);
			die;
		}
	}
    protected function output_json($api_code , $msg = '',$data = ''){
        if(empty($msg)){
            $aCode2msg_cn = array(
                APICODE_SUCC => '操作成功',
                APICODE_FAIL => '操作失败，请联系管理员',
            );
            $aCode2msg_eng = array(
                APICODE_SUCC => 'Success!',
                APICODE_FAIL => 'Error!',
            );

            $aMsg = $this->lang == 'cn' ? $aCode2msg_cn : $aCode2msg_eng;
            $msg = $aMsg[$api_code];
        }

        echo json_encode(array(
                'code' => $api_code,
                'data' => $data,
                'msg' => $msg,
            ));
        $this->_over();
        die;
    }
	protected function error($msg){
		Tool_logger::monitorLog('tpl','error:'.$msg,Tool_logger::LOG_LEVEL_ALERM);
	}
	private function _over()
    {
        Tool_logger::debugLog('time' , (microtime(1)-$this->start_time) , 1);

        if(SYSDEF_DEBUG)
        {
            Tool_logger::saveRunningLog();
        }
        if(SYSDEF_DEBUG && extension_loaded('xhprof'))
        {
            $arr = xhprof_disable();
            $filename = get_class($this).'.xhprof';
            Tool_logger::oneLog('xhprof' , $filename , serialize($arr) );
        }
        return ;
    }
    /**
     * 页面重定向
     *
     * @param string $url
     */
    public function redirect($url)
    {
        die;
        $url = str_replace(array("\n", "\r"), '', $url);
        if (!headers_sent())
        {
            header("Content-Type:text/html; charset=utf-8");
            header("Location: ".$url);
            exit;
        }
        else
        {
            ml_tool_httpheader::no_cache();
            $str = "<meta http-equiv='Refresh' content='0;URL={$url}'>";
            exit($str);
        }
    }
}