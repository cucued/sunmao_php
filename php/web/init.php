<?php
require('../__sys_inc/init.php');
require('../__biz_inc/init.php');
require('config/version.php');
require('bs_web_controller.php');
require('function.php');

define('WEB_ROOT_PATH' , dirname(__file__).'/');

function web_autoload($class){
	
    $aClass = explode('_', $class);
    $sign = array_shift($aClass);

    // echo $class;
    if($sign == 'bs'){
        
        if(in_array($aClass[0] , array('tool',))){

            $s = WEB_ROOT_PATH.Autoload::class2path(implode('_' , $aClass),'bs_');    
        }else{
            $s = WEB_ROOT_PATH.'/module'.Autoload::class2path(implode('_' , $aClass),'bs_');    
            if(!is_file($s)){
                $s = WEB_ROOT_PATH.'/module/'.Autoload::class2path(implode('_' , $aClass),'bs_');    
            }
        }

        return $s;
    }else{
        return false;
    }
}
Autoload::register_autoload_function('web' , 'web_autoload');
